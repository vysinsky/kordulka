!(function(e, t) {
  "function" == typeof define && define.amd
    ? define(t)
    : "object" == typeof exports
    ? (module.exports = t())
    : (e.PhotoSwipe = t());
})(this, function() {
  "use strict";
  var e = function(e, t, n, o) {
    var i = {
      features: null,
      bind: function(e, t, n, o) {
        var i = (o ? "remove" : "add") + "EventListener";
        t = t.split(" ");
        for (var r = 0; r < t.length; r++) t[r] && e[i](t[r], n, !1);
      },
      isArray: function(e) {
        return e instanceof Array;
      },
      createEl: function(e, t) {
        var n = document.createElement(t || "div");
        return e && (n.className = e), n;
      },
      getScrollY: function() {
        var e = window.pageYOffset;
        return void 0 !== e ? e : document.documentElement.scrollTop;
      },
      unbind: function(e, t, n) {
        i.bind(e, t, n, !0);
      },
      removeClass: function(e, t) {
        var n = new RegExp("(\\s|^)" + t + "(\\s|$)");
        e.className = e.className
          .replace(n, " ")
          .replace(/^\s\s*/, "")
          .replace(/\s\s*$/, "");
      },
      addClass: function(e, t) {
        i.hasClass(e, t) || (e.className += (e.className ? " " : "") + t);
      },
      hasClass: function(e, t) {
        return (
          e.className && new RegExp("(^|\\s)" + t + "(\\s|$)").test(e.className)
        );
      },
      getChildByClass: function(e, t) {
        for (var n = e.firstChild; n; ) {
          if (i.hasClass(n, t)) return n;
          n = n.nextSibling;
        }
      },
      arraySearch: function(e, t, n) {
        for (var o = e.length; o--; ) if (e[o][n] === t) return o;
        return -1;
      },
      extend: function(e, t, n) {
        for (var o in t)
          if (t.hasOwnProperty(o)) {
            if (n && e.hasOwnProperty(o)) continue;
            e[o] = t[o];
          }
      },
      easing: {
        sine: {
          out: function(e) {
            return Math.sin(e * (Math.PI / 2));
          },
          inOut: function(e) {
            return -(Math.cos(Math.PI * e) - 1) / 2;
          }
        },
        cubic: {
          out: function(e) {
            return --e * e * e + 1;
          }
        }
      },
      detectFeatures: function() {
        if (i.features) return i.features;
        var e = i.createEl(),
          t = e.style,
          n = "",
          o = {};
        if (
          ((o.oldIE = document.all && !document.addEventListener),
          (o.touch = "ontouchstart" in window),
          window.requestAnimationFrame &&
            ((o.raf = window.requestAnimationFrame),
            (o.caf = window.cancelAnimationFrame)),
          (o.pointerEvent =
            navigator.pointerEnabled || navigator.msPointerEnabled),
          !o.pointerEvent)
        ) {
          var r = navigator.userAgent;
          if (/iP(hone|od)/.test(navigator.platform)) {
            var a = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
            a &&
              a.length > 0 &&
              ((a = parseInt(a[1], 10)),
              a >= 1 && a < 8 && (o.isOldIOSPhone = !0));
          }
          var s = r.match(/Android\s([0-9\.]*)/),
            l = s ? s[1] : 0;
          (l = parseFloat(l)),
            l >= 1 &&
              (l < 4.4 && (o.isOldAndroid = !0), (o.androidVersion = l)),
            (o.isMobileOpera = /opera mini|opera mobi/i.test(r));
        }
        for (
          var u,
            c,
            p = ["transform", "perspective", "animationName"],
            d = ["", "webkit", "Moz", "ms", "O"],
            f = 0;
          f < 4;
          f++
        ) {
          n = d[f];
          for (var h = 0; h < 3; h++)
            (u = p[h]),
              (c = n + (n ? u.charAt(0).toUpperCase() + u.slice(1) : u)),
              !o[u] && c in t && (o[u] = c);
          n &&
            !o.raf &&
            ((n = n.toLowerCase()),
            (o.raf = window[n + "RequestAnimationFrame"]),
            o.raf &&
              (o.caf =
                window[n + "CancelAnimationFrame"] ||
                window[n + "CancelRequestAnimationFrame"]));
        }
        if (!o.raf) {
          var m = 0;
          (o.raf = function(e) {
            var t = new Date().getTime(),
              n = Math.max(0, 16 - (t - m)),
              o = window.setTimeout(function() {
                e(t + n);
              }, n);
            return (m = t + n), o;
          }),
            (o.caf = function(e) {
              clearTimeout(e);
            });
        }
        return (
          (o.svg =
            !!document.createElementNS &&
            !!document.createElementNS("http://www.w3.org/2000/svg", "svg")
              .createSVGRect),
          (i.features = o),
          o
        );
      }
    };
    i.detectFeatures(),
      i.features.oldIE &&
        (i.bind = function(e, t, n, o) {
          t = t.split(" ");
          for (
            var i,
              r = (o ? "detach" : "attach") + "Event",
              a = function() {
                n.handleEvent.call(n);
              },
              s = 0;
            s < t.length;
            s++
          )
            if ((i = t[s]))
              if ("object" == typeof n && n.handleEvent) {
                if (o) {
                  if (!n["oldIE" + i]) return !1;
                } else n["oldIE" + i] = a;
                e[r]("on" + i, n["oldIE" + i]);
              } else e[r]("on" + i, n);
        });
    var r = this,
      a = 25,
      s = 3,
      l = {
        allowPanToNext: !0,
        spacing: 0.12,
        bgOpacity: 1,
        mouseUsed: !1,
        loop: !0,
        pinchToClose: !0,
        closeOnScroll: !0,
        closeOnVerticalDrag: !0,
        verticalDragRange: 0.6,
        hideAnimationDuration: 333,
        showAnimationDuration: 333,
        showHideOpacity: !1,
        focus: !0,
        escKey: !0,
        arrowKeys: !0,
        mainScrollEndFriction: 0.35,
        panEndFriction: 0.35,
        isClickableElement: function(e) {
          return "A" === e.tagName;
        },
        getDoubleTapZoom: function(e, t) {
          return e ? 1 : t.initialZoomLevel < 0.7 ? 1 : 1.5;
        },
        maxSpreadZoom: 2,
        modal: !0,
        scaleMode: "fit",
        alwaysFadeIn: !1
      };
    i.extend(l, o);
    var u,
      c,
      p,
      d,
      f,
      h,
      m,
      g,
      v,
      y,
      x,
      w,
      b,
      T,
      C,
      E,
      k,
      S,
      D,
      I,
      N,
      F,
      A,
      _,
      L,
      O,
      M,
      R,
      P,
      j,
      H,
      q,
      z,
      W,
      B,
      Z,
      U,
      $,
      K,
      X,
      G,
      Y,
      V,
      Q,
      J,
      ee,
      te,
      ne,
      oe,
      ie,
      re,
      ae,
      se,
      le,
      ue,
      ce,
      pe = function() {
        return { x: 0, y: 0 };
      },
      de = pe(),
      fe = pe(),
      he = pe(),
      me = {},
      ge = 0,
      ve = {},
      ye = pe(),
      xe = 0,
      we = !0,
      be = [],
      Te = {},
      Ce = function(e, t) {
        i.extend(r, t.publicMethods), be.push(e);
      },
      Ee = function(e) {
        var t = Qt();
        return e > t - 1 ? e - t : e < 0 ? t + e : e;
      },
      ke = {},
      Se = function(e, t) {
        return ke[e] || (ke[e] = []), ke[e].push(t);
      },
      De = function(e) {
        var t = ke[e];
        if (t) {
          var n = Array.prototype.slice.call(arguments);
          n.shift();
          for (var o = 0; o < t.length; o++) t[o].apply(r, n);
        }
      },
      Ie = function() {
        return new Date().getTime();
      },
      Ne = function(e) {
        (le = e), (r.bg.style.opacity = e * l.bgOpacity);
      },
      Fe = function(e, t, n, o) {
        e[F] = w + t + "px, " + n + "px" + b + " scale(" + o + ")";
      },
      Ae = function() {
        oe && Fe(oe, he.x, he.y, y);
      },
      _e = function(e) {
        e.container &&
          Fe(
            e.container.style,
            e.initialPosition.x,
            e.initialPosition.y,
            e.initialZoomLevel
          );
      },
      Le = function(e, t) {
        t[F] = w + e + "px, 0px" + b;
      },
      Oe = function(e, t) {
        if (!l.loop && t) {
          var n = d + (ye.x * ge - e) / ye.x,
            o = Math.round(e - vt.x);
          ((n < 0 && o > 0) || (n >= Qt() - 1 && o < 0)) &&
            (e = vt.x + o * l.mainScrollEndFriction);
        }
        (vt.x = e), Le(e, f);
      },
      Me = function(e, t) {
        var n = yt[e] - ve[e];
        return fe[e] + de[e] + n - n * (t / x);
      },
      Re = function(e, t) {
        (e.x = t.x), (e.y = t.y), t.id && (e.id = t.id);
      },
      Pe = function(e) {
        (e.x = Math.round(e.x)), (e.y = Math.round(e.y));
      },
      je = null,
      He = function() {
        je &&
          (i.unbind(document, "mousemove", He),
          i.addClass(e, "pswp--has_mouse"),
          (l.mouseUsed = !0),
          De("mouseUsed")),
          (je = setTimeout(function() {
            je = null;
          }, 100));
      },
      qe = function() {
        i.bind(document, "keydown", r),
          H.transform && i.bind(r.scrollWrap, "click", r),
          l.mouseUsed || i.bind(document, "mousemove", He),
          i.bind(window, "resize scroll", r),
          De("bindEvents");
      },
      ze = function() {
        i.unbind(window, "resize", r),
          i.unbind(window, "scroll", v.scroll),
          i.unbind(document, "keydown", r),
          i.unbind(document, "mousemove", He),
          H.transform && i.unbind(r.scrollWrap, "click", r),
          $ && i.unbind(window, m, r),
          De("unbindEvents");
      },
      We = function(e, t) {
        var n = sn(r.currItem, me, e);
        return t && (ne = n), n;
      },
      Be = function(e) {
        return e || (e = r.currItem), e.initialZoomLevel;
      },
      Ze = function(e) {
        return e || (e = r.currItem), e.w > 0 ? l.maxSpreadZoom : 1;
      },
      Ue = function(e, t, n, o) {
        return o === r.currItem.initialZoomLevel
          ? ((n[e] = r.currItem.initialPosition[e]), !0)
          : ((n[e] = Me(e, o)),
            n[e] > t.min[e]
              ? ((n[e] = t.min[e]), !0)
              : n[e] < t.max[e] && ((n[e] = t.max[e]), !0));
      },
      $e = function() {
        if (F) {
          var t = H.perspective && !_;
          return (
            (w = "translate" + (t ? "3d(" : "(")),
            void (b = H.perspective ? ", 0px)" : ")")
          );
        }
        (F = "left"),
          i.addClass(e, "pswp--ie"),
          (Le = function(e, t) {
            t.left = e + "px";
          }),
          (_e = function(e) {
            var t = e.fitRatio > 1 ? 1 : e.fitRatio,
              n = e.container.style,
              o = t * e.w,
              i = t * e.h;
            (n.width = o + "px"),
              (n.height = i + "px"),
              (n.left = e.initialPosition.x + "px"),
              (n.top = e.initialPosition.y + "px");
          }),
          (Ae = function() {
            if (oe) {
              var e = oe,
                t = r.currItem,
                n = t.fitRatio > 1 ? 1 : t.fitRatio,
                o = n * t.w,
                i = n * t.h;
              (e.width = o + "px"),
                (e.height = i + "px"),
                (e.left = he.x + "px"),
                (e.top = he.y + "px");
            }
          });
      },
      Ke = function(e) {
        var t = "";
        l.escKey && 27 === e.keyCode
          ? (t = "close")
          : l.arrowKeys &&
            (37 === e.keyCode
              ? (t = "prev")
              : 39 === e.keyCode && (t = "next")),
          t &&
            (e.ctrlKey ||
              e.altKey ||
              e.shiftKey ||
              e.metaKey ||
              (e.preventDefault ? e.preventDefault() : (e.returnValue = !1),
              r[t]()));
      },
      Xe = function(e) {
        e && (G || X || ie || Z) && (e.preventDefault(), e.stopPropagation());
      },
      Ge = function() {
        r.setScrollOffset(0, i.getScrollY());
      },
      Ye = {},
      Ve = 0,
      Qe = function(e) {
        Ye[e] && (Ye[e].raf && O(Ye[e].raf), Ve--, delete Ye[e]);
      },
      Je = function(e) {
        Ye[e] && Qe(e), Ye[e] || (Ve++, (Ye[e] = {}));
      },
      et = function() {
        for (var e in Ye) Ye.hasOwnProperty(e) && Qe(e);
      },
      tt = function(e, t, n, o, i, r, a) {
        var s,
          l = Ie();
        Je(e);
        var u = function() {
          if (Ye[e]) {
            if (((s = Ie() - l), s >= o)) return Qe(e), r(n), void (a && a());
            r((n - t) * i(s / o) + t), (Ye[e].raf = L(u));
          }
        };
        u();
      },
      nt = {
        shout: De,
        listen: Se,
        viewportSize: me,
        options: l,
        isMainScrollAnimating: function() {
          return ie;
        },
        getZoomLevel: function() {
          return y;
        },
        getCurrentIndex: function() {
          return d;
        },
        isDragging: function() {
          return $;
        },
        isZooming: function() {
          return J;
        },
        setScrollOffset: function(e, t) {
          (ve.x = e), (j = ve.y = t), De("updateScrollOffset", ve);
        },
        applyZoomPan: function(e, t, n) {
          (he.x = t), (he.y = n), (y = e), Ae();
        },
        init: function() {
          if (!u && !c) {
            var n;
            (r.framework = i),
              (r.template = e),
              (r.bg = i.getChildByClass(e, "pswp__bg")),
              (M = e.className),
              (u = !0),
              (H = i.detectFeatures()),
              (L = H.raf),
              (O = H.caf),
              (F = H.transform),
              (P = H.oldIE),
              (r.scrollWrap = i.getChildByClass(e, "pswp__scroll-wrap")),
              (r.container = i.getChildByClass(
                r.scrollWrap,
                "pswp__container"
              )),
              (f = r.container.style),
              (r.itemHolders = E = [
                { el: r.container.children[0], wrap: 0, index: -1 },
                { el: r.container.children[1], wrap: 0, index: -1 },
                { el: r.container.children[2], wrap: 0, index: -1 }
              ]),
              (E[0].el.style.display = E[2].el.style.display = "none"),
              $e(),
              (v = {
                resize: r.updateSize,
                scroll: Ge,
                keydown: Ke,
                click: Xe
              });
            var o = H.isOldIOSPhone || H.isOldAndroid || H.isMobileOpera;
            for (
              (H.animationName && H.transform && !o) ||
                (l.showAnimationDuration = l.hideAnimationDuration = 0),
                n = 0;
              n < be.length;
              n++
            )
              r["init" + be[n]]();
            if (t) {
              var a = (r.ui = new t(r, i));
              a.init();
            }
            De("firstUpdate"),
              (d = d || l.index || 0),
              (isNaN(d) || d < 0 || d >= Qt()) && (d = 0),
              (r.currItem = Vt(d)),
              (H.isOldIOSPhone || H.isOldAndroid) && (we = !1),
              e.setAttribute("aria-hidden", "false"),
              l.modal &&
                (we
                  ? (e.style.position = "fixed")
                  : ((e.style.position = "absolute"),
                    (e.style.top = i.getScrollY() + "px"))),
              void 0 === j && (De("initialLayout"), (j = R = i.getScrollY()));
            var p = "pswp--open ";
            for (
              l.mainClass && (p += l.mainClass + " "),
                l.showHideOpacity && (p += "pswp--animate_opacity "),
                p += _ ? "pswp--touch" : "pswp--notouch",
                p += H.animationName ? " pswp--css_animation" : "",
                p += H.svg ? " pswp--svg" : "",
                i.addClass(e, p),
                r.updateSize(),
                h = -1,
                xe = null,
                n = 0;
              n < s;
              n++
            )
              Le((n + h) * ye.x, E[n].el.style);
            P || i.bind(r.scrollWrap, g, r),
              Se("initialZoomInEnd", function() {
                r.setContent(E[0], d - 1),
                  r.setContent(E[2], d + 1),
                  (E[0].el.style.display = E[2].el.style.display = "block"),
                  l.focus && e.focus(),
                  qe();
              }),
              r.setContent(E[1], d),
              r.updateCurrItem(),
              De("afterInit"),
              we ||
                (T = setInterval(function() {
                  Ve ||
                    $ ||
                    J ||
                    y !== r.currItem.initialZoomLevel ||
                    r.updateSize();
                }, 1e3)),
              i.addClass(e, "pswp--visible");
          }
        },
        close: function() {
          u &&
            ((u = !1),
            (c = !0),
            De("close"),
            ze(),
            en(r.currItem, null, !0, r.destroy));
        },
        destroy: function() {
          De("destroy"),
            Kt && clearTimeout(Kt),
            e.setAttribute("aria-hidden", "true"),
            (e.className = M),
            T && clearInterval(T),
            i.unbind(r.scrollWrap, g, r),
            i.unbind(window, "scroll", r),
            Ct(),
            et(),
            (ke = null);
        },
        panTo: function(e, t, n) {
          n ||
            (e > ne.min.x ? (e = ne.min.x) : e < ne.max.x && (e = ne.max.x),
            t > ne.min.y ? (t = ne.min.y) : t < ne.max.y && (t = ne.max.y)),
            (he.x = e),
            (he.y = t),
            Ae();
        },
        handleEvent: function(e) {
          (e = e || window.event), v[e.type] && v[e.type](e);
        },
        goTo: function(e) {
          e = Ee(e);
          var t = e - d;
          (xe = t),
            (d = e),
            (r.currItem = Vt(d)),
            (ge -= t),
            Oe(ye.x * ge),
            et(),
            (ie = !1),
            r.updateCurrItem();
        },
        next: function() {
          r.goTo(d + 1);
        },
        prev: function() {
          r.goTo(d - 1);
        },
        updateCurrZoomItem: function(e) {
          if ((e && De("beforeChange", 0), E[1].el.children.length)) {
            var t = E[1].el.children[0];
            oe = i.hasClass(t, "pswp__zoom-wrap") ? t.style : null;
          } else oe = null;
          (ne = r.currItem.bounds),
            (x = y = r.currItem.initialZoomLevel),
            (he.x = ne.center.x),
            (he.y = ne.center.y),
            e && De("afterChange");
        },
        invalidateCurrItems: function() {
          C = !0;
          for (var e = 0; e < s; e++) E[e].item && (E[e].item.needsUpdate = !0);
        },
        updateCurrItem: function(e) {
          if (0 !== xe) {
            var t,
              n = Math.abs(xe);
            if (!(e && n < 2)) {
              (r.currItem = Vt(d)),
                De("beforeChange", xe),
                n >= s && ((h += xe + (xe > 0 ? -s : s)), (n = s));
              for (var o = 0; o < n; o++)
                xe > 0
                  ? ((t = E.shift()),
                    (E[s - 1] = t),
                    h++,
                    Le((h + 2) * ye.x, t.el.style),
                    r.setContent(t, d - n + o + 1 + 1))
                  : ((t = E.pop()),
                    E.unshift(t),
                    h--,
                    Le(h * ye.x, t.el.style),
                    r.setContent(t, d + n - o - 1 - 1));
              if (oe && 1 === Math.abs(xe)) {
                var i = Vt(k);
                i.initialZoomLevel !== y && (sn(i, me), _e(i));
              }
              (xe = 0), r.updateCurrZoomItem(), (k = d), De("afterChange");
            }
          }
        },
        updateSize: function(t) {
          if (!we && l.modal) {
            var n = i.getScrollY();
            if (
              (j !== n && ((e.style.top = n + "px"), (j = n)),
              !t && Te.x === window.innerWidth && Te.y === window.innerHeight)
            )
              return;
            (Te.x = window.innerWidth),
              (Te.y = window.innerHeight),
              (e.style.height = Te.y + "px");
          }
          if (
            ((me.x = r.scrollWrap.clientWidth),
            (me.y = r.scrollWrap.clientHeight),
            Ge(),
            (ye.x = me.x + Math.round(me.x * l.spacing)),
            (ye.y = me.y),
            Oe(ye.x * ge),
            De("beforeResize"),
            void 0 !== h)
          ) {
            for (var o, a, u, c = 0; c < s; c++)
              (o = E[c]),
                Le((c + h) * ye.x, o.el.style),
                (u = d + c - 1),
                l.loop && Qt() > 2 && (u = Ee(u)),
                (a = Vt(u)),
                a && (C || a.needsUpdate || !a.bounds)
                  ? (r.cleanSlide(a),
                    r.setContent(o, u),
                    1 === c && ((r.currItem = a), r.updateCurrZoomItem(!0)),
                    (a.needsUpdate = !1))
                  : o.index === -1 && u >= 0 && r.setContent(o, u),
                a && a.container && (sn(a, me), _e(a));
            C = !1;
          }
          (x = y = r.currItem.initialZoomLevel),
            (ne = r.currItem.bounds),
            ne && ((he.x = ne.center.x), (he.y = ne.center.y), Ae()),
            De("resize");
        },
        zoomTo: function(e, t, n, o, r) {
          t &&
            ((x = y),
            (yt.x = Math.abs(t.x) - he.x),
            (yt.y = Math.abs(t.y) - he.y),
            Re(fe, he));
          var a = We(e, !1),
            s = {};
          Ue("x", a, s, e), Ue("y", a, s, e);
          var l = y,
            u = { x: he.x, y: he.y };
          Pe(s);
          var c = function(t) {
            1 === t
              ? ((y = e), (he.x = s.x), (he.y = s.y))
              : ((y = (e - l) * t + l),
                (he.x = (s.x - u.x) * t + u.x),
                (he.y = (s.y - u.y) * t + u.y)),
              r && r(t),
              Ae();
          };
          n ? tt("customZoomTo", 0, 1, n, o || i.easing.sine.inOut, c) : c(1);
        }
      },
      ot = 30,
      it = 10,
      rt = {},
      at = {},
      st = {},
      lt = {},
      ut = {},
      ct = [],
      pt = {},
      dt = [],
      ft = {},
      ht = 0,
      mt = pe(),
      gt = 0,
      vt = pe(),
      yt = pe(),
      xt = pe(),
      wt = function(e, t) {
        return e.x === t.x && e.y === t.y;
      },
      bt = function(e, t) {
        return Math.abs(e.x - t.x) < a && Math.abs(e.y - t.y) < a;
      },
      Tt = function(e, t) {
        return (
          (ft.x = Math.abs(e.x - t.x)),
          (ft.y = Math.abs(e.y - t.y)),
          Math.sqrt(ft.x * ft.x + ft.y * ft.y)
        );
      },
      Ct = function() {
        Y && (O(Y), (Y = null));
      },
      Et = function() {
        $ && ((Y = L(Et)), qt());
      },
      kt = function() {
        return !("fit" === l.scaleMode && y === r.currItem.initialZoomLevel);
      },
      St = function(e, t) {
        return (
          !!e &&
          !(e.className && e.className.indexOf("pswp__scroll-wrap") > -1) &&
            (t(e) ? e : St(e.parentNode, t))
        );
      },
      Dt = {},
      It = function(e, t) {
        return (
          (Dt.prevent = !St(e.target, l.isClickableElement)),
          De("preventDragEvent", e, t, Dt),
          Dt.prevent
        );
      },
      Nt = function(e, t) {
        return (t.x = e.pageX), (t.y = e.pageY), (t.id = e.identifier), t;
      },
      Ft = function(e, t, n) {
        (n.x = 0.5 * (e.x + t.x)), (n.y = 0.5 * (e.y + t.y));
      },
      At = function(e, t, n) {
        if (e - z > 50) {
          var o = dt.length > 2 ? dt.shift() : {};
          (o.x = t), (o.y = n), dt.push(o), (z = e);
        }
      },
      _t = function() {
        var e = he.y - r.currItem.initialPosition.y;
        return 1 - Math.abs(e / (me.y / 2));
      },
      Lt = {},
      Ot = {},
      Mt = [],
      Rt = function(e) {
        for (; Mt.length > 0; ) Mt.pop();
        return (
          A
            ? ((ce = 0),
              ct.forEach(function(e) {
                0 === ce ? (Mt[0] = e) : 1 === ce && (Mt[1] = e), ce++;
              }))
            : e.type.indexOf("touch") > -1
            ? e.touches &&
              e.touches.length > 0 &&
              ((Mt[0] = Nt(e.touches[0], Lt)),
              e.touches.length > 1 && (Mt[1] = Nt(e.touches[1], Ot)))
            : ((Lt.x = e.pageX), (Lt.y = e.pageY), (Lt.id = ""), (Mt[0] = Lt)),
          Mt
        );
      },
      Pt = function(e, t) {
        var n,
          o,
          i,
          a,
          s = 0,
          u = he[e] + t[e],
          c = t[e] > 0,
          p = vt.x + t.x,
          d = vt.x - pt.x;
        return (
          (n = u > ne.min[e] || u < ne.max[e] ? l.panEndFriction : 1),
          (u = he[e] + t[e] * n),
          (!l.allowPanToNext && y !== r.currItem.initialZoomLevel) ||
          (oe
            ? "h" !== re ||
              "x" !== e ||
              X ||
              (c
                ? (u > ne.min[e] &&
                    ((n = l.panEndFriction),
                    (s = ne.min[e] - u),
                    (o = ne.min[e] - fe[e])),
                  (o <= 0 || d < 0) && Qt() > 1
                    ? ((a = p), d < 0 && p > pt.x && (a = pt.x))
                    : ne.min.x !== ne.max.x && (i = u))
                : (u < ne.max[e] &&
                    ((n = l.panEndFriction),
                    (s = u - ne.max[e]),
                    (o = fe[e] - ne.max[e])),
                  (o <= 0 || d > 0) && Qt() > 1
                    ? ((a = p), d > 0 && p < pt.x && (a = pt.x))
                    : ne.min.x !== ne.max.x && (i = u)))
            : (a = p),
          "x" !== e)
            ? void (ie || V || (y > r.currItem.fitRatio && (he[e] += t[e] * n)))
            : (void 0 !== a && (Oe(a, !0), (V = a !== pt.x)),
              ne.min.x !== ne.max.x &&
                (void 0 !== i ? (he.x = i) : V || (he.x += t.x * n)),
              void 0 !== a)
        );
      },
      jt = function(e) {
        if (!("mousedown" === e.type && e.button > 0)) {
          if (Yt) return void e.preventDefault();
          if (!U || "mousedown" !== e.type) {
            if ((It(e, !0) && e.preventDefault(), De("pointerDown"), A)) {
              var t = i.arraySearch(ct, e.pointerId, "id");
              t < 0 && (t = ct.length),
                (ct[t] = { x: e.pageX, y: e.pageY, id: e.pointerId });
            }
            var n = Rt(e),
              o = n.length;
            (Q = null),
              et(),
              ($ && 1 !== o) ||
                (($ = ae = !0),
                i.bind(window, m, r),
                (B = ue = se = Z = V = G = K = X = !1),
                (re = null),
                De("firstTouchStart", n),
                Re(fe, he),
                (de.x = de.y = 0),
                Re(lt, n[0]),
                Re(ut, lt),
                (pt.x = ye.x * ge),
                (dt = [{ x: lt.x, y: lt.y }]),
                (z = q = Ie()),
                We(y, !0),
                Ct(),
                Et()),
              !J &&
                o > 1 &&
                !ie &&
                !V &&
                ((x = y),
                (X = !1),
                (J = K = !0),
                (de.y = de.x = 0),
                Re(fe, he),
                Re(rt, n[0]),
                Re(at, n[1]),
                Ft(rt, at, xt),
                (yt.x = Math.abs(xt.x) - he.x),
                (yt.y = Math.abs(xt.y) - he.y),
                (ee = te = Tt(rt, at)));
          }
        }
      },
      Ht = function(e) {
        if ((e.preventDefault(), A)) {
          var t = i.arraySearch(ct, e.pointerId, "id");
          if (t > -1) {
            var n = ct[t];
            (n.x = e.pageX), (n.y = e.pageY);
          }
        }
        if ($) {
          var o = Rt(e);
          if (re || G || J) Q = o;
          else {
            var r = Math.abs(o[0].x - lt.x) - Math.abs(o[0].y - lt.y);
            Math.abs(r) >= it && ((re = r > 0 ? "h" : "v"), (Q = o));
          }
        }
      },
      qt = function() {
        if (Q) {
          var e = Q.length;
          if (0 !== e)
            if (
              (Re(rt, Q[0]),
              (st.x = rt.x - lt.x),
              (st.y = rt.y - lt.y),
              J && e > 1)
            ) {
              if (
                ((lt.x = rt.x), (lt.y = rt.y), !st.x && !st.y && wt(Q[1], at))
              )
                return;
              Re(at, Q[1]), X || ((X = !0), De("zoomGestureStarted"));
              var t = Tt(rt, at),
                n = Ut(t);
              n >
                r.currItem.initialZoomLevel +
                  r.currItem.initialZoomLevel / 15 && (ue = !0);
              var o = 1,
                i = Be(),
                a = Ze();
              if (n < i)
                if (l.pinchToClose && !ue && x <= r.currItem.initialZoomLevel) {
                  var s = i - n,
                    u = 1 - s / (i / 1.2);
                  Ne(u), De("onPinchClose", u), (se = !0);
                } else
                  (o = (i - n) / i), o > 1 && (o = 1), (n = i - o * (i / 3));
              else
                n > a &&
                  ((o = (n - a) / (6 * i)), o > 1 && (o = 1), (n = a + o * i));
              o < 0 && (o = 0),
                (ee = t),
                Ft(rt, at, mt),
                (de.x += mt.x - xt.x),
                (de.y += mt.y - xt.y),
                Re(xt, mt),
                (he.x = Me("x", n)),
                (he.y = Me("y", n)),
                (B = n > y),
                (y = n),
                Ae();
            } else {
              if (!re) return;
              if (
                (ae &&
                  ((ae = !1),
                  Math.abs(st.x) >= it && (st.x -= Q[0].x - ut.x),
                  Math.abs(st.y) >= it && (st.y -= Q[0].y - ut.y)),
                (lt.x = rt.x),
                (lt.y = rt.y),
                0 === st.x && 0 === st.y)
              )
                return;
              if ("v" === re && l.closeOnVerticalDrag && !kt()) {
                (de.y += st.y), (he.y += st.y);
                var c = _t();
                return (Z = !0), De("onVerticalDrag", c), Ne(c), void Ae();
              }
              At(Ie(), rt.x, rt.y), (G = !0), (ne = r.currItem.bounds);
              var p = Pt("x", st);
              p || (Pt("y", st), Pe(he), Ae());
            }
        }
      },
      zt = function(e) {
        if (H.isOldAndroid) {
          if (U && "mouseup" === e.type) return;
          e.type.indexOf("touch") > -1 &&
            (clearTimeout(U),
            (U = setTimeout(function() {
              U = 0;
            }, 600)));
        }
        De("pointerUp"), It(e, !1) && e.preventDefault();
        var t;
        if (A) {
          var n = i.arraySearch(ct, e.pointerId, "id");
          if (n > -1)
            if (((t = ct.splice(n, 1)[0]), navigator.pointerEnabled))
              t.type = e.pointerType || "mouse";
            else {
              var o = { 4: "mouse", 2: "touch", 3: "pen" };
              (t.type = o[e.pointerType]),
                t.type || (t.type = e.pointerType || "mouse");
            }
        }
        var a,
          s = Rt(e),
          u = s.length;
        if (("mouseup" === e.type && (u = 0), 2 === u)) return (Q = null), !0;
        1 === u && Re(ut, s[0]),
          0 !== u ||
            re ||
            ie ||
            (t ||
              ("mouseup" === e.type
                ? (t = { x: e.pageX, y: e.pageY, type: "mouse" })
                : e.changedTouches &&
                  e.changedTouches[0] &&
                  (t = {
                    x: e.changedTouches[0].pageX,
                    y: e.changedTouches[0].pageY,
                    type: "touch"
                  })),
            De("touchRelease", e, t));
        var c = -1;
        if (
          (0 === u &&
            (($ = !1),
            i.unbind(window, m, r),
            Ct(),
            J ? (c = 0) : gt !== -1 && (c = Ie() - gt)),
          (gt = 1 === u ? Ie() : -1),
          (a = c !== -1 && c < 150 ? "zoom" : "swipe"),
          J &&
            u < 2 &&
            ((J = !1),
            1 === u && (a = "zoomPointerUp"),
            De("zoomGestureEnded")),
          (Q = null),
          G || X || ie || Z)
        )
          if ((et(), W || (W = Wt()), W.calculateSwipeSpeed("x"), Z)) {
            var p = _t();
            if (p < l.verticalDragRange) r.close();
            else {
              var d = he.y,
                f = le;
              tt("verticalDrag", 0, 1, 300, i.easing.cubic.out, function(e) {
                (he.y = (r.currItem.initialPosition.y - d) * e + d),
                  Ne((1 - f) * e + f),
                  Ae();
              }),
                De("onVerticalDrag", 1);
            }
          } else {
            if ((V || ie) && 0 === u) {
              var h = Zt(a, W);
              if (h) return;
              a = "zoomPointerUp";
            }
            if (!ie)
              return "swipe" !== a
                ? void $t()
                : void (!V && y > r.currItem.fitRatio && Bt(W));
          }
      },
      Wt = function() {
        var e,
          t,
          n = {
            lastFlickOffset: {},
            lastFlickDist: {},
            lastFlickSpeed: {},
            slowDownRatio: {},
            slowDownRatioReverse: {},
            speedDecelerationRatio: {},
            speedDecelerationRatioAbs: {},
            distanceOffset: {},
            backAnimDestination: {},
            backAnimStarted: {},
            calculateSwipeSpeed: function(o) {
              dt.length > 1
                ? ((e = Ie() - z + 50), (t = dt[dt.length - 2][o]))
                : ((e = Ie() - q), (t = ut[o])),
                (n.lastFlickOffset[o] = lt[o] - t),
                (n.lastFlickDist[o] = Math.abs(n.lastFlickOffset[o])),
                n.lastFlickDist[o] > 20
                  ? (n.lastFlickSpeed[o] = n.lastFlickOffset[o] / e)
                  : (n.lastFlickSpeed[o] = 0),
                Math.abs(n.lastFlickSpeed[o]) < 0.1 &&
                  (n.lastFlickSpeed[o] = 0),
                (n.slowDownRatio[o] = 0.95),
                (n.slowDownRatioReverse[o] = 1 - n.slowDownRatio[o]),
                (n.speedDecelerationRatio[o] = 1);
            },
            calculateOverBoundsAnimOffset: function(e, t) {
              n.backAnimStarted[e] ||
                (he[e] > ne.min[e]
                  ? (n.backAnimDestination[e] = ne.min[e])
                  : he[e] < ne.max[e] && (n.backAnimDestination[e] = ne.max[e]),
                void 0 !== n.backAnimDestination[e] &&
                  ((n.slowDownRatio[e] = 0.7),
                  (n.slowDownRatioReverse[e] = 1 - n.slowDownRatio[e]),
                  n.speedDecelerationRatioAbs[e] < 0.05 &&
                    ((n.lastFlickSpeed[e] = 0),
                    (n.backAnimStarted[e] = !0),
                    tt(
                      "bounceZoomPan" + e,
                      he[e],
                      n.backAnimDestination[e],
                      t || 300,
                      i.easing.sine.out,
                      function(t) {
                        (he[e] = t), Ae();
                      }
                    ))));
            },
            calculateAnimOffset: function(e) {
              n.backAnimStarted[e] ||
                ((n.speedDecelerationRatio[e] =
                  n.speedDecelerationRatio[e] *
                  (n.slowDownRatio[e] +
                    n.slowDownRatioReverse[e] -
                    (n.slowDownRatioReverse[e] * n.timeDiff) / 10)),
                (n.speedDecelerationRatioAbs[e] = Math.abs(
                  n.lastFlickSpeed[e] * n.speedDecelerationRatio[e]
                )),
                (n.distanceOffset[e] =
                  n.lastFlickSpeed[e] *
                  n.speedDecelerationRatio[e] *
                  n.timeDiff),
                (he[e] += n.distanceOffset[e]));
            },
            panAnimLoop: function() {
              if (
                Ye.zoomPan &&
                ((Ye.zoomPan.raf = L(n.panAnimLoop)),
                (n.now = Ie()),
                (n.timeDiff = n.now - n.lastNow),
                (n.lastNow = n.now),
                n.calculateAnimOffset("x"),
                n.calculateAnimOffset("y"),
                Ae(),
                n.calculateOverBoundsAnimOffset("x"),
                n.calculateOverBoundsAnimOffset("y"),
                n.speedDecelerationRatioAbs.x < 0.05 &&
                  n.speedDecelerationRatioAbs.y < 0.05)
              )
                return (
                  (he.x = Math.round(he.x)),
                  (he.y = Math.round(he.y)),
                  Ae(),
                  void Qe("zoomPan")
                );
            }
          };
        return n;
      },
      Bt = function(e) {
        return (
          e.calculateSwipeSpeed("y"),
          (ne = r.currItem.bounds),
          (e.backAnimDestination = {}),
          (e.backAnimStarted = {}),
          Math.abs(e.lastFlickSpeed.x) <= 0.05 &&
          Math.abs(e.lastFlickSpeed.y) <= 0.05
            ? ((e.speedDecelerationRatioAbs.x = e.speedDecelerationRatioAbs.y = 0),
              e.calculateOverBoundsAnimOffset("x"),
              e.calculateOverBoundsAnimOffset("y"),
              !0)
            : (Je("zoomPan"), (e.lastNow = Ie()), void e.panAnimLoop())
        );
      },
      Zt = function(e, t) {
        var n;
        ie || (ht = d);
        var o;
        if ("swipe" === e) {
          var a = lt.x - ut.x,
            s = t.lastFlickDist.x < 10;
          a > ot && (s || t.lastFlickOffset.x > 20)
            ? (o = -1)
            : a < -ot && (s || t.lastFlickOffset.x < -20) && (o = 1);
        }
        var u;
        o &&
          ((d += o),
          d < 0
            ? ((d = l.loop ? Qt() - 1 : 0), (u = !0))
            : d >= Qt() && ((d = l.loop ? 0 : Qt() - 1), (u = !0)),
          (u && !l.loop) || ((xe += o), (ge -= o), (n = !0)));
        var c,
          p = ye.x * ge,
          f = Math.abs(p - vt.x);
        return (
          n || p > vt.x == t.lastFlickSpeed.x > 0
            ? ((c =
                Math.abs(t.lastFlickSpeed.x) > 0
                  ? f / Math.abs(t.lastFlickSpeed.x)
                  : 333),
              (c = Math.min(c, 400)),
              (c = Math.max(c, 250)))
            : (c = 333),
          ht === d && (n = !1),
          (ie = !0),
          De("mainScrollAnimStart"),
          tt("mainScroll", vt.x, p, c, i.easing.cubic.out, Oe, function() {
            et(),
              (ie = !1),
              (ht = -1),
              (n || ht !== d) && r.updateCurrItem(),
              De("mainScrollAnimComplete");
          }),
          n && r.updateCurrItem(!0),
          n
        );
      },
      Ut = function(e) {
        return (1 / te) * e * x;
      },
      $t = function() {
        var e = y,
          t = Be(),
          n = Ze();
        y < t ? (e = t) : y > n && (e = n);
        var o,
          a = 1,
          s = le;
        return se && !B && !ue && y < t
          ? (r.close(), !0)
          : (se &&
              (o = function(e) {
                Ne((a - s) * e + s);
              }),
            r.zoomTo(e, 0, 300, i.easing.cubic.out, o),
            !0);
      };
    Ce("Gestures", {
      publicMethods: {
        initGestures: function() {
          var e = function(e, t, n, o, i) {
            (S = e + t), (D = e + n), (I = e + o), (N = i ? e + i : "");
          };
          (A = H.pointerEvent),
            A && H.touch && (H.touch = !1),
            A
              ? navigator.pointerEnabled
                ? e("pointer", "down", "move", "up", "cancel")
                : e("MSPointer", "Down", "Move", "Up", "Cancel")
              : H.touch
              ? (e("touch", "start", "move", "end", "cancel"), (_ = !0))
              : e("mouse", "down", "move", "up"),
            (m = D + " " + I + " " + N),
            (g = S),
            A &&
              !_ &&
              (_ =
                navigator.maxTouchPoints > 1 || navigator.msMaxTouchPoints > 1),
            (r.likelyTouchDevice = _),
            (v[S] = jt),
            (v[D] = Ht),
            (v[I] = zt),
            N && (v[N] = v[I]),
            H.touch &&
              ((g += " mousedown"),
              (m += " mousemove mouseup"),
              (v.mousedown = v[S]),
              (v.mousemove = v[D]),
              (v.mouseup = v[I])),
            _ || (l.allowPanToNext = !1);
        }
      }
    });
    var Kt,
      Xt,
      Gt,
      Yt,
      Vt,
      Qt,
      Jt,
      en = function(t, n, o, a) {
        Kt && clearTimeout(Kt), (Yt = !0), (Gt = !0);
        var s;
        t.initialLayout
          ? ((s = t.initialLayout), (t.initialLayout = null))
          : (s = l.getThumbBoundsFn && l.getThumbBoundsFn(d));
        var u = o ? l.hideAnimationDuration : l.showAnimationDuration,
          c = function() {
            Qe("initialZoom"),
              o
                ? (r.template.removeAttribute("style"),
                  r.bg.removeAttribute("style"))
                : (Ne(1),
                  n && (n.style.display = "block"),
                  i.addClass(e, "pswp--animated-in"),
                  De("initialZoom" + (o ? "OutEnd" : "InEnd"))),
              a && a(),
              (Yt = !1);
          };
        if (!u || !s || void 0 === s.x) {
          var f = function() {
            De("initialZoom" + (o ? "Out" : "In")),
              (y = t.initialZoomLevel),
              Re(he, t.initialPosition),
              Ae(),
              (e.style.opacity = o ? 0 : 1),
              Ne(1),
              c();
          };
          return void f();
        }
        var h = function() {
          var n = p,
            a = !r.currItem.src || r.currItem.loadError || l.showHideOpacity;
          t.miniImg && (t.miniImg.style.webkitBackfaceVisibility = "hidden"),
            o ||
              ((y = s.w / t.w),
              (he.x = s.x),
              (he.y = s.y - R),
              (r[a ? "template" : "bg"].style.opacity = 0.001),
              Ae()),
            Je("initialZoom"),
            o && !n && i.removeClass(e, "pswp--animated-in"),
            a &&
              (o
                ? i[(n ? "remove" : "add") + "Class"](
                    e,
                    "pswp--animate_opacity"
                  )
                : setTimeout(function() {
                    i.addClass(e, "pswp--animate_opacity");
                  }, 30)),
            (Kt = setTimeout(
              function() {
                if ((De("initialZoom" + (o ? "Out" : "In")), o)) {
                  var r = s.w / t.w,
                    l = { x: he.x, y: he.y },
                    p = y,
                    d = le,
                    f = function(t) {
                      1 === t
                        ? ((y = r), (he.x = s.x), (he.y = s.y - j))
                        : ((y = (r - p) * t + p),
                          (he.x = (s.x - l.x) * t + l.x),
                          (he.y = (s.y - j - l.y) * t + l.y)),
                        Ae(),
                        a ? (e.style.opacity = 1 - t) : Ne(d - t * d);
                    };
                  n
                    ? tt("initialZoom", 0, 1, u, i.easing.cubic.out, f, c)
                    : (f(1), (Kt = setTimeout(c, u + 20)));
                } else
                  (y = t.initialZoomLevel),
                    Re(he, t.initialPosition),
                    Ae(),
                    Ne(1),
                    a ? (e.style.opacity = 1) : Ne(1),
                    (Kt = setTimeout(c, u + 20));
              },
              o ? 25 : 90
            ));
        };
        h();
      },
      tn = {},
      nn = [],
      on = {
        index: 0,
        errorMsg:
          '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
        forceProgressiveLoading: !1,
        preload: [1, 1],
        getNumItemsFn: function() {
          return Xt.length;
        }
      },
      rn = function() {
        return {
          center: { x: 0, y: 0 },
          max: { x: 0, y: 0 },
          min: { x: 0, y: 0 }
        };
      },
      an = function(e, t, n) {
        var o = e.bounds;
        (o.center.x = Math.round((tn.x - t) / 2)),
          (o.center.y = Math.round((tn.y - n) / 2) + e.vGap.top),
          (o.max.x = t > tn.x ? Math.round(tn.x - t) : o.center.x),
          (o.max.y = n > tn.y ? Math.round(tn.y - n) + e.vGap.top : o.center.y),
          (o.min.x = t > tn.x ? 0 : o.center.x),
          (o.min.y = n > tn.y ? e.vGap.top : o.center.y);
      },
      sn = function(e, t, n) {
        if (e.src && !e.loadError) {
          var o = !n;
          if (
            (o &&
              (e.vGap || (e.vGap = { top: 0, bottom: 0 }),
              De("parseVerticalMargin", e)),
            (tn.x = t.x),
            (tn.y = t.y - e.vGap.top - e.vGap.bottom),
            o)
          ) {
            var i = tn.x / e.w,
              r = tn.y / e.h;
            e.fitRatio = i < r ? i : r;
            var a = l.scaleMode;
            "orig" === a ? (n = 1) : "fit" === a && (n = e.fitRatio),
              n > 1 && (n = 1),
              (e.initialZoomLevel = n),
              e.bounds || (e.bounds = rn());
          }
          if (!n) return;
          return (
            an(e, e.w * n, e.h * n),
            o &&
              n === e.initialZoomLevel &&
              (e.initialPosition = e.bounds.center),
            e.bounds
          );
        }
        return (
          (e.w = e.h = 0),
          (e.initialZoomLevel = e.fitRatio = 1),
          (e.bounds = rn()),
          (e.initialPosition = e.bounds.center),
          e.bounds
        );
      },
      ln = function(e, t, n, o, i, a) {
        if (!t.loadError) {
          var s,
            u = r.isDragging() && !r.isZooming(),
            c = e === d || r.isMainScrollAnimating() || u;
          !i && (_ || l.alwaysFadeIn) && c && (s = !0),
            o &&
              (s && (o.style.opacity = 0),
              (t.imageAppended = !0),
              pn(o, t.w, t.h),
              n.appendChild(o),
              s &&
                setTimeout(function() {
                  (o.style.opacity = 1),
                    a &&
                      setTimeout(function() {
                        t &&
                          t.loaded &&
                          t.placeholder &&
                          ((t.placeholder.style.display = "none"),
                          (t.placeholder = null));
                      }, 500);
                }, 50));
        }
      },
      un = function(e) {
        (e.loading = !0), (e.loaded = !1);
        var t = (e.img = i.createEl("pswp__img", "img")),
          n = function() {
            (e.loading = !1),
              (e.loaded = !0),
              e.loadComplete ? e.loadComplete(e) : (e.img = null),
              (t.onload = t.onerror = null),
              (t = null);
          };
        return (
          (t.onload = n),
          (t.onerror = function() {
            (e.loadError = !0), n();
          }),
          (t.src = e.src),
          t
        );
      },
      cn = function(e, t) {
        if (e.src && e.loadError && e.container)
          return (
            t && (e.container.innerHTML = ""),
            (e.container.innerHTML = l.errorMsg.replace("%url%", e.src)),
            !0
          );
      },
      pn = function(e, t, n) {
        (e.style.width = t + "px"), (e.style.height = n + "px");
      },
      dn = function() {
        if (nn.length) {
          for (var e, t = 0; t < nn.length; t++)
            (e = nn[t]),
              e.holder.index === e.index &&
                ln(e.index, e.item, e.baseDiv, e.img);
          nn = [];
        }
      };
    Ce("Controller", {
      publicMethods: {
        lazyLoadItem: function(e) {
          e = Ee(e);
          var t = Vt(e);
          !t ||
            t.loaded ||
            t.loading ||
            (De("gettingData", e, t), t.src && un(t));
        },
        initController: function() {
          i.extend(l, on, !0),
            (r.items = Xt = n),
            (Vt = r.getItemAt),
            (Qt = l.getNumItemsFn),
            (Jt = l.loop),
            Qt() < 3 && (l.loop = !1),
            Se("beforeChange", function(e) {
              var t,
                n = l.preload,
                o = null === e || e > 0,
                i = Math.min(n[0], Qt()),
                a = Math.min(n[1], Qt());
              for (t = 1; t <= (o ? a : i); t++) r.lazyLoadItem(d + t);
              for (t = 1; t <= (o ? i : a); t++) r.lazyLoadItem(d - t);
            }),
            Se("initialLayout", function() {
              r.currItem.initialLayout =
                l.getThumbBoundsFn && l.getThumbBoundsFn(d);
            }),
            Se("mainScrollAnimComplete", dn),
            Se("initialZoomInEnd", dn),
            Se("destroy", function() {
              for (var e, t = 0; t < Xt.length; t++)
                (e = Xt[t]),
                  e.container && (e.container = null),
                  e.placeholder && (e.placeholder = null),
                  e.img && (e.img = null),
                  e.preloader && (e.preloader = null),
                  e.loadError && (e.loaded = e.loadError = !1);
              nn = null;
            });
        },
        getItemAt: function(e) {
          return e >= 0 && void 0 !== Xt[e] && Xt[e];
        },
        allowProgressiveImg: function() {
          return (
            l.forceProgressiveLoading ||
            !_ ||
            l.mouseUsed ||
            screen.width > 1200
          );
        },
        setContent: function(e, t) {
          l.loop && (t = Ee(t));
          var n = r.getItemAt(e.index);
          n && (n.container = null);
          var o,
            a = r.getItemAt(t);
          if (!a) return void (e.el.innerHTML = "");
          De("gettingData", t, a), (e.index = t), (e.item = a);
          var s = (a.container = i.createEl("pswp__zoom-wrap"));
          if (
            (!a.src &&
              a.html &&
              (a.html.tagName ? s.appendChild(a.html) : (s.innerHTML = a.html)),
            cn(a),
            !a.src || a.loadError || a.loaded)
          )
            a.src &&
              !a.loadError &&
              ((o = i.createEl("pswp__img", "img")),
              (o.style.webkitBackfaceVisibility = "hidden"),
              (o.style.opacity = 1),
              (o.src = a.src),
              pn(o, a.w, a.h),
              ln(t, a, s, o, !0));
          else {
            if (
              ((a.loadComplete = function(n) {
                if (u) {
                  if (
                    (n.img && (n.img.style.webkitBackfaceVisibility = "hidden"),
                    e && e.index === t)
                  ) {
                    if (cn(n, !0))
                      return (
                        (n.loadComplete = n.img = null),
                        sn(n, me),
                        _e(n),
                        void (e.index === d && r.updateCurrZoomItem())
                      );
                    n.imageAppended
                      ? !Yt &&
                        n.placeholder &&
                        ((n.placeholder.style.display = "none"),
                        (n.placeholder = null))
                      : H.transform && (ie || Yt)
                      ? nn.push({
                          item: n,
                          baseDiv: s,
                          img: n.img,
                          index: t,
                          holder: e
                        })
                      : ln(t, n, s, n.img, ie || Yt);
                  }
                  (n.loadComplete = null),
                    (n.img = null),
                    De("imageLoadComplete", t, n);
                }
              }),
              i.features.transform)
            ) {
              var c = "pswp__img pswp__img--placeholder";
              c += a.msrc ? "" : " pswp__img--placeholder--blank";
              var p = i.createEl(c, a.msrc ? "img" : "");
              a.msrc && (p.src = a.msrc),
                pn(p, a.w, a.h),
                s.appendChild(p),
                (a.placeholder = p);
            }
            a.loading || un(a),
              r.allowProgressiveImg() &&
                (!Gt && H.transform
                  ? nn.push({
                      item: a,
                      baseDiv: s,
                      img: a.img,
                      index: t,
                      holder: e
                    })
                  : ln(t, a, s, a.img, !0, !0));
          }
          sn(a, me),
            Gt || t !== d ? _e(a) : ((oe = s.style), en(a, o || a.img)),
            (e.el.innerHTML = ""),
            e.el.appendChild(s);
        },
        cleanSlide: function(e) {
          e.img && (e.img.onload = e.img.onerror = null),
            (e.loaded = e.loading = e.img = e.imageAppended = !1);
        }
      }
    });
    var fn,
      hn = {},
      mn = function(e, t, n) {
        var o = document.createEvent("CustomEvent"),
          i = {
            origEvent: e,
            target: e.target,
            releasePoint: t,
            pointerType: n || "touch"
          };
        o.initCustomEvent("pswpTap", !0, !0, i), e.target.dispatchEvent(o);
      };
    Ce("Tap", {
      publicMethods: {
        initTap: function() {
          Se("firstTouchStart", r.onTapStart),
            Se("touchRelease", r.onTapRelease),
            Se("destroy", function() {
              (hn = {}), (fn = null);
            });
        },
        onTapStart: function(e) {
          e.length > 1 && (clearTimeout(fn), (fn = null));
        },
        onTapRelease: function(e, t) {
          if (t && !G && !K && !Ve) {
            var n = t;
            if (fn && (clearTimeout(fn), (fn = null), bt(n, hn)))
              return void De("doubleTap", n);
            if ("mouse" === t.type) return void mn(e, t, "mouse");
            var o = e.target.tagName.toUpperCase();
            if ("BUTTON" === o || i.hasClass(e.target, "pswp__single-tap"))
              return void mn(e, t);
            Re(hn, n),
              (fn = setTimeout(function() {
                mn(e, t), (fn = null);
              }, 300));
          }
        }
      }
    });
    var gn;
    Ce("DesktopZoom", {
      publicMethods: {
        initDesktopZoom: function() {
          P ||
            (_
              ? Se("mouseUsed", function() {
                  r.setupDesktopZoom();
                })
              : r.setupDesktopZoom(!0));
        },
        setupDesktopZoom: function(t) {
          gn = {};
          var n = "wheel mousewheel DOMMouseScroll";
          Se("bindEvents", function() {
            i.bind(e, n, r.handleMouseWheel);
          }),
            Se("unbindEvents", function() {
              gn && i.unbind(e, n, r.handleMouseWheel);
            }),
            (r.mouseZoomedIn = !1);
          var o,
            a = function() {
              r.mouseZoomedIn &&
                (i.removeClass(e, "pswp--zoomed-in"), (r.mouseZoomedIn = !1)),
                y < 1
                  ? i.addClass(e, "pswp--zoom-allowed")
                  : i.removeClass(e, "pswp--zoom-allowed"),
                s();
            },
            s = function() {
              o && (i.removeClass(e, "pswp--dragging"), (o = !1));
            };
          Se("resize", a),
            Se("afterChange", a),
            Se("pointerDown", function() {
              r.mouseZoomedIn && ((o = !0), i.addClass(e, "pswp--dragging"));
            }),
            Se("pointerUp", s),
            t || a();
        },
        handleMouseWheel: function(e) {
          if (y <= r.currItem.fitRatio)
            return (
              l.modal &&
                (l.closeOnScroll
                  ? F && Math.abs(e.deltaY) > 2 && ((p = !0), r.close())
                  : e.preventDefault()),
              !0
            );
          if ((e.stopPropagation(), (gn.x = 0), "deltaX" in e))
            1 === e.deltaMode
              ? ((gn.x = 18 * e.deltaX), (gn.y = 18 * e.deltaY))
              : ((gn.x = e.deltaX), (gn.y = e.deltaY));
          else if ("wheelDelta" in e)
            e.wheelDeltaX && (gn.x = -0.16 * e.wheelDeltaX),
              e.wheelDeltaY
                ? (gn.y = -0.16 * e.wheelDeltaY)
                : (gn.y = -0.16 * e.wheelDelta);
          else {
            if (!("detail" in e)) return;
            gn.y = e.detail;
          }
          We(y, !0);
          var t = he.x - gn.x,
            n = he.y - gn.y;
          (l.modal ||
            (t <= ne.min.x &&
              t >= ne.max.x &&
              n <= ne.min.y &&
              n >= ne.max.y)) &&
            e.preventDefault(),
            r.panTo(t, n);
        },
        toggleDesktopZoom: function(t) {
          t = t || { x: me.x / 2 + ve.x, y: me.y / 2 + ve.y };
          var n = l.getDoubleTapZoom(!0, r.currItem),
            o = y === n;
          (r.mouseZoomedIn = !o),
            r.zoomTo(o ? r.currItem.initialZoomLevel : n, t, 333),
            i[(o ? "remove" : "add") + "Class"](e, "pswp--zoomed-in");
        }
      }
    });
    var vn,
      yn,
      xn,
      wn,
      bn,
      Tn,
      Cn,
      En,
      kn,
      Sn,
      Dn,
      In,
      Nn = { history: !0, galleryUID: 1 },
      Fn = function() {
        return Dn.hash.substring(1);
      },
      An = function() {
        vn && clearTimeout(vn), xn && clearTimeout(xn);
      },
      _n = function() {
        var e = Fn(),
          t = {};
        if (e.length < 5) return t;
        var n,
          o = e.split("&");
        for (n = 0; n < o.length; n++)
          if (o[n]) {
            var i = o[n].split("=");
            i.length < 2 || (t[i[0]] = i[1]);
          }
        if (l.galleryPIDs) {
          var r = t.pid;
          for (t.pid = 0, n = 0; n < Xt.length; n++)
            if (Xt[n].pid === r) {
              t.pid = n;
              break;
            }
        } else t.pid = parseInt(t.pid, 10) - 1;
        return t.pid < 0 && (t.pid = 0), t;
      },
      Ln = function() {
        if ((xn && clearTimeout(xn), Ve || $))
          return void (xn = setTimeout(Ln, 500));
        wn ? clearTimeout(yn) : (wn = !0);
        var e = d + 1,
          t = Vt(d);
        t.hasOwnProperty("pid") && (e = t.pid);
        var n = Cn + "&gid=" + l.galleryUID + "&pid=" + e;
        En || (Dn.hash.indexOf(n) === -1 && (Sn = !0));
        var o = Dn.href.split("#")[0] + "#" + n;
        In
          ? "#" + n !== window.location.hash &&
            history[En ? "replaceState" : "pushState"]("", document.title, o)
          : En
          ? Dn.replace(o)
          : (Dn.hash = n),
          (En = !0),
          (yn = setTimeout(function() {
            wn = !1;
          }, 60));
      };
    Ce("History", {
      publicMethods: {
        initHistory: function() {
          if ((i.extend(l, Nn, !0), l.history)) {
            (Dn = window.location),
              (Sn = !1),
              (kn = !1),
              (En = !1),
              (Cn = Fn()),
              (In = "pushState" in history),
              Cn.indexOf("gid=") > -1 &&
                ((Cn = Cn.split("&gid=")[0]), (Cn = Cn.split("?gid=")[0])),
              Se("afterChange", r.updateURL),
              Se("unbindEvents", function() {
                i.unbind(window, "hashchange", r.onHashChange);
              });
            var e = function() {
              (Tn = !0),
                kn ||
                  (Sn
                    ? history.back()
                    : Cn
                    ? (Dn.hash = Cn)
                    : In
                    ? history.pushState(
                        "",
                        document.title,
                        Dn.pathname + Dn.search
                      )
                    : (Dn.hash = "")),
                An();
            };
            Se("unbindEvents", function() {
              p && e();
            }),
              Se("destroy", function() {
                Tn || e();
              }),
              Se("firstUpdate", function() {
                d = _n().pid;
              });
            var t = Cn.indexOf("pid=");
            t > -1 &&
              ((Cn = Cn.substring(0, t)),
              "&" === Cn.slice(-1) && (Cn = Cn.slice(0, -1))),
              setTimeout(function() {
                u && i.bind(window, "hashchange", r.onHashChange);
              }, 40);
          }
        },
        onHashChange: function() {
          return Fn() === Cn
            ? ((kn = !0), void r.close())
            : void (wn || ((bn = !0), r.goTo(_n().pid), (bn = !1)));
        },
        updateURL: function() {
          An(), bn || (En ? (vn = setTimeout(Ln, 800)) : Ln());
        }
      }
    }),
      i.extend(r, nt);
  };
  return e;
}),
  (function(e, t) {
    "function" == typeof define && define.amd
      ? define(t)
      : "object" == typeof exports
      ? (module.exports = t())
      : (e.PhotoSwipeUI_Default = t());
  })(this, function() {
    "use strict";
    var e = function(e, t) {
      var n,
        o,
        i,
        r,
        a,
        s,
        l,
        u,
        c,
        p,
        d,
        f,
        h,
        m,
        g,
        v,
        y,
        x,
        w,
        b = this,
        T = !1,
        C = !0,
        E = !0,
        k = {
          barsSize: { top: 44, bottom: "auto" },
          closeElClasses: ["item", "caption", "zoom-wrap", "ui", "top-bar"],
          timeToIdle: 4e3,
          timeToIdleOutside: 1e3,
          loadingIndicatorDelay: 1e3,
          addCaptionHTMLFn: function(e, t) {
            return e.title
              ? ((t.children[0].innerHTML = e.title), !0)
              : ((t.children[0].innerHTML = ""), !1);
          },
          closeEl: !0,
          captionEl: !0,
          fullscreenEl: !0,
          zoomEl: !0,
          shareEl: !0,
          counterEl: !0,
          arrowEl: !0,
          preloaderEl: !0,
          tapToClose: !1,
          tapToToggleControls: !0,
          clickToCloseNonZoomable: !0,
          shareButtons: [
            {
              id: "facebook",
              label: "Share on Facebook",
              url: "https://www.facebook.com/sharer/sharer.php?u={{url}}"
            },
            {
              id: "twitter",
              label: "Tweet",
              url: "https://twitter.com/intent/tweet?text={{text}}&url={{url}}"
            },
            {
              id: "pinterest",
              label: "Pin it",
              url:
                "http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}"
            },
            {
              id: "download",
              label: "Download image",
              url: "{{raw_image_url}}",
              download: !0
            }
          ],
          getImageURLForShare: function() {
            return e.currItem.src || "";
          },
          getPageURLForShare: function() {
            return window.location.href;
          },
          getTextForShare: function() {
            return e.currItem.title || "";
          },
          indexIndicatorSep: " / "
        },
        S = function(e) {
          if (v) return !0;
          (e = e || window.event), g.timeToIdle && g.mouseUsed && !c && R();
          for (
            var n, o, i = e.target || e.srcElement, r = i.className, a = 0;
            a < Z.length;
            a++
          )
            (n = Z[a]),
              n.onTap &&
                r.indexOf("pswp__" + n.name) > -1 &&
                (n.onTap(), (o = !0));
          if (o) {
            e.stopPropagation && e.stopPropagation(), (v = !0);
            var s = t.features.isOldAndroid ? 600 : 30;
            y = setTimeout(function() {
              v = !1;
            }, s);
          }
        },
        D = function() {
          return !e.likelyTouchDevice || g.mouseUsed || screen.width > 1200;
        },
        I = function(e, n, o) {
          t[(o ? "add" : "remove") + "Class"](e, "pswp__" + n);
        },
        N = function() {
          var e = 1 === g.getNumItemsFn();
          e !== m && (I(o, "ui--one-slide", e), (m = e));
        },
        F = function() {
          I(l, "share-modal--hidden", E);
        },
        A = function() {
          return (
            (E = !E),
            E
              ? (t.removeClass(l, "pswp__share-modal--fade-in"),
                setTimeout(function() {
                  E && F();
                }, 300))
              : (F(),
                setTimeout(function() {
                  E || t.addClass(l, "pswp__share-modal--fade-in");
                }, 30)),
            E || L(),
            !1
          );
        },
        _ = function(t) {
          t = t || window.event;
          var n = t.target || t.srcElement;
          return (
            e.shout("shareLinkClick", t, n),
            !!n.href &&
              (!!n.hasAttribute("download") ||
                (window.open(
                  n.href,
                  "pswp_share",
                  "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=" +
                    (window.screen ? Math.round(screen.width / 2 - 275) : 100)
                ),
                E || A(),
                !1))
          );
        },
        L = function() {
          for (var e, t, n, o, i, r = "", a = 0; a < g.shareButtons.length; a++)
            (e = g.shareButtons[a]),
              (n = g.getImageURLForShare(e)),
              (o = g.getPageURLForShare(e)),
              (i = g.getTextForShare(e)),
              (t = e.url
                .replace("{{url}}", encodeURIComponent(o))
                .replace("{{image_url}}", encodeURIComponent(n))
                .replace("{{raw_image_url}}", n)
                .replace("{{text}}", encodeURIComponent(i))),
              (r +=
                '<a href="' +
                t +
                '" target="_blank" class="pswp__share--' +
                e.id +
                '"' +
                (e.download ? "download" : "") +
                ">" +
                e.label +
                "</a>"),
              g.parseShareButtonOut && (r = g.parseShareButtonOut(e, r));
          (l.children[0].innerHTML = r), (l.children[0].onclick = _);
        },
        O = function(e) {
          for (var n = 0; n < g.closeElClasses.length; n++)
            if (t.hasClass(e, "pswp__" + g.closeElClasses[n])) return !0;
        },
        M = 0,
        R = function() {
          clearTimeout(w), (M = 0), c && b.setIdle(!1);
        },
        P = function(e) {
          e = e ? e : window.event;
          var t = e.relatedTarget || e.toElement;
          (t && "HTML" !== t.nodeName) ||
            (clearTimeout(w),
            (w = setTimeout(function() {
              b.setIdle(!0);
            }, g.timeToIdleOutside)));
        },
        j = function() {
          g.fullscreenEl &&
            (n || (n = b.getFullscreenAPI()),
            n
              ? (t.bind(document, n.eventK, b.updateFullscreen),
                b.updateFullscreen(),
                t.addClass(e.template, "pswp--supports-fs"))
              : t.removeClass(e.template, "pswp--supports-fs"));
        },
        H = function() {
          g.preloaderEl &&
            (q(!0),
            p("beforeChange", function() {
              clearTimeout(h),
                (h = setTimeout(function() {
                  e.currItem && e.currItem.loading
                    ? (!e.allowProgressiveImg() ||
                        (e.currItem.img && !e.currItem.img.naturalWidth)) &&
                      q(!1)
                    : q(!0);
                }, g.loadingIndicatorDelay));
            }),
            p("imageLoadComplete", function(t, n) {
              e.currItem === n && q(!0);
            }));
        },
        q = function(e) {
          f !== e && (I(d, "preloader--active", !e), (f = e));
        },
        z = function(e) {
          var n = e.vGap;
          if (D()) {
            var a = g.barsSize;
            if (g.captionEl && "auto" === a.bottom)
              if (
                (r ||
                  ((r = t.createEl("pswp__caption pswp__caption--fake")),
                  r.appendChild(t.createEl("pswp__caption__center")),
                  o.insertBefore(r, i),
                  t.addClass(o, "pswp__ui--fit")),
                g.addCaptionHTMLFn(e, r, !0))
              ) {
                var s = r.clientHeight;
                n.bottom = parseInt(s, 10) || 44;
              } else n.bottom = a.top;
            else n.bottom = "auto" === a.bottom ? 0 : a.bottom;
            n.top = a.top;
          } else n.top = n.bottom = 0;
        },
        W = function() {
          g.timeToIdle &&
            p("mouseUsed", function() {
              t.bind(document, "mousemove", R),
                t.bind(document, "mouseout", P),
                (x = setInterval(function() {
                  M++, 2 === M && b.setIdle(!0);
                }, g.timeToIdle / 2));
            });
        },
        B = function() {
          p("onVerticalDrag", function(e) {
            C && e < 0.95
              ? b.hideControls()
              : !C && e >= 0.95 && b.showControls();
          });
          var e;
          p("onPinchClose", function(t) {
            C && t < 0.9
              ? (b.hideControls(), (e = !0))
              : e && !C && t > 0.9 && b.showControls();
          }),
            p("zoomGestureEnded", function() {
              (e = !1), e && !C && b.showControls();
            });
        },
        Z = [
          {
            name: "caption",
            option: "captionEl",
            onInit: function(e) {
              i = e;
            }
          },
          {
            name: "share-modal",
            option: "shareEl",
            onInit: function(e) {
              l = e;
            },
            onTap: function() {
              A();
            }
          },
          {
            name: "button--share",
            option: "shareEl",
            onInit: function(e) {
              s = e;
            },
            onTap: function() {
              A();
            }
          },
          {
            name: "button--zoom",
            option: "zoomEl",
            onTap: e.toggleDesktopZoom
          },
          {
            name: "counter",
            option: "counterEl",
            onInit: function(e) {
              a = e;
            }
          },
          { name: "button--close", option: "closeEl", onTap: e.close },
          { name: "button--arrow--left", option: "arrowEl", onTap: e.prev },
          { name: "button--arrow--right", option: "arrowEl", onTap: e.next },
          {
            name: "button--fs",
            option: "fullscreenEl",
            onTap: function() {
              n.isFullscreen() ? n.exit() : n.enter();
            }
          },
          {
            name: "preloader",
            option: "preloaderEl",
            onInit: function(e) {
              d = e;
            }
          }
        ],
        U = function() {
          var e,
            n,
            i,
            r = function(o) {
              if (o)
                for (var r = o.length, a = 0; a < r; a++) {
                  (e = o[a]), (n = e.className);
                  for (var s = 0; s < Z.length; s++)
                    (i = Z[s]),
                      n.indexOf("pswp__" + i.name) > -1 &&
                        (g[i.option]
                          ? (t.removeClass(e, "pswp__element--disabled"),
                            i.onInit && i.onInit(e))
                          : t.addClass(e, "pswp__element--disabled"));
                }
            };
          r(o.children);
          var a = t.getChildByClass(o, "pswp__top-bar");
          a && r(a.children);
        };
      (b.init = function() {
        t.extend(e.options, k, !0),
          (g = e.options),
          (o = t.getChildByClass(e.scrollWrap, "pswp__ui")),
          (p = e.listen),
          B(),
          p("beforeChange", b.update),
          p("doubleTap", function(t) {
            var n = e.currItem.initialZoomLevel;
            e.getZoomLevel() !== n
              ? e.zoomTo(n, t, 333)
              : e.zoomTo(g.getDoubleTapZoom(!1, e.currItem), t, 333);
          }),
          p("preventDragEvent", function(e, t, n) {
            var o = e.target || e.srcElement;
            o &&
              o.className &&
              e.type.indexOf("mouse") > -1 &&
              (o.className.indexOf("__caption") > 0 ||
                /(SMALL|STRONG|EM)/i.test(o.tagName)) &&
              (n.prevent = !1);
          }),
          p("bindEvents", function() {
            t.bind(o, "pswpTap click", S),
              t.bind(e.scrollWrap, "pswpTap", b.onGlobalTap),
              e.likelyTouchDevice ||
                t.bind(e.scrollWrap, "mouseover", b.onMouseOver);
          }),
          p("unbindEvents", function() {
            E || A(),
              x && clearInterval(x),
              t.unbind(document, "mouseout", P),
              t.unbind(document, "mousemove", R),
              t.unbind(o, "pswpTap click", S),
              t.unbind(e.scrollWrap, "pswpTap", b.onGlobalTap),
              t.unbind(e.scrollWrap, "mouseover", b.onMouseOver),
              n &&
                (t.unbind(document, n.eventK, b.updateFullscreen),
                n.isFullscreen() && ((g.hideAnimationDuration = 0), n.exit()),
                (n = null));
          }),
          p("destroy", function() {
            g.captionEl &&
              (r && o.removeChild(r), t.removeClass(i, "pswp__caption--empty")),
              l && (l.children[0].onclick = null),
              t.removeClass(o, "pswp__ui--over-close"),
              t.addClass(o, "pswp__ui--hidden"),
              b.setIdle(!1);
          }),
          g.showAnimationDuration || t.removeClass(o, "pswp__ui--hidden"),
          p("initialZoomIn", function() {
            g.showAnimationDuration && t.removeClass(o, "pswp__ui--hidden");
          }),
          p("initialZoomOut", function() {
            t.addClass(o, "pswp__ui--hidden");
          }),
          p("parseVerticalMargin", z),
          U(),
          g.shareEl && s && l && (E = !0),
          N(),
          W(),
          j(),
          H();
      }),
        (b.setIdle = function(e) {
          (c = e), I(o, "ui--idle", e);
        }),
        (b.update = function() {
          C && e.currItem
            ? (b.updateIndexIndicator(),
              g.captionEl &&
                (g.addCaptionHTMLFn(e.currItem, i),
                I(i, "caption--empty", !e.currItem.title)),
              (T = !0))
            : (T = !1),
            E || A(),
            N();
        }),
        (b.updateFullscreen = function(o) {
          o &&
            setTimeout(function() {
              e.setScrollOffset(0, t.getScrollY());
            }, 50),
            t[(n.isFullscreen() ? "add" : "remove") + "Class"](
              e.template,
              "pswp--fs"
            );
        }),
        (b.updateIndexIndicator = function() {
          g.counterEl &&
            (a.innerHTML =
              e.getCurrentIndex() +
              1 +
              g.indexIndicatorSep +
              g.getNumItemsFn());
        }),
        (b.onGlobalTap = function(n) {
          n = n || window.event;
          var o = n.target || n.srcElement;
          if (!v)
            if (n.detail && "mouse" === n.detail.pointerType) {
              if (O(o)) return void e.close();
              t.hasClass(o, "pswp__img") &&
                (1 === e.getZoomLevel() &&
                e.getZoomLevel() <= e.currItem.fitRatio
                  ? g.clickToCloseNonZoomable && e.close()
                  : e.toggleDesktopZoom(n.detail.releasePoint));
            } else if (
              (g.tapToToggleControls &&
                (C ? b.hideControls() : b.showControls()),
              g.tapToClose && (t.hasClass(o, "pswp__img") || O(o)))
            )
              return void e.close();
        }),
        (b.onMouseOver = function(e) {
          e = e || window.event;
          var t = e.target || e.srcElement;
          I(o, "ui--over-close", O(t));
        }),
        (b.hideControls = function() {
          t.addClass(o, "pswp__ui--hidden"), (C = !1);
        }),
        (b.showControls = function() {
          (C = !0), T || b.update(), t.removeClass(o, "pswp__ui--hidden");
        }),
        (b.supportsFullscreen = function() {
          var e = document;
          return !!(
            e.exitFullscreen ||
            e.mozCancelFullScreen ||
            e.webkitExitFullscreen ||
            e.msExitFullscreen
          );
        }),
        (b.getFullscreenAPI = function() {
          var t,
            n = document.documentElement,
            o = "fullscreenchange";
          return (
            n.requestFullscreen
              ? (t = {
                  enterK: "requestFullscreen",
                  exitK: "exitFullscreen",
                  elementK: "fullscreenElement",
                  eventK: o
                })
              : n.mozRequestFullScreen
              ? (t = {
                  enterK: "mozRequestFullScreen",
                  exitK: "mozCancelFullScreen",
                  elementK: "mozFullScreenElement",
                  eventK: "moz" + o
                })
              : n.webkitRequestFullscreen
              ? (t = {
                  enterK: "webkitRequestFullscreen",
                  exitK: "webkitExitFullscreen",
                  elementK: "webkitFullscreenElement",
                  eventK: "webkit" + o
                })
              : n.msRequestFullscreen &&
                (t = {
                  enterK: "msRequestFullscreen",
                  exitK: "msExitFullscreen",
                  elementK: "msFullscreenElement",
                  eventK: "MSFullscreenChange"
                }),
            t &&
              ((t.enter = function() {
                return (
                  (u = g.closeOnScroll),
                  (g.closeOnScroll = !1),
                  "webkitRequestFullscreen" !== this.enterK
                    ? e.template[this.enterK]()
                    : void e.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT)
                );
              }),
              (t.exit = function() {
                return (g.closeOnScroll = u), document[this.exitK]();
              }),
              (t.isFullscreen = function() {
                return document[this.elementK];
              })),
            t
          );
        });
    };
    return e;
  }),
  (function(e, t) {
    "object" == typeof module && "object" == typeof module.exports
      ? (module.exports = e.document
          ? t(e, !0)
          : function(e) {
              if (!e.document)
                throw new Error("jQuery requires a window with a document");
              return t(e);
            })
      : t(e);
  })("undefined" != typeof window ? window : this, function(e, t) {
    function n(e) {
      var t = "length" in e && e.length,
        n = J.type(e);
      return (
        "function" !== n &&
        !J.isWindow(e) &&
        (!(1 !== e.nodeType || !t) ||
          "array" === n ||
            0 === t ||
            ("number" == typeof t && t > 0 && t - 1 in e))
      );
    }
    function o(e, t, n) {
      if (J.isFunction(t))
        return J.grep(e, function(e, o) {
          return !!t.call(e, o, e) !== n;
        });
      if (t.nodeType)
        return J.grep(e, function(e) {
          return (e === t) !== n;
        });
      if ("string" == typeof t) {
        if (se.test(t)) return J.filter(t, e, n);
        t = J.filter(t, e);
      }
      return J.grep(e, function(e) {
        return $.call(t, e) >= 0 !== n;
      });
    }
    function i(e, t) {
      for (; (e = e[t]) && 1 !== e.nodeType; );
      return e;
    }
    function r(e) {
      var t = (he[e] = {});
      return (
        J.each(e.match(fe) || [], function(e, n) {
          t[n] = !0;
        }),
        t
      );
    }
    function a() {
      V.removeEventListener("DOMContentLoaded", a, !1),
        e.removeEventListener("load", a, !1),
        J.ready();
    }
    function s() {
      Object.defineProperty((this.cache = {}), 0, {
        get: function() {
          return {};
        }
      }),
        (this.expando = J.expando + s.uid++);
    }
    function l(e, t, n) {
      var o;
      if (void 0 === n && 1 === e.nodeType)
        if (
          ((o = "data-" + t.replace(we, "-$1").toLowerCase()),
          (n = e.getAttribute(o)),
          "string" == typeof n)
        ) {
          try {
            n =
              "true" === n ||
              ("false" !== n &&
                ("null" === n
                  ? null
                  : +n + "" === n
                  ? +n
                  : xe.test(n)
                  ? J.parseJSON(n)
                  : n));
          } catch (i) {}
          ye.set(e, t, n);
        } else n = void 0;
      return n;
    }
    function u() {
      return !0;
    }
    function c() {
      return !1;
    }
    function p() {
      try {
        return V.activeElement;
      } catch (e) {}
    }
    function d(e, t) {
      return J.nodeName(e, "table") &&
        J.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr")
        ? e.getElementsByTagName("tbody")[0] ||
            e.appendChild(e.ownerDocument.createElement("tbody"))
        : e;
    }
    function f(e) {
      return (e.type = (null !== e.getAttribute("type")) + "/" + e.type), e;
    }
    function h(e) {
      var t = Re.exec(e.type);
      return t ? (e.type = t[1]) : e.removeAttribute("type"), e;
    }
    function m(e, t) {
      for (var n = 0, o = e.length; n < o; n++)
        ve.set(e[n], "globalEval", !t || ve.get(t[n], "globalEval"));
    }
    function g(e, t) {
      var n, o, i, r, a, s, l, u;
      if (1 === t.nodeType) {
        if (
          ve.hasData(e) &&
          ((r = ve.access(e)), (a = ve.set(t, r)), (u = r.events))
        ) {
          delete a.handle, (a.events = {});
          for (i in u)
            for (n = 0, o = u[i].length; n < o; n++) J.event.add(t, i, u[i][n]);
        }
        ye.hasData(e) &&
          ((s = ye.access(e)), (l = J.extend({}, s)), ye.set(t, l));
      }
    }
    function v(e, t) {
      var n = e.getElementsByTagName
        ? e.getElementsByTagName(t || "*")
        : e.querySelectorAll
        ? e.querySelectorAll(t || "*")
        : [];
      return void 0 === t || (t && J.nodeName(e, t)) ? J.merge([e], n) : n;
    }
    function y(e, t) {
      var n = t.nodeName.toLowerCase();
      "input" === n && Ee.test(e.type)
        ? (t.checked = e.checked)
        : ("input" !== n && "textarea" !== n) ||
          (t.defaultValue = e.defaultValue);
    }
    function x(t, n) {
      var o,
        i = J(n.createElement(t)).appendTo(n.body),
        r =
          e.getDefaultComputedStyle && (o = e.getDefaultComputedStyle(i[0]))
            ? o.display
            : J.css(i[0], "display");
      return i.detach(), r;
    }
    function w(e) {
      var t = V,
        n = qe[e];
      return (
        n ||
          ((n = x(e, t)),
          ("none" !== n && n) ||
            ((He = (
              He || J("<iframe frameborder='0' width='0' height='0'/>")
            ).appendTo(t.documentElement)),
            (t = He[0].contentDocument),
            t.write(),
            t.close(),
            (n = x(e, t)),
            He.detach()),
          (qe[e] = n)),
        n
      );
    }
    function b(e, t, n) {
      var o,
        i,
        r,
        a,
        s = e.style;
      return (
        (n = n || Be(e)),
        n && (a = n.getPropertyValue(t) || n[t]),
        n &&
          ("" !== a || J.contains(e.ownerDocument, e) || (a = J.style(e, t)),
          We.test(a) &&
            ze.test(t) &&
            ((o = s.width),
            (i = s.minWidth),
            (r = s.maxWidth),
            (s.minWidth = s.maxWidth = s.width = a),
            (a = n.width),
            (s.width = o),
            (s.minWidth = i),
            (s.maxWidth = r))),
        void 0 !== a ? a + "" : a
      );
    }
    function T(e, t) {
      return {
        get: function() {
          return e()
            ? void delete this.get
            : (this.get = t).apply(this, arguments);
        }
      };
    }
    function C(e, t) {
      if (t in e) return t;
      for (var n = t[0].toUpperCase() + t.slice(1), o = t, i = Ge.length; i--; )
        if (((t = Ge[i] + n), t in e)) return t;
      return o;
    }
    function E(e, t, n) {
      var o = Ue.exec(t);
      return o ? Math.max(0, o[1] - (n || 0)) + (o[2] || "px") : t;
    }
    function k(e, t, n, o, i) {
      for (
        var r = n === (o ? "border" : "content") ? 4 : "width" === t ? 1 : 0,
          a = 0;
        r < 4;
        r += 2
      )
        "margin" === n && (a += J.css(e, n + Te[r], !0, i)),
          o
            ? ("content" === n && (a -= J.css(e, "padding" + Te[r], !0, i)),
              "margin" !== n &&
                (a -= J.css(e, "border" + Te[r] + "Width", !0, i)))
            : ((a += J.css(e, "padding" + Te[r], !0, i)),
              "padding" !== n &&
                (a += J.css(e, "border" + Te[r] + "Width", !0, i)));
      return a;
    }
    function S(e, t, n) {
      var o = !0,
        i = "width" === t ? e.offsetWidth : e.offsetHeight,
        r = Be(e),
        a = "border-box" === J.css(e, "boxSizing", !1, r);
      if (i <= 0 || null == i) {
        if (
          ((i = b(e, t, r)),
          (i < 0 || null == i) && (i = e.style[t]),
          We.test(i))
        )
          return i;
        (o = a && (Y.boxSizingReliable() || i === e.style[t])),
          (i = parseFloat(i) || 0);
      }
      return i + k(e, t, n || (a ? "border" : "content"), o, r) + "px";
    }
    function D(e, t) {
      for (var n, o, i, r = [], a = 0, s = e.length; a < s; a++)
        (o = e[a]),
          o.style &&
            ((r[a] = ve.get(o, "olddisplay")),
            (n = o.style.display),
            t
              ? (r[a] || "none" !== n || (o.style.display = ""),
                "" === o.style.display &&
                  Ce(o) &&
                  (r[a] = ve.access(o, "olddisplay", w(o.nodeName))))
              : ((i = Ce(o)),
                ("none" === n && i) ||
                  ve.set(o, "olddisplay", i ? n : J.css(o, "display"))));
      for (a = 0; a < s; a++)
        (o = e[a]),
          o.style &&
            ((t && "none" !== o.style.display && "" !== o.style.display) ||
              (o.style.display = t ? r[a] || "" : "none"));
      return e;
    }
    function I(e, t, n, o, i) {
      return new I.prototype.init(e, t, n, o, i);
    }
    function N() {
      return (
        setTimeout(function() {
          Ye = void 0;
        }),
        (Ye = J.now())
      );
    }
    function F(e, t) {
      var n,
        o = 0,
        i = { height: e };
      for (t = t ? 1 : 0; o < 4; o += 2 - t)
        (n = Te[o]), (i["margin" + n] = i["padding" + n] = e);
      return t && (i.opacity = i.width = e), i;
    }
    function A(e, t, n) {
      for (
        var o, i = (nt[t] || []).concat(nt["*"]), r = 0, a = i.length;
        r < a;
        r++
      )
        if ((o = i[r].call(n, t, e))) return o;
    }
    function _(e, t, n) {
      var o,
        i,
        r,
        a,
        s,
        l,
        u,
        c,
        p = this,
        d = {},
        f = e.style,
        h = e.nodeType && Ce(e),
        m = ve.get(e, "fxshow");
      n.queue ||
        ((s = J._queueHooks(e, "fx")),
        null == s.unqueued &&
          ((s.unqueued = 0),
          (l = s.empty.fire),
          (s.empty.fire = function() {
            s.unqueued || l();
          })),
        s.unqueued++,
        p.always(function() {
          p.always(function() {
            s.unqueued--, J.queue(e, "fx").length || s.empty.fire();
          });
        })),
        1 === e.nodeType &&
          ("height" in t || "width" in t) &&
          ((n.overflow = [f.overflow, f.overflowX, f.overflowY]),
          (u = J.css(e, "display")),
          (c = "none" === u ? ve.get(e, "olddisplay") || w(e.nodeName) : u),
          "inline" === c &&
            "none" === J.css(e, "float") &&
            (f.display = "inline-block")),
        n.overflow &&
          ((f.overflow = "hidden"),
          p.always(function() {
            (f.overflow = n.overflow[0]),
              (f.overflowX = n.overflow[1]),
              (f.overflowY = n.overflow[2]);
          }));
      for (o in t)
        if (((i = t[o]), Qe.exec(i))) {
          if (
            (delete t[o],
            (r = r || "toggle" === i),
            i === (h ? "hide" : "show"))
          ) {
            if ("show" !== i || !m || void 0 === m[o]) continue;
            h = !0;
          }
          d[o] = (m && m[o]) || J.style(e, o);
        } else u = void 0;
      if (J.isEmptyObject(d))
        "inline" === ("none" === u ? w(e.nodeName) : u) && (f.display = u);
      else {
        m ? "hidden" in m && (h = m.hidden) : (m = ve.access(e, "fxshow", {})),
          r && (m.hidden = !h),
          h
            ? J(e).show()
            : p.done(function() {
                J(e).hide();
              }),
          p.done(function() {
            var t;
            ve.remove(e, "fxshow");
            for (t in d) J.style(e, t, d[t]);
          });
        for (o in d)
          (a = A(h ? m[o] : 0, o, p)),
            o in m ||
              ((m[o] = a.start),
              h &&
                ((a.end = a.start),
                (a.start = "width" === o || "height" === o ? 1 : 0)));
      }
    }
    function L(e, t) {
      var n, o, i, r, a;
      for (n in e)
        if (
          ((o = J.camelCase(n)),
          (i = t[o]),
          (r = e[n]),
          J.isArray(r) && ((i = r[1]), (r = e[n] = r[0])),
          n !== o && ((e[o] = r), delete e[n]),
          (a = J.cssHooks[o]),
          a && "expand" in a)
        ) {
          (r = a.expand(r)), delete e[o];
          for (n in r) n in e || ((e[n] = r[n]), (t[n] = i));
        } else t[o] = i;
    }
    function O(e, t, n) {
      var o,
        i,
        r = 0,
        a = tt.length,
        s = J.Deferred().always(function() {
          delete l.elem;
        }),
        l = function() {
          if (i) return !1;
          for (
            var t = Ye || N(),
              n = Math.max(0, u.startTime + u.duration - t),
              o = n / u.duration || 0,
              r = 1 - o,
              a = 0,
              l = u.tweens.length;
            a < l;
            a++
          )
            u.tweens[a].run(r);
          return (
            s.notifyWith(e, [u, r, n]),
            r < 1 && l ? n : (s.resolveWith(e, [u]), !1)
          );
        },
        u = s.promise({
          elem: e,
          props: J.extend({}, t),
          opts: J.extend(!0, { specialEasing: {} }, n),
          originalProperties: t,
          originalOptions: n,
          startTime: Ye || N(),
          duration: n.duration,
          tweens: [],
          createTween: function(t, n) {
            var o = J.Tween(
              e,
              u.opts,
              t,
              n,
              u.opts.specialEasing[t] || u.opts.easing
            );
            return u.tweens.push(o), o;
          },
          stop: function(t) {
            var n = 0,
              o = t ? u.tweens.length : 0;
            if (i) return this;
            for (i = !0; n < o; n++) u.tweens[n].run(1);
            return t ? s.resolveWith(e, [u, t]) : s.rejectWith(e, [u, t]), this;
          }
        }),
        c = u.props;
      for (L(c, u.opts.specialEasing); r < a; r++)
        if ((o = tt[r].call(u, e, c, u.opts))) return o;
      return (
        J.map(c, A, u),
        J.isFunction(u.opts.start) && u.opts.start.call(e, u),
        J.fx.timer(J.extend(l, { elem: e, anim: u, queue: u.opts.queue })),
        u
          .progress(u.opts.progress)
          .done(u.opts.done, u.opts.complete)
          .fail(u.opts.fail)
          .always(u.opts.always)
      );
    }
    function M(e) {
      return function(t, n) {
        "string" != typeof t && ((n = t), (t = "*"));
        var o,
          i = 0,
          r = t.toLowerCase().match(fe) || [];
        if (J.isFunction(n))
          for (; (o = r[i++]); )
            "+" === o[0]
              ? ((o = o.slice(1) || "*"), (e[o] = e[o] || []).unshift(n))
              : (e[o] = e[o] || []).push(n);
      };
    }
    function R(e, t, n, o) {
      function i(s) {
        var l;
        return (
          (r[s] = !0),
          J.each(e[s] || [], function(e, s) {
            var u = s(t, n, o);
            return "string" != typeof u || a || r[u]
              ? a
                ? !(l = u)
                : void 0
              : (t.dataTypes.unshift(u), i(u), !1);
          }),
          l
        );
      }
      var r = {},
        a = e === xt;
      return i(t.dataTypes[0]) || (!r["*"] && i("*"));
    }
    function P(e, t) {
      var n,
        o,
        i = J.ajaxSettings.flatOptions || {};
      for (n in t) void 0 !== t[n] && ((i[n] ? e : o || (o = {}))[n] = t[n]);
      return o && J.extend(!0, e, o), e;
    }
    function j(e, t, n) {
      for (var o, i, r, a, s = e.contents, l = e.dataTypes; "*" === l[0]; )
        l.shift(),
          void 0 === o &&
            (o = e.mimeType || t.getResponseHeader("Content-Type"));
      if (o)
        for (i in s)
          if (s[i] && s[i].test(o)) {
            l.unshift(i);
            break;
          }
      if (l[0] in n) r = l[0];
      else {
        for (i in n) {
          if (!l[0] || e.converters[i + " " + l[0]]) {
            r = i;
            break;
          }
          a || (a = i);
        }
        r = r || a;
      }
      if (r) return r !== l[0] && l.unshift(r), n[r];
    }
    function H(e, t, n, o) {
      var i,
        r,
        a,
        s,
        l,
        u = {},
        c = e.dataTypes.slice();
      if (c[1]) for (a in e.converters) u[a.toLowerCase()] = e.converters[a];
      for (r = c.shift(); r; )
        if (
          (e.responseFields[r] && (n[e.responseFields[r]] = t),
          !l && o && e.dataFilter && (t = e.dataFilter(t, e.dataType)),
          (l = r),
          (r = c.shift()))
        )
          if ("*" === r) r = l;
          else if ("*" !== l && l !== r) {
            if (((a = u[l + " " + r] || u["* " + r]), !a))
              for (i in u)
                if (
                  ((s = i.split(" ")),
                  s[1] === r && (a = u[l + " " + s[0]] || u["* " + s[0]]))
                ) {
                  a === !0
                    ? (a = u[i])
                    : u[i] !== !0 && ((r = s[0]), c.unshift(s[1]));
                  break;
                }
            if (a !== !0)
              if (a && e["throws"]) t = a(t);
              else
                try {
                  t = a(t);
                } catch (p) {
                  return {
                    state: "parsererror",
                    error: a ? p : "No conversion from " + l + " to " + r
                  };
                }
          }
      return { state: "success", data: t };
    }
    function q(e, t, n, o) {
      var i;
      if (J.isArray(t))
        J.each(t, function(t, i) {
          n || Et.test(e)
            ? o(e, i)
            : q(e + "[" + ("object" == typeof i ? t : "") + "]", i, n, o);
        });
      else if (n || "object" !== J.type(t)) o(e, t);
      else for (i in t) q(e + "[" + i + "]", t[i], n, o);
    }
    function z(e) {
      return J.isWindow(e) ? e : 9 === e.nodeType && e.defaultView;
    }
    var W = [],
      B = W.slice,
      Z = W.concat,
      U = W.push,
      $ = W.indexOf,
      K = {},
      X = K.toString,
      G = K.hasOwnProperty,
      Y = {},
      V = e.document,
      Q = "2.1.4",
      J = function(e, t) {
        return new J.fn.init(e, t);
      },
      ee = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      te = /^-ms-/,
      ne = /-([\da-z])/gi,
      oe = function(e, t) {
        return t.toUpperCase();
      };
    (J.fn = J.prototype = {
      jquery: Q,
      constructor: J,
      selector: "",
      length: 0,
      toArray: function() {
        return B.call(this);
      },
      get: function(e) {
        return null != e
          ? e < 0
            ? this[e + this.length]
            : this[e]
          : B.call(this);
      },
      pushStack: function(e) {
        var t = J.merge(this.constructor(), e);
        return (t.prevObject = this), (t.context = this.context), t;
      },
      each: function(e, t) {
        return J.each(this, e, t);
      },
      map: function(e) {
        return this.pushStack(
          J.map(this, function(t, n) {
            return e.call(t, n, t);
          })
        );
      },
      slice: function() {
        return this.pushStack(B.apply(this, arguments));
      },
      first: function() {
        return this.eq(0);
      },
      last: function() {
        return this.eq(-1);
      },
      eq: function(e) {
        var t = this.length,
          n = +e + (e < 0 ? t : 0);
        return this.pushStack(n >= 0 && n < t ? [this[n]] : []);
      },
      end: function() {
        return this.prevObject || this.constructor(null);
      },
      push: U,
      sort: W.sort,
      splice: W.splice
    }),
      (J.extend = J.fn.extend = function() {
        var e,
          t,
          n,
          o,
          i,
          r,
          a = arguments[0] || {},
          s = 1,
          l = arguments.length,
          u = !1;
        for (
          "boolean" == typeof a && ((u = a), (a = arguments[s] || {}), s++),
            "object" == typeof a || J.isFunction(a) || (a = {}),
            s === l && ((a = this), s--);
          s < l;
          s++
        )
          if (null != (e = arguments[s]))
            for (t in e)
              (n = a[t]),
                (o = e[t]),
                a !== o &&
                  (u && o && (J.isPlainObject(o) || (i = J.isArray(o)))
                    ? (i
                        ? ((i = !1), (r = n && J.isArray(n) ? n : []))
                        : (r = n && J.isPlainObject(n) ? n : {}),
                      (a[t] = J.extend(u, r, o)))
                    : void 0 !== o && (a[t] = o));
        return a;
      }),
      J.extend({
        expando: "jQuery" + (Q + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(e) {
          throw new Error(e);
        },
        noop: function() {},
        isFunction: function(e) {
          return "function" === J.type(e);
        },
        isArray: Array.isArray,
        isWindow: function(e) {
          return null != e && e === e.window;
        },
        isNumeric: function(e) {
          return !J.isArray(e) && e - parseFloat(e) + 1 >= 0;
        },
        isPlainObject: function(e) {
          return (
            "object" === J.type(e) &&
            !e.nodeType &&
            !J.isWindow(e) &&
            !(
              e.constructor && !G.call(e.constructor.prototype, "isPrototypeOf")
            )
          );
        },
        isEmptyObject: function(e) {
          var t;
          for (t in e) return !1;
          return !0;
        },
        type: function(e) {
          return null == e
            ? e + ""
            : "object" == typeof e || "function" == typeof e
            ? K[X.call(e)] || "object"
            : typeof e;
        },
        globalEval: function(e) {
          var t,
            n = eval;
          (e = J.trim(e)),
            e &&
              (1 === e.indexOf("use strict")
                ? ((t = V.createElement("script")),
                  (t.text = e),
                  V.head.appendChild(t).parentNode.removeChild(t))
                : n(e));
        },
        camelCase: function(e) {
          return e.replace(te, "ms-").replace(ne, oe);
        },
        nodeName: function(e, t) {
          return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
        },
        each: function(e, t, o) {
          var i,
            r = 0,
            a = e.length,
            s = n(e);
          if (o) {
            if (s) for (; r < a && ((i = t.apply(e[r], o)), i !== !1); r++);
            else for (r in e) if (((i = t.apply(e[r], o)), i === !1)) break;
          } else if (s)
            for (; r < a && ((i = t.call(e[r], r, e[r])), i !== !1); r++);
          else for (r in e) if (((i = t.call(e[r], r, e[r])), i === !1)) break;
          return e;
        },
        trim: function(e) {
          return null == e ? "" : (e + "").replace(ee, "");
        },
        makeArray: function(e, t) {
          var o = t || [];
          return (
            null != e &&
              (n(Object(e))
                ? J.merge(o, "string" == typeof e ? [e] : e)
                : U.call(o, e)),
            o
          );
        },
        inArray: function(e, t, n) {
          return null == t ? -1 : $.call(t, e, n);
        },
        merge: function(e, t) {
          for (var n = +t.length, o = 0, i = e.length; o < n; o++)
            e[i++] = t[o];
          return (e.length = i), e;
        },
        grep: function(e, t, n) {
          for (var o, i = [], r = 0, a = e.length, s = !n; r < a; r++)
            (o = !t(e[r], r)), o !== s && i.push(e[r]);
          return i;
        },
        map: function(e, t, o) {
          var i,
            r = 0,
            a = e.length,
            s = n(e),
            l = [];
          if (s) for (; r < a; r++) (i = t(e[r], r, o)), null != i && l.push(i);
          else for (r in e) (i = t(e[r], r, o)), null != i && l.push(i);
          return Z.apply([], l);
        },
        guid: 1,
        proxy: function(e, t) {
          var n, o, i;
          if (
            ("string" == typeof t && ((n = e[t]), (t = e), (e = n)),
            J.isFunction(e))
          )
            return (
              (o = B.call(arguments, 2)),
              (i = function() {
                return e.apply(t || this, o.concat(B.call(arguments)));
              }),
              (i.guid = e.guid = e.guid || J.guid++),
              i
            );
        },
        now: Date.now,
        support: Y
      }),
      J.each(
        "Boolean Number String Function Array Date RegExp Object Error".split(
          " "
        ),
        function(e, t) {
          K["[object " + t + "]"] = t.toLowerCase();
        }
      );
    var ie = (function(e) {
      function t(e, t, n, o) {
        var i, r, a, s, l, u, p, f, h, m;
        if (
          ((t ? t.ownerDocument || t : q) !== _ && A(t),
          (t = t || _),
          (n = n || []),
          (s = t.nodeType),
          "string" != typeof e || !e || (1 !== s && 9 !== s && 11 !== s))
        )
          return n;
        if (!o && O) {
          if (11 !== s && (i = ye.exec(e)))
            if ((a = i[1])) {
              if (9 === s) {
                if (((r = t.getElementById(a)), !r || !r.parentNode)) return n;
                if (r.id === a) return n.push(r), n;
              } else if (
                t.ownerDocument &&
                (r = t.ownerDocument.getElementById(a)) &&
                j(t, r) &&
                r.id === a
              )
                return n.push(r), n;
            } else {
              if (i[2]) return Q.apply(n, t.getElementsByTagName(e)), n;
              if ((a = i[3]) && b.getElementsByClassName)
                return Q.apply(n, t.getElementsByClassName(a)), n;
            }
          if (b.qsa && (!M || !M.test(e))) {
            if (
              ((f = p = H),
              (h = t),
              (m = 1 !== s && e),
              1 === s && "object" !== t.nodeName.toLowerCase())
            ) {
              for (
                u = k(e),
                  (p = t.getAttribute("id"))
                    ? (f = p.replace(we, "\\$&"))
                    : t.setAttribute("id", f),
                  f = "[id='" + f + "'] ",
                  l = u.length;
                l--;

              )
                u[l] = f + d(u[l]);
              (h = (xe.test(e) && c(t.parentNode)) || t), (m = u.join(","));
            }
            if (m)
              try {
                return Q.apply(n, h.querySelectorAll(m)), n;
              } catch (g) {
              } finally {
                p || t.removeAttribute("id");
              }
          }
        }
        return D(e.replace(le, "$1"), t, n, o);
      }
      function n() {
        function e(n, o) {
          return (
            t.push(n + " ") > T.cacheLength && delete e[t.shift()],
            (e[n + " "] = o)
          );
        }
        var t = [];
        return e;
      }
      function o(e) {
        return (e[H] = !0), e;
      }
      function i(e) {
        var t = _.createElement("div");
        try {
          return !!e(t);
        } catch (n) {
          return !1;
        } finally {
          t.parentNode && t.parentNode.removeChild(t), (t = null);
        }
      }
      function r(e, t) {
        for (var n = e.split("|"), o = e.length; o--; ) T.attrHandle[n[o]] = t;
      }
      function a(e, t) {
        var n = t && e,
          o =
            n &&
            1 === e.nodeType &&
            1 === t.nodeType &&
            (~t.sourceIndex || K) - (~e.sourceIndex || K);
        if (o) return o;
        if (n) for (; (n = n.nextSibling); ) if (n === t) return -1;
        return e ? 1 : -1;
      }
      function s(e) {
        return function(t) {
          var n = t.nodeName.toLowerCase();
          return "input" === n && t.type === e;
        };
      }
      function l(e) {
        return function(t) {
          var n = t.nodeName.toLowerCase();
          return ("input" === n || "button" === n) && t.type === e;
        };
      }
      function u(e) {
        return o(function(t) {
          return (
            (t = +t),
            o(function(n, o) {
              for (var i, r = e([], n.length, t), a = r.length; a--; )
                n[(i = r[a])] && (n[i] = !(o[i] = n[i]));
            })
          );
        });
      }
      function c(e) {
        return e && "undefined" != typeof e.getElementsByTagName && e;
      }
      function p() {}
      function d(e) {
        for (var t = 0, n = e.length, o = ""; t < n; t++) o += e[t].value;
        return o;
      }
      function f(e, t, n) {
        var o = t.dir,
          i = n && "parentNode" === o,
          r = W++;
        return t.first
          ? function(t, n, r) {
              for (; (t = t[o]); ) if (1 === t.nodeType || i) return e(t, n, r);
            }
          : function(t, n, a) {
              var s,
                l,
                u = [z, r];
              if (a) {
                for (; (t = t[o]); )
                  if ((1 === t.nodeType || i) && e(t, n, a)) return !0;
              } else
                for (; (t = t[o]); )
                  if (1 === t.nodeType || i) {
                    if (
                      ((l = t[H] || (t[H] = {})),
                      (s = l[o]) && s[0] === z && s[1] === r)
                    )
                      return (u[2] = s[2]);
                    if (((l[o] = u), (u[2] = e(t, n, a)))) return !0;
                  }
            };
      }
      function h(e) {
        return e.length > 1
          ? function(t, n, o) {
              for (var i = e.length; i--; ) if (!e[i](t, n, o)) return !1;
              return !0;
            }
          : e[0];
      }
      function m(e, n, o) {
        for (var i = 0, r = n.length; i < r; i++) t(e, n[i], o);
        return o;
      }
      function g(e, t, n, o, i) {
        for (var r, a = [], s = 0, l = e.length, u = null != t; s < l; s++)
          (r = e[s]) && ((n && !n(r, o, i)) || (a.push(r), u && t.push(s)));
        return a;
      }
      function v(e, t, n, i, r, a) {
        return (
          i && !i[H] && (i = v(i)),
          r && !r[H] && (r = v(r, a)),
          o(function(o, a, s, l) {
            var u,
              c,
              p,
              d = [],
              f = [],
              h = a.length,
              v = o || m(t || "*", s.nodeType ? [s] : s, []),
              y = !e || (!o && t) ? v : g(v, d, e, s, l),
              x = n ? (r || (o ? e : h || i) ? [] : a) : y;
            if ((n && n(y, x, s, l), i))
              for (u = g(x, f), i(u, [], s, l), c = u.length; c--; )
                (p = u[c]) && (x[f[c]] = !(y[f[c]] = p));
            if (o) {
              if (r || e) {
                if (r) {
                  for (u = [], c = x.length; c--; )
                    (p = x[c]) && u.push((y[c] = p));
                  r(null, (x = []), u, l);
                }
                for (c = x.length; c--; )
                  (p = x[c]) &&
                    (u = r ? ee(o, p) : d[c]) > -1 &&
                    (o[u] = !(a[u] = p));
              }
            } else (x = g(x === a ? x.splice(h, x.length) : x)), r ? r(null, a, x, l) : Q.apply(a, x);
          })
        );
      }
      function y(e) {
        for (
          var t,
            n,
            o,
            i = e.length,
            r = T.relative[e[0].type],
            a = r || T.relative[" "],
            s = r ? 1 : 0,
            l = f(
              function(e) {
                return e === t;
              },
              a,
              !0
            ),
            u = f(
              function(e) {
                return ee(t, e) > -1;
              },
              a,
              !0
            ),
            c = [
              function(e, n, o) {
                var i =
                  (!r && (o || n !== I)) ||
                  ((t = n).nodeType ? l(e, n, o) : u(e, n, o));
                return (t = null), i;
              }
            ];
          s < i;
          s++
        )
          if ((n = T.relative[e[s].type])) c = [f(h(c), n)];
          else {
            if (((n = T.filter[e[s].type].apply(null, e[s].matches)), n[H])) {
              for (o = ++s; o < i && !T.relative[e[o].type]; o++);
              return v(
                s > 1 && h(c),
                s > 1 &&
                  d(
                    e
                      .slice(0, s - 1)
                      .concat({ value: " " === e[s - 2].type ? "*" : "" })
                  ).replace(le, "$1"),
                n,
                s < o && y(e.slice(s, o)),
                o < i && y((e = e.slice(o))),
                o < i && d(e)
              );
            }
            c.push(n);
          }
        return h(c);
      }
      function x(e, n) {
        var i = n.length > 0,
          r = e.length > 0,
          a = function(o, a, s, l, u) {
            var c,
              p,
              d,
              f = 0,
              h = "0",
              m = o && [],
              v = [],
              y = I,
              x = o || (r && T.find.TAG("*", u)),
              w = (z += null == y ? 1 : Math.random() || 0.1),
              b = x.length;
            for (u && (I = a !== _ && a); h !== b && null != (c = x[h]); h++) {
              if (r && c) {
                for (p = 0; (d = e[p++]); )
                  if (d(c, a, s)) {
                    l.push(c);
                    break;
                  }
                u && (z = w);
              }
              i && ((c = !d && c) && f--, o && m.push(c));
            }
            if (((f += h), i && h !== f)) {
              for (p = 0; (d = n[p++]); ) d(m, v, a, s);
              if (o) {
                if (f > 0) for (; h--; ) m[h] || v[h] || (v[h] = Y.call(l));
                v = g(v);
              }
              Q.apply(l, v),
                u && !o && v.length > 0 && f + n.length > 1 && t.uniqueSort(l);
            }
            return u && ((z = w), (I = y)), m;
          };
        return i ? o(a) : a;
      }
      var w,
        b,
        T,
        C,
        E,
        k,
        S,
        D,
        I,
        N,
        F,
        A,
        _,
        L,
        O,
        M,
        R,
        P,
        j,
        H = "sizzle" + 1 * new Date(),
        q = e.document,
        z = 0,
        W = 0,
        B = n(),
        Z = n(),
        U = n(),
        $ = function(e, t) {
          return e === t && (F = !0), 0;
        },
        K = 1 << 31,
        X = {}.hasOwnProperty,
        G = [],
        Y = G.pop,
        V = G.push,
        Q = G.push,
        J = G.slice,
        ee = function(e, t) {
          for (var n = 0, o = e.length; n < o; n++) if (e[n] === t) return n;
          return -1;
        },
        te =
          "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
        ne = "[\\x20\\t\\r\\n\\f]",
        oe = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
        ie = oe.replace("w", "w#"),
        re =
          "\\[" +
          ne +
          "*(" +
          oe +
          ")(?:" +
          ne +
          "*([*^$|!~]?=)" +
          ne +
          "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" +
          ie +
          "))|)" +
          ne +
          "*\\]",
        ae =
          ":(" +
          oe +
          ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" +
          re +
          ")*)|.*)\\)|)",
        se = new RegExp(ne + "+", "g"),
        le = new RegExp(
          "^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$",
          "g"
        ),
        ue = new RegExp("^" + ne + "*," + ne + "*"),
        ce = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"),
        pe = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"),
        de = new RegExp(ae),
        fe = new RegExp("^" + ie + "$"),
        he = {
          ID: new RegExp("^#(" + oe + ")"),
          CLASS: new RegExp("^\\.(" + oe + ")"),
          TAG: new RegExp("^(" + oe.replace("w", "w*") + ")"),
          ATTR: new RegExp("^" + re),
          PSEUDO: new RegExp("^" + ae),
          CHILD: new RegExp(
            "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" +
              ne +
              "*(even|odd|(([+-]|)(\\d*)n|)" +
              ne +
              "*(?:([+-]|)" +
              ne +
              "*(\\d+)|))" +
              ne +
              "*\\)|)",
            "i"
          ),
          bool: new RegExp("^(?:" + te + ")$", "i"),
          needsContext: new RegExp(
            "^" +
              ne +
              "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
              ne +
              "*((?:-\\d)?\\d*)" +
              ne +
              "*\\)|)(?=[^-]|$)",
            "i"
          )
        },
        me = /^(?:input|select|textarea|button)$/i,
        ge = /^h\d$/i,
        ve = /^[^{]+\{\s*\[native \w/,
        ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        xe = /[+~]/,
        we = /'|\\/g,
        be = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"),
        Te = function(e, t, n) {
          var o = "0x" + t - 65536;
          return o !== o || n
            ? t
            : o < 0
            ? String.fromCharCode(o + 65536)
            : String.fromCharCode((o >> 10) | 55296, (1023 & o) | 56320);
        },
        Ce = function() {
          A();
        };
      try {
        Q.apply((G = J.call(q.childNodes)), q.childNodes),
          G[q.childNodes.length].nodeType;
      } catch (Ee) {
        Q = {
          apply: G.length
            ? function(e, t) {
                V.apply(e, J.call(t));
              }
            : function(e, t) {
                for (var n = e.length, o = 0; (e[n++] = t[o++]); );
                e.length = n - 1;
              }
        };
      }
      (b = t.support = {}),
        (E = t.isXML = function(e) {
          var t = e && (e.ownerDocument || e).documentElement;
          return !!t && "HTML" !== t.nodeName;
        }),
        (A = t.setDocument = function(e) {
          var t,
            n,
            o = e ? e.ownerDocument || e : q;
          return o !== _ && 9 === o.nodeType && o.documentElement
            ? ((_ = o),
              (L = o.documentElement),
              (n = o.defaultView),
              n &&
                n !== n.top &&
                (n.addEventListener
                  ? n.addEventListener("unload", Ce, !1)
                  : n.attachEvent && n.attachEvent("onunload", Ce)),
              (O = !E(o)),
              (b.attributes = i(function(e) {
                return (e.className = "i"), !e.getAttribute("className");
              })),
              (b.getElementsByTagName = i(function(e) {
                return (
                  e.appendChild(o.createComment("")),
                  !e.getElementsByTagName("*").length
                );
              })),
              (b.getElementsByClassName = ve.test(o.getElementsByClassName)),
              (b.getById = i(function(e) {
                return (
                  (L.appendChild(e).id = H),
                  !o.getElementsByName || !o.getElementsByName(H).length
                );
              })),
              b.getById
                ? ((T.find.ID = function(e, t) {
                    if ("undefined" != typeof t.getElementById && O) {
                      var n = t.getElementById(e);
                      return n && n.parentNode ? [n] : [];
                    }
                  }),
                  (T.filter.ID = function(e) {
                    var t = e.replace(be, Te);
                    return function(e) {
                      return e.getAttribute("id") === t;
                    };
                  }))
                : (delete T.find.ID,
                  (T.filter.ID = function(e) {
                    var t = e.replace(be, Te);
                    return function(e) {
                      var n =
                        "undefined" != typeof e.getAttributeNode &&
                        e.getAttributeNode("id");
                      return n && n.value === t;
                    };
                  })),
              (T.find.TAG = b.getElementsByTagName
                ? function(e, t) {
                    return "undefined" != typeof t.getElementsByTagName
                      ? t.getElementsByTagName(e)
                      : b.qsa
                      ? t.querySelectorAll(e)
                      : void 0;
                  }
                : function(e, t) {
                    var n,
                      o = [],
                      i = 0,
                      r = t.getElementsByTagName(e);
                    if ("*" === e) {
                      for (; (n = r[i++]); ) 1 === n.nodeType && o.push(n);
                      return o;
                    }
                    return r;
                  }),
              (T.find.CLASS =
                b.getElementsByClassName &&
                function(e, t) {
                  if (O) return t.getElementsByClassName(e);
                }),
              (R = []),
              (M = []),
              (b.qsa = ve.test(o.querySelectorAll)) &&
                (i(function(e) {
                  (L.appendChild(e).innerHTML =
                    "<a id='" +
                    H +
                    "'></a><select id='" +
                    H +
                    "-\f]' msallowcapture=''><option selected=''></option></select>"),
                    e.querySelectorAll("[msallowcapture^='']").length &&
                      M.push("[*^$]=" + ne + "*(?:''|\"\")"),
                    e.querySelectorAll("[selected]").length ||
                      M.push("\\[" + ne + "*(?:value|" + te + ")"),
                    e.querySelectorAll("[id~=" + H + "-]").length ||
                      M.push("~="),
                    e.querySelectorAll(":checked").length || M.push(":checked"),
                    e.querySelectorAll("a#" + H + "+*").length ||
                      M.push(".#.+[+~]");
                }),
                i(function(e) {
                  var t = o.createElement("input");
                  t.setAttribute("type", "hidden"),
                    e.appendChild(t).setAttribute("name", "D"),
                    e.querySelectorAll("[name=d]").length &&
                      M.push("name" + ne + "*[*^$|!~]?="),
                    e.querySelectorAll(":enabled").length ||
                      M.push(":enabled", ":disabled"),
                    e.querySelectorAll("*,:x"),
                    M.push(",.*:");
                })),
              (b.matchesSelector = ve.test(
                (P =
                  L.matches ||
                  L.webkitMatchesSelector ||
                  L.mozMatchesSelector ||
                  L.oMatchesSelector ||
                  L.msMatchesSelector)
              )) &&
                i(function(e) {
                  (b.disconnectedMatch = P.call(e, "div")),
                    P.call(e, "[s!='']:x"),
                    R.push("!=", ae);
                }),
              (M = M.length && new RegExp(M.join("|"))),
              (R = R.length && new RegExp(R.join("|"))),
              (t = ve.test(L.compareDocumentPosition)),
              (j =
                t || ve.test(L.contains)
                  ? function(e, t) {
                      var n = 9 === e.nodeType ? e.documentElement : e,
                        o = t && t.parentNode;
                      return (
                        e === o ||
                        !(
                          !o ||
                          1 !== o.nodeType ||
                          !(n.contains
                            ? n.contains(o)
                            : e.compareDocumentPosition &&
                              16 & e.compareDocumentPosition(o))
                        )
                      );
                    }
                  : function(e, t) {
                      if (t)
                        for (; (t = t.parentNode); ) if (t === e) return !0;
                      return !1;
                    }),
              ($ = t
                ? function(e, t) {
                    if (e === t) return (F = !0), 0;
                    var n =
                      !e.compareDocumentPosition - !t.compareDocumentPosition;
                    return n
                      ? n
                      : ((n =
                          (e.ownerDocument || e) === (t.ownerDocument || t)
                            ? e.compareDocumentPosition(t)
                            : 1),
                        1 & n ||
                        (!b.sortDetached && t.compareDocumentPosition(e) === n)
                          ? e === o || (e.ownerDocument === q && j(q, e))
                            ? -1
                            : t === o || (t.ownerDocument === q && j(q, t))
                            ? 1
                            : N
                            ? ee(N, e) - ee(N, t)
                            : 0
                          : 4 & n
                          ? -1
                          : 1);
                  }
                : function(e, t) {
                    if (e === t) return (F = !0), 0;
                    var n,
                      i = 0,
                      r = e.parentNode,
                      s = t.parentNode,
                      l = [e],
                      u = [t];
                    if (!r || !s)
                      return e === o
                        ? -1
                        : t === o
                        ? 1
                        : r
                        ? -1
                        : s
                        ? 1
                        : N
                        ? ee(N, e) - ee(N, t)
                        : 0;
                    if (r === s) return a(e, t);
                    for (n = e; (n = n.parentNode); ) l.unshift(n);
                    for (n = t; (n = n.parentNode); ) u.unshift(n);
                    for (; l[i] === u[i]; ) i++;
                    return i
                      ? a(l[i], u[i])
                      : l[i] === q
                      ? -1
                      : u[i] === q
                      ? 1
                      : 0;
                  }),
              o)
            : _;
        }),
        (t.matches = function(e, n) {
          return t(e, null, null, n);
        }),
        (t.matchesSelector = function(e, n) {
          if (
            ((e.ownerDocument || e) !== _ && A(e),
            (n = n.replace(pe, "='$1']")),
            b.matchesSelector && O && (!R || !R.test(n)) && (!M || !M.test(n)))
          )
            try {
              var o = P.call(e, n);
              if (
                o ||
                b.disconnectedMatch ||
                (e.document && 11 !== e.document.nodeType)
              )
                return o;
            } catch (i) {}
          return t(n, _, null, [e]).length > 0;
        }),
        (t.contains = function(e, t) {
          return (e.ownerDocument || e) !== _ && A(e), j(e, t);
        }),
        (t.attr = function(e, t) {
          (e.ownerDocument || e) !== _ && A(e);
          var n = T.attrHandle[t.toLowerCase()],
            o =
              n && X.call(T.attrHandle, t.toLowerCase()) ? n(e, t, !O) : void 0;
          return void 0 !== o
            ? o
            : b.attributes || !O
            ? e.getAttribute(t)
            : (o = e.getAttributeNode(t)) && o.specified
            ? o.value
            : null;
        }),
        (t.error = function(e) {
          throw new Error("Syntax error, unrecognized expression: " + e);
        }),
        (t.uniqueSort = function(e) {
          var t,
            n = [],
            o = 0,
            i = 0;
          if (
            ((F = !b.detectDuplicates),
            (N = !b.sortStable && e.slice(0)),
            e.sort($),
            F)
          ) {
            for (; (t = e[i++]); ) t === e[i] && (o = n.push(i));
            for (; o--; ) e.splice(n[o], 1);
          }
          return (N = null), e;
        }),
        (C = t.getText = function(e) {
          var t,
            n = "",
            o = 0,
            i = e.nodeType;
          if (i) {
            if (1 === i || 9 === i || 11 === i) {
              if ("string" == typeof e.textContent) return e.textContent;
              for (e = e.firstChild; e; e = e.nextSibling) n += C(e);
            } else if (3 === i || 4 === i) return e.nodeValue;
          } else for (; (t = e[o++]); ) n += C(t);
          return n;
        }),
        (T = t.selectors = {
          cacheLength: 50,
          createPseudo: o,
          match: he,
          attrHandle: {},
          find: {},
          relative: {
            ">": { dir: "parentNode", first: !0 },
            " ": { dir: "parentNode" },
            "+": { dir: "previousSibling", first: !0 },
            "~": { dir: "previousSibling" }
          },
          preFilter: {
            ATTR: function(e) {
              return (
                (e[1] = e[1].replace(be, Te)),
                (e[3] = (e[3] || e[4] || e[5] || "").replace(be, Te)),
                "~=" === e[2] && (e[3] = " " + e[3] + " "),
                e.slice(0, 4)
              );
            },
            CHILD: function(e) {
              return (
                (e[1] = e[1].toLowerCase()),
                "nth" === e[1].slice(0, 3)
                  ? (e[3] || t.error(e[0]),
                    (e[4] = +(e[4]
                      ? e[5] + (e[6] || 1)
                      : 2 * ("even" === e[3] || "odd" === e[3]))),
                    (e[5] = +(e[7] + e[8] || "odd" === e[3])))
                  : e[3] && t.error(e[0]),
                e
              );
            },
            PSEUDO: function(e) {
              var t,
                n = !e[6] && e[2];
              return he.CHILD.test(e[0])
                ? null
                : (e[3]
                    ? (e[2] = e[4] || e[5] || "")
                    : n &&
                      de.test(n) &&
                      (t = k(n, !0)) &&
                      (t = n.indexOf(")", n.length - t) - n.length) &&
                      ((e[0] = e[0].slice(0, t)), (e[2] = n.slice(0, t))),
                  e.slice(0, 3));
            }
          },
          filter: {
            TAG: function(e) {
              var t = e.replace(be, Te).toLowerCase();
              return "*" === e
                ? function() {
                    return !0;
                  }
                : function(e) {
                    return e.nodeName && e.nodeName.toLowerCase() === t;
                  };
            },
            CLASS: function(e) {
              var t = B[e + " "];
              return (
                t ||
                ((t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) &&
                  B(e, function(e) {
                    return t.test(
                      ("string" == typeof e.className && e.className) ||
                        ("undefined" != typeof e.getAttribute &&
                          e.getAttribute("class")) ||
                        ""
                    );
                  }))
              );
            },
            ATTR: function(e, n, o) {
              return function(i) {
                var r = t.attr(i, e);
                return null == r
                  ? "!=" === n
                  : !n ||
                      ((r += ""),
                      "=" === n
                        ? r === o
                        : "!=" === n
                        ? r !== o
                        : "^=" === n
                        ? o && 0 === r.indexOf(o)
                        : "*=" === n
                        ? o && r.indexOf(o) > -1
                        : "$=" === n
                        ? o && r.slice(-o.length) === o
                        : "~=" === n
                        ? (" " + r.replace(se, " ") + " ").indexOf(o) > -1
                        : "|=" === n &&
                          (r === o || r.slice(0, o.length + 1) === o + "-"));
              };
            },
            CHILD: function(e, t, n, o, i) {
              var r = "nth" !== e.slice(0, 3),
                a = "last" !== e.slice(-4),
                s = "of-type" === t;
              return 1 === o && 0 === i
                ? function(e) {
                    return !!e.parentNode;
                  }
                : function(t, n, l) {
                    var u,
                      c,
                      p,
                      d,
                      f,
                      h,
                      m = r !== a ? "nextSibling" : "previousSibling",
                      g = t.parentNode,
                      v = s && t.nodeName.toLowerCase(),
                      y = !l && !s;
                    if (g) {
                      if (r) {
                        for (; m; ) {
                          for (p = t; (p = p[m]); )
                            if (
                              s
                                ? p.nodeName.toLowerCase() === v
                                : 1 === p.nodeType
                            )
                              return !1;
                          h = m = "only" === e && !h && "nextSibling";
                        }
                        return !0;
                      }
                      if (((h = [a ? g.firstChild : g.lastChild]), a && y)) {
                        for (
                          c = g[H] || (g[H] = {}),
                            u = c[e] || [],
                            f = u[0] === z && u[1],
                            d = u[0] === z && u[2],
                            p = f && g.childNodes[f];
                          (p = (++f && p && p[m]) || (d = f = 0) || h.pop());

                        )
                          if (1 === p.nodeType && ++d && p === t) {
                            c[e] = [z, f, d];
                            break;
                          }
                      } else if (
                        y &&
                        (u = (t[H] || (t[H] = {}))[e]) &&
                        u[0] === z
                      )
                        d = u[1];
                      else
                        for (
                          ;
                          (p = (++f && p && p[m]) || (d = f = 0) || h.pop()) &&
                          ((s
                            ? p.nodeName.toLowerCase() !== v
                            : 1 !== p.nodeType) ||
                            !++d ||
                            (y && ((p[H] || (p[H] = {}))[e] = [z, d]),
                            p !== t));

                        );
                      return (d -= i), d === o || (d % o === 0 && d / o >= 0);
                    }
                  };
            },
            PSEUDO: function(e, n) {
              var i,
                r =
                  T.pseudos[e] ||
                  T.setFilters[e.toLowerCase()] ||
                  t.error("unsupported pseudo: " + e);
              return r[H]
                ? r(n)
                : r.length > 1
                ? ((i = [e, e, "", n]),
                  T.setFilters.hasOwnProperty(e.toLowerCase())
                    ? o(function(e, t) {
                        for (var o, i = r(e, n), a = i.length; a--; )
                          (o = ee(e, i[a])), (e[o] = !(t[o] = i[a]));
                      })
                    : function(e) {
                        return r(e, 0, i);
                      })
                : r;
            }
          },
          pseudos: {
            not: o(function(e) {
              var t = [],
                n = [],
                i = S(e.replace(le, "$1"));
              return i[H]
                ? o(function(e, t, n, o) {
                    for (var r, a = i(e, null, o, []), s = e.length; s--; )
                      (r = a[s]) && (e[s] = !(t[s] = r));
                  })
                : function(e, o, r) {
                    return (
                      (t[0] = e), i(t, null, r, n), (t[0] = null), !n.pop()
                    );
                  };
            }),
            has: o(function(e) {
              return function(n) {
                return t(e, n).length > 0;
              };
            }),
            contains: o(function(e) {
              return (
                (e = e.replace(be, Te)),
                function(t) {
                  return (t.textContent || t.innerText || C(t)).indexOf(e) > -1;
                }
              );
            }),
            lang: o(function(e) {
              return (
                fe.test(e || "") || t.error("unsupported lang: " + e),
                (e = e.replace(be, Te).toLowerCase()),
                function(t) {
                  var n;
                  do
                    if (
                      (n = O
                        ? t.lang
                        : t.getAttribute("xml:lang") || t.getAttribute("lang"))
                    )
                      return (
                        (n = n.toLowerCase()),
                        n === e || 0 === n.indexOf(e + "-")
                      );
                  while ((t = t.parentNode) && 1 === t.nodeType);
                  return !1;
                }
              );
            }),
            target: function(t) {
              var n = e.location && e.location.hash;
              return n && n.slice(1) === t.id;
            },
            root: function(e) {
              return e === L;
            },
            focus: function(e) {
              return (
                e === _.activeElement &&
                (!_.hasFocus || _.hasFocus()) &&
                !!(e.type || e.href || ~e.tabIndex)
              );
            },
            enabled: function(e) {
              return e.disabled === !1;
            },
            disabled: function(e) {
              return e.disabled === !0;
            },
            checked: function(e) {
              var t = e.nodeName.toLowerCase();
              return (
                ("input" === t && !!e.checked) ||
                ("option" === t && !!e.selected)
              );
            },
            selected: function(e) {
              return (
                e.parentNode && e.parentNode.selectedIndex, e.selected === !0
              );
            },
            empty: function(e) {
              for (e = e.firstChild; e; e = e.nextSibling)
                if (e.nodeType < 6) return !1;
              return !0;
            },
            parent: function(e) {
              return !T.pseudos.empty(e);
            },
            header: function(e) {
              return ge.test(e.nodeName);
            },
            input: function(e) {
              return me.test(e.nodeName);
            },
            button: function(e) {
              var t = e.nodeName.toLowerCase();
              return ("input" === t && "button" === e.type) || "button" === t;
            },
            text: function(e) {
              var t;
              return (
                "input" === e.nodeName.toLowerCase() &&
                "text" === e.type &&
                (null == (t = e.getAttribute("type")) ||
                  "text" === t.toLowerCase())
              );
            },
            first: u(function() {
              return [0];
            }),
            last: u(function(e, t) {
              return [t - 1];
            }),
            eq: u(function(e, t, n) {
              return [n < 0 ? n + t : n];
            }),
            even: u(function(e, t) {
              for (var n = 0; n < t; n += 2) e.push(n);
              return e;
            }),
            odd: u(function(e, t) {
              for (var n = 1; n < t; n += 2) e.push(n);
              return e;
            }),
            lt: u(function(e, t, n) {
              for (var o = n < 0 ? n + t : n; --o >= 0; ) e.push(o);
              return e;
            }),
            gt: u(function(e, t, n) {
              for (var o = n < 0 ? n + t : n; ++o < t; ) e.push(o);
              return e;
            })
          }
        }),
        (T.pseudos.nth = T.pseudos.eq);
      for (w in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 })
        T.pseudos[w] = s(w);
      for (w in { submit: !0, reset: !0 }) T.pseudos[w] = l(w);
      return (
        (p.prototype = T.filters = T.pseudos),
        (T.setFilters = new p()),
        (k = t.tokenize = function(e, n) {
          var o,
            i,
            r,
            a,
            s,
            l,
            u,
            c = Z[e + " "];
          if (c) return n ? 0 : c.slice(0);
          for (s = e, l = [], u = T.preFilter; s; ) {
            (o && !(i = ue.exec(s))) ||
              (i && (s = s.slice(i[0].length) || s), l.push((r = []))),
              (o = !1),
              (i = ce.exec(s)) &&
                ((o = i.shift()),
                r.push({ value: o, type: i[0].replace(le, " ") }),
                (s = s.slice(o.length)));
            for (a in T.filter)
              !(i = he[a].exec(s)) ||
                (u[a] && !(i = u[a](i))) ||
                ((o = i.shift()),
                r.push({ value: o, type: a, matches: i }),
                (s = s.slice(o.length)));
            if (!o) break;
          }
          return n ? s.length : s ? t.error(e) : Z(e, l).slice(0);
        }),
        (S = t.compile = function(e, t) {
          var n,
            o = [],
            i = [],
            r = U[e + " "];
          if (!r) {
            for (t || (t = k(e)), n = t.length; n--; )
              (r = y(t[n])), r[H] ? o.push(r) : i.push(r);
            (r = U(e, x(i, o))), (r.selector = e);
          }
          return r;
        }),
        (D = t.select = function(e, t, n, o) {
          var i,
            r,
            a,
            s,
            l,
            u = "function" == typeof e && e,
            p = !o && k((e = u.selector || e));
          if (((n = n || []), 1 === p.length)) {
            if (
              ((r = p[0] = p[0].slice(0)),
              r.length > 2 &&
                "ID" === (a = r[0]).type &&
                b.getById &&
                9 === t.nodeType &&
                O &&
                T.relative[r[1].type])
            ) {
              if (
                ((t = (T.find.ID(a.matches[0].replace(be, Te), t) || [])[0]),
                !t)
              )
                return n;
              u && (t = t.parentNode), (e = e.slice(r.shift().value.length));
            }
            for (
              i = he.needsContext.test(e) ? 0 : r.length;
              i-- && ((a = r[i]), !T.relative[(s = a.type)]);

            )
              if (
                (l = T.find[s]) &&
                (o = l(
                  a.matches[0].replace(be, Te),
                  (xe.test(r[0].type) && c(t.parentNode)) || t
                ))
              ) {
                if ((r.splice(i, 1), (e = o.length && d(r)), !e))
                  return Q.apply(n, o), n;
                break;
              }
          }
          return (
            (u || S(e, p))(o, t, !O, n, (xe.test(e) && c(t.parentNode)) || t), n
          );
        }),
        (b.sortStable =
          H.split("")
            .sort($)
            .join("") === H),
        (b.detectDuplicates = !!F),
        A(),
        (b.sortDetached = i(function(e) {
          return 1 & e.compareDocumentPosition(_.createElement("div"));
        })),
        i(function(e) {
          return (
            (e.innerHTML = "<a href='#'></a>"),
            "#" === e.firstChild.getAttribute("href")
          );
        }) ||
          r("type|href|height|width", function(e, t, n) {
            if (!n)
              return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2);
          }),
        (b.attributes &&
          i(function(e) {
            return (
              (e.innerHTML = "<input/>"),
              e.firstChild.setAttribute("value", ""),
              "" === e.firstChild.getAttribute("value")
            );
          })) ||
          r("value", function(e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase())
              return e.defaultValue;
          }),
        i(function(e) {
          return null == e.getAttribute("disabled");
        }) ||
          r(te, function(e, t, n) {
            var o;
            if (!n)
              return e[t] === !0
                ? t.toLowerCase()
                : (o = e.getAttributeNode(t)) && o.specified
                ? o.value
                : null;
          }),
        t
      );
    })(e);
    (J.find = ie),
      (J.expr = ie.selectors),
      (J.expr[":"] = J.expr.pseudos),
      (J.unique = ie.uniqueSort),
      (J.text = ie.getText),
      (J.isXMLDoc = ie.isXML),
      (J.contains = ie.contains);
    var re = J.expr.match.needsContext,
      ae = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
      se = /^.[^:#\[\.,]*$/;
    (J.filter = function(e, t, n) {
      var o = t[0];
      return (
        n && (e = ":not(" + e + ")"),
        1 === t.length && 1 === o.nodeType
          ? J.find.matchesSelector(o, e)
            ? [o]
            : []
          : J.find.matches(
              e,
              J.grep(t, function(e) {
                return 1 === e.nodeType;
              })
            )
      );
    }),
      J.fn.extend({
        find: function(e) {
          var t,
            n = this.length,
            o = [],
            i = this;
          if ("string" != typeof e)
            return this.pushStack(
              J(e).filter(function() {
                for (t = 0; t < n; t++) if (J.contains(i[t], this)) return !0;
              })
            );
          for (t = 0; t < n; t++) J.find(e, i[t], o);
          return (
            (o = this.pushStack(n > 1 ? J.unique(o) : o)),
            (o.selector = this.selector ? this.selector + " " + e : e),
            o
          );
        },
        filter: function(e) {
          return this.pushStack(o(this, e || [], !1));
        },
        not: function(e) {
          return this.pushStack(o(this, e || [], !0));
        },
        is: function(e) {
          return !!o(
            this,
            "string" == typeof e && re.test(e) ? J(e) : e || [],
            !1
          ).length;
        }
      });
    var le,
      ue = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
      ce = (J.fn.init = function(e, t) {
        var n, o;
        if (!e) return this;
        if ("string" == typeof e) {
          if (
            ((n =
              "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3
                ? [null, e, null]
                : ue.exec(e)),
            !n || (!n[1] && t))
          )
            return !t || t.jquery
              ? (t || le).find(e)
              : this.constructor(t).find(e);
          if (n[1]) {
            if (
              ((t = t instanceof J ? t[0] : t),
              J.merge(
                this,
                J.parseHTML(
                  n[1],
                  t && t.nodeType ? t.ownerDocument || t : V,
                  !0
                )
              ),
              ae.test(n[1]) && J.isPlainObject(t))
            )
              for (n in t)
                J.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
            return this;
          }
          return (
            (o = V.getElementById(n[2])),
            o && o.parentNode && ((this.length = 1), (this[0] = o)),
            (this.context = V),
            (this.selector = e),
            this
          );
        }
        return e.nodeType
          ? ((this.context = this[0] = e), (this.length = 1), this)
          : J.isFunction(e)
          ? "undefined" != typeof le.ready
            ? le.ready(e)
            : e(J)
          : (void 0 !== e.selector &&
              ((this.selector = e.selector), (this.context = e.context)),
            J.makeArray(e, this));
      });
    (ce.prototype = J.fn), (le = J(V));
    var pe = /^(?:parents|prev(?:Until|All))/,
      de = { children: !0, contents: !0, next: !0, prev: !0 };
    J.extend({
      dir: function(e, t, n) {
        for (var o = [], i = void 0 !== n; (e = e[t]) && 9 !== e.nodeType; )
          if (1 === e.nodeType) {
            if (i && J(e).is(n)) break;
            o.push(e);
          }
        return o;
      },
      sibling: function(e, t) {
        for (var n = []; e; e = e.nextSibling)
          1 === e.nodeType && e !== t && n.push(e);
        return n;
      }
    }),
      J.fn.extend({
        has: function(e) {
          var t = J(e, this),
            n = t.length;
          return this.filter(function() {
            for (var e = 0; e < n; e++) if (J.contains(this, t[e])) return !0;
          });
        },
        closest: function(e, t) {
          for (
            var n,
              o = 0,
              i = this.length,
              r = [],
              a =
                re.test(e) || "string" != typeof e
                  ? J(e, t || this.context)
                  : 0;
            o < i;
            o++
          )
            for (n = this[o]; n && n !== t; n = n.parentNode)
              if (
                n.nodeType < 11 &&
                (a
                  ? a.index(n) > -1
                  : 1 === n.nodeType && J.find.matchesSelector(n, e))
              ) {
                r.push(n);
                break;
              }
          return this.pushStack(r.length > 1 ? J.unique(r) : r);
        },
        index: function(e) {
          return e
            ? "string" == typeof e
              ? $.call(J(e), this[0])
              : $.call(this, e.jquery ? e[0] : e)
            : this[0] && this[0].parentNode
            ? this.first().prevAll().length
            : -1;
        },
        add: function(e, t) {
          return this.pushStack(J.unique(J.merge(this.get(), J(e, t))));
        },
        addBack: function(e) {
          return this.add(
            null == e ? this.prevObject : this.prevObject.filter(e)
          );
        }
      }),
      J.each(
        {
          parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null;
          },
          parents: function(e) {
            return J.dir(e, "parentNode");
          },
          parentsUntil: function(e, t, n) {
            return J.dir(e, "parentNode", n);
          },
          next: function(e) {
            return i(e, "nextSibling");
          },
          prev: function(e) {
            return i(e, "previousSibling");
          },
          nextAll: function(e) {
            return J.dir(e, "nextSibling");
          },
          prevAll: function(e) {
            return J.dir(e, "previousSibling");
          },
          nextUntil: function(e, t, n) {
            return J.dir(e, "nextSibling", n);
          },
          prevUntil: function(e, t, n) {
            return J.dir(e, "previousSibling", n);
          },
          siblings: function(e) {
            return J.sibling((e.parentNode || {}).firstChild, e);
          },
          children: function(e) {
            return J.sibling(e.firstChild);
          },
          contents: function(e) {
            return e.contentDocument || J.merge([], e.childNodes);
          }
        },
        function(e, t) {
          J.fn[e] = function(n, o) {
            var i = J.map(this, t, n);
            return (
              "Until" !== e.slice(-5) && (o = n),
              o && "string" == typeof o && (i = J.filter(o, i)),
              this.length > 1 &&
                (de[e] || J.unique(i), pe.test(e) && i.reverse()),
              this.pushStack(i)
            );
          };
        }
      );
    var fe = /\S+/g,
      he = {};
    (J.Callbacks = function(e) {
      e = "string" == typeof e ? he[e] || r(e) : J.extend({}, e);
      var t,
        n,
        o,
        i,
        a,
        s,
        l = [],
        u = !e.once && [],
        c = function(r) {
          for (
            t = e.memory && r, n = !0, s = i || 0, i = 0, a = l.length, o = !0;
            l && s < a;
            s++
          )
            if (l[s].apply(r[0], r[1]) === !1 && e.stopOnFalse) {
              t = !1;
              break;
            }
          (o = !1),
            l && (u ? u.length && c(u.shift()) : t ? (l = []) : p.disable());
        },
        p = {
          add: function() {
            if (l) {
              var n = l.length;
              !(function r(t) {
                J.each(t, function(t, n) {
                  var o = J.type(n);
                  "function" === o
                    ? (e.unique && p.has(n)) || l.push(n)
                    : n && n.length && "string" !== o && r(n);
                });
              })(arguments),
                o ? (a = l.length) : t && ((i = n), c(t));
            }
            return this;
          },
          remove: function() {
            return (
              l &&
                J.each(arguments, function(e, t) {
                  for (var n; (n = J.inArray(t, l, n)) > -1; )
                    l.splice(n, 1), o && (n <= a && a--, n <= s && s--);
                }),
              this
            );
          },
          has: function(e) {
            return e ? J.inArray(e, l) > -1 : !(!l || !l.length);
          },
          empty: function() {
            return (l = []), (a = 0), this;
          },
          disable: function() {
            return (l = u = t = void 0), this;
          },
          disabled: function() {
            return !l;
          },
          lock: function() {
            return (u = void 0), t || p.disable(), this;
          },
          locked: function() {
            return !u;
          },
          fireWith: function(e, t) {
            return (
              !l ||
                (n && !u) ||
                ((t = t || []),
                (t = [e, t.slice ? t.slice() : t]),
                o ? u.push(t) : c(t)),
              this
            );
          },
          fire: function() {
            return p.fireWith(this, arguments), this;
          },
          fired: function() {
            return !!n;
          }
        };
      return p;
    }),
      J.extend({
        Deferred: function(e) {
          var t = [
              ["resolve", "done", J.Callbacks("once memory"), "resolved"],
              ["reject", "fail", J.Callbacks("once memory"), "rejected"],
              ["notify", "progress", J.Callbacks("memory")]
            ],
            n = "pending",
            o = {
              state: function() {
                return n;
              },
              always: function() {
                return i.done(arguments).fail(arguments), this;
              },
              then: function() {
                var e = arguments;
                return J.Deferred(function(n) {
                  J.each(t, function(t, r) {
                    var a = J.isFunction(e[t]) && e[t];
                    i[r[1]](function() {
                      var e = a && a.apply(this, arguments);
                      e && J.isFunction(e.promise)
                        ? e
                            .promise()
                            .done(n.resolve)
                            .fail(n.reject)
                            .progress(n.notify)
                        : n[r[0] + "With"](
                            this === o ? n.promise() : this,
                            a ? [e] : arguments
                          );
                    });
                  }),
                    (e = null);
                }).promise();
              },
              promise: function(e) {
                return null != e ? J.extend(e, o) : o;
              }
            },
            i = {};
          return (
            (o.pipe = o.then),
            J.each(t, function(e, r) {
              var a = r[2],
                s = r[3];
              (o[r[1]] = a.add),
                s &&
                  a.add(
                    function() {
                      n = s;
                    },
                    t[1 ^ e][2].disable,
                    t[2][2].lock
                  ),
                (i[r[0]] = function() {
                  return (
                    i[r[0] + "With"](this === i ? o : this, arguments), this
                  );
                }),
                (i[r[0] + "With"] = a.fireWith);
            }),
            o.promise(i),
            e && e.call(i, i),
            i
          );
        },
        when: function(e) {
          var t,
            n,
            o,
            i = 0,
            r = B.call(arguments),
            a = r.length,
            s = 1 !== a || (e && J.isFunction(e.promise)) ? a : 0,
            l = 1 === s ? e : J.Deferred(),
            u = function(e, n, o) {
              return function(i) {
                (n[e] = this),
                  (o[e] = arguments.length > 1 ? B.call(arguments) : i),
                  o === t ? l.notifyWith(n, o) : --s || l.resolveWith(n, o);
              };
            };
          if (a > 1)
            for (
              t = new Array(a), n = new Array(a), o = new Array(a);
              i < a;
              i++
            )
              r[i] && J.isFunction(r[i].promise)
                ? r[i]
                    .promise()
                    .done(u(i, o, r))
                    .fail(l.reject)
                    .progress(u(i, n, t))
                : --s;
          return s || l.resolveWith(o, r), l.promise();
        }
      });
    var me;
    (J.fn.ready = function(e) {
      return J.ready.promise().done(e), this;
    }),
      J.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
          e ? J.readyWait++ : J.ready(!0);
        },
        ready: function(e) {
          (e === !0 ? --J.readyWait : J.isReady) ||
            ((J.isReady = !0),
            (e !== !0 && --J.readyWait > 0) ||
              (me.resolveWith(V, [J]),
              J.fn.triggerHandler &&
                (J(V).triggerHandler("ready"), J(V).off("ready"))));
        }
      }),
      (J.ready.promise = function(t) {
        return (
          me ||
            ((me = J.Deferred()),
            "complete" === V.readyState
              ? setTimeout(J.ready)
              : (V.addEventListener("DOMContentLoaded", a, !1),
                e.addEventListener("load", a, !1))),
          me.promise(t)
        );
      }),
      J.ready.promise();
    var ge = (J.access = function(e, t, n, o, i, r, a) {
      var s = 0,
        l = e.length,
        u = null == n;
      if ("object" === J.type(n)) {
        i = !0;
        for (s in n) J.access(e, t, s, n[s], !0, r, a);
      } else if (
        void 0 !== o &&
        ((i = !0),
        J.isFunction(o) || (a = !0),
        u &&
          (a
            ? (t.call(e, o), (t = null))
            : ((u = t),
              (t = function(e, t, n) {
                return u.call(J(e), n);
              }))),
        t)
      )
        for (; s < l; s++) t(e[s], n, a ? o : o.call(e[s], s, t(e[s], n)));
      return i ? e : u ? t.call(e) : l ? t(e[0], n) : r;
    });
    (J.acceptData = function(e) {
      return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType;
    }),
      (s.uid = 1),
      (s.accepts = J.acceptData),
      (s.prototype = {
        key: function(e) {
          if (!s.accepts(e)) return 0;
          var t = {},
            n = e[this.expando];
          if (!n) {
            n = s.uid++;
            try {
              (t[this.expando] = { value: n }), Object.defineProperties(e, t);
            } catch (o) {
              (t[this.expando] = n), J.extend(e, t);
            }
          }
          return this.cache[n] || (this.cache[n] = {}), n;
        },
        set: function(e, t, n) {
          var o,
            i = this.key(e),
            r = this.cache[i];
          if ("string" == typeof t) r[t] = n;
          else if (J.isEmptyObject(r)) J.extend(this.cache[i], t);
          else for (o in t) r[o] = t[o];
          return r;
        },
        get: function(e, t) {
          var n = this.cache[this.key(e)];
          return void 0 === t ? n : n[t];
        },
        access: function(e, t, n) {
          var o;
          return void 0 === t || (t && "string" == typeof t && void 0 === n)
            ? ((o = this.get(e, t)),
              void 0 !== o ? o : this.get(e, J.camelCase(t)))
            : (this.set(e, t, n), void 0 !== n ? n : t);
        },
        remove: function(e, t) {
          var n,
            o,
            i,
            r = this.key(e),
            a = this.cache[r];
          if (void 0 === t) this.cache[r] = {};
          else {
            J.isArray(t)
              ? (o = t.concat(t.map(J.camelCase)))
              : ((i = J.camelCase(t)),
                t in a
                  ? (o = [t, i])
                  : ((o = i), (o = o in a ? [o] : o.match(fe) || []))),
              (n = o.length);
            for (; n--; ) delete a[o[n]];
          }
        },
        hasData: function(e) {
          return !J.isEmptyObject(this.cache[e[this.expando]] || {});
        },
        discard: function(e) {
          e[this.expando] && delete this.cache[e[this.expando]];
        }
      });
    var ve = new s(),
      ye = new s(),
      xe = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      we = /([A-Z])/g;
    J.extend({
      hasData: function(e) {
        return ye.hasData(e) || ve.hasData(e);
      },
      data: function(e, t, n) {
        return ye.access(e, t, n);
      },
      removeData: function(e, t) {
        ye.remove(e, t);
      },
      _data: function(e, t, n) {
        return ve.access(e, t, n);
      },
      _removeData: function(e, t) {
        ve.remove(e, t);
      }
    }),
      J.fn.extend({
        data: function(e, t) {
          var n,
            o,
            i,
            r = this[0],
            a = r && r.attributes;
          if (void 0 === e) {
            if (
              this.length &&
              ((i = ye.get(r)), 1 === r.nodeType && !ve.get(r, "hasDataAttrs"))
            ) {
              for (n = a.length; n--; )
                a[n] &&
                  ((o = a[n].name),
                  0 === o.indexOf("data-") &&
                    ((o = J.camelCase(o.slice(5))), l(r, o, i[o])));
              ve.set(r, "hasDataAttrs", !0);
            }
            return i;
          }
          return "object" == typeof e
            ? this.each(function() {
                ye.set(this, e);
              })
            : ge(
                this,
                function(t) {
                  var n,
                    o = J.camelCase(e);
                  if (r && void 0 === t) {
                    if (((n = ye.get(r, e)), void 0 !== n)) return n;
                    if (((n = ye.get(r, o)), void 0 !== n)) return n;
                    if (((n = l(r, o, void 0)), void 0 !== n)) return n;
                  } else
                    this.each(function() {
                      var n = ye.get(this, o);
                      ye.set(this, o, t),
                        e.indexOf("-") !== -1 &&
                          void 0 !== n &&
                          ye.set(this, e, t);
                    });
                },
                null,
                t,
                arguments.length > 1,
                null,
                !0
              );
        },
        removeData: function(e) {
          return this.each(function() {
            ye.remove(this, e);
          });
        }
      }),
      J.extend({
        queue: function(e, t, n) {
          var o;
          if (e)
            return (
              (t = (t || "fx") + "queue"),
              (o = ve.get(e, t)),
              n &&
                (!o || J.isArray(n)
                  ? (o = ve.access(e, t, J.makeArray(n)))
                  : o.push(n)),
              o || []
            );
        },
        dequeue: function(e, t) {
          t = t || "fx";
          var n = J.queue(e, t),
            o = n.length,
            i = n.shift(),
            r = J._queueHooks(e, t),
            a = function() {
              J.dequeue(e, t);
            };
          "inprogress" === i && ((i = n.shift()), o--),
            i &&
              ("fx" === t && n.unshift("inprogress"),
              delete r.stop,
              i.call(e, a, r)),
            !o && r && r.empty.fire();
        },
        _queueHooks: function(e, t) {
          var n = t + "queueHooks";
          return (
            ve.get(e, n) ||
            ve.access(e, n, {
              empty: J.Callbacks("once memory").add(function() {
                ve.remove(e, [t + "queue", n]);
              })
            })
          );
        }
      }),
      J.fn.extend({
        queue: function(e, t) {
          var n = 2;
          return (
            "string" != typeof e && ((t = e), (e = "fx"), n--),
            arguments.length < n
              ? J.queue(this[0], e)
              : void 0 === t
              ? this
              : this.each(function() {
                  var n = J.queue(this, e, t);
                  J._queueHooks(this, e),
                    "fx" === e && "inprogress" !== n[0] && J.dequeue(this, e);
                })
          );
        },
        dequeue: function(e) {
          return this.each(function() {
            J.dequeue(this, e);
          });
        },
        clearQueue: function(e) {
          return this.queue(e || "fx", []);
        },
        promise: function(e, t) {
          var n,
            o = 1,
            i = J.Deferred(),
            r = this,
            a = this.length,
            s = function() {
              --o || i.resolveWith(r, [r]);
            };
          for (
            "string" != typeof e && ((t = e), (e = void 0)), e = e || "fx";
            a--;

          )
            (n = ve.get(r[a], e + "queueHooks")),
              n && n.empty && (o++, n.empty.add(s));
          return s(), i.promise(t);
        }
      });
    var be = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
      Te = ["Top", "Right", "Bottom", "Left"],
      Ce = function(e, t) {
        return (
          (e = t || e),
          "none" === J.css(e, "display") || !J.contains(e.ownerDocument, e)
        );
      },
      Ee = /^(?:checkbox|radio)$/i;
    !(function() {
      var e = V.createDocumentFragment(),
        t = e.appendChild(V.createElement("div")),
        n = V.createElement("input");
      n.setAttribute("type", "radio"),
        n.setAttribute("checked", "checked"),
        n.setAttribute("name", "t"),
        t.appendChild(n),
        (Y.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked),
        (t.innerHTML = "<textarea>x</textarea>"),
        (Y.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue);
    })();
    var ke = "undefined";
    Y.focusinBubbles = "onfocusin" in e;
    var Se = /^key/,
      De = /^(?:mouse|pointer|contextmenu)|click/,
      Ie = /^(?:focusinfocus|focusoutblur)$/,
      Ne = /^([^.]*)(?:\.(.+)|)$/;
    (J.event = {
      global: {},
      add: function(e, t, n, o, i) {
        var r,
          a,
          s,
          l,
          u,
          c,
          p,
          d,
          f,
          h,
          m,
          g = ve.get(e);
        if (g)
          for (
            n.handler && ((r = n), (n = r.handler), (i = r.selector)),
              n.guid || (n.guid = J.guid++),
              (l = g.events) || (l = g.events = {}),
              (a = g.handle) ||
                (a = g.handle = function(t) {
                  return typeof J !== ke && J.event.triggered !== t.type
                    ? J.event.dispatch.apply(e, arguments)
                    : void 0;
                }),
              t = (t || "").match(fe) || [""],
              u = t.length;
            u--;

          )
            (s = Ne.exec(t[u]) || []),
              (f = m = s[1]),
              (h = (s[2] || "").split(".").sort()),
              f &&
                ((p = J.event.special[f] || {}),
                (f = (i ? p.delegateType : p.bindType) || f),
                (p = J.event.special[f] || {}),
                (c = J.extend(
                  {
                    type: f,
                    origType: m,
                    data: o,
                    handler: n,
                    guid: n.guid,
                    selector: i,
                    needsContext: i && J.expr.match.needsContext.test(i),
                    namespace: h.join(".")
                  },
                  r
                )),
                (d = l[f]) ||
                  ((d = l[f] = []),
                  (d.delegateCount = 0),
                  (p.setup && p.setup.call(e, o, h, a) !== !1) ||
                    (e.addEventListener && e.addEventListener(f, a, !1))),
                p.add &&
                  (p.add.call(e, c),
                  c.handler.guid || (c.handler.guid = n.guid)),
                i ? d.splice(d.delegateCount++, 0, c) : d.push(c),
                (J.event.global[f] = !0));
      },
      remove: function(e, t, n, o, i) {
        var r,
          a,
          s,
          l,
          u,
          c,
          p,
          d,
          f,
          h,
          m,
          g = ve.hasData(e) && ve.get(e);
        if (g && (l = g.events)) {
          for (t = (t || "").match(fe) || [""], u = t.length; u--; )
            if (
              ((s = Ne.exec(t[u]) || []),
              (f = m = s[1]),
              (h = (s[2] || "").split(".").sort()),
              f)
            ) {
              for (
                p = J.event.special[f] || {},
                  f = (o ? p.delegateType : p.bindType) || f,
                  d = l[f] || [],
                  s =
                    s[2] &&
                    new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                  a = r = d.length;
                r--;

              )
                (c = d[r]),
                  (!i && m !== c.origType) ||
                    (n && n.guid !== c.guid) ||
                    (s && !s.test(c.namespace)) ||
                    (o && o !== c.selector && ("**" !== o || !c.selector)) ||
                    (d.splice(r, 1),
                    c.selector && d.delegateCount--,
                    p.remove && p.remove.call(e, c));
              a &&
                !d.length &&
                ((p.teardown && p.teardown.call(e, h, g.handle) !== !1) ||
                  J.removeEvent(e, f, g.handle),
                delete l[f]);
            } else for (f in l) J.event.remove(e, f + t[u], n, o, !0);
          J.isEmptyObject(l) && (delete g.handle, ve.remove(e, "events"));
        }
      },
      trigger: function(t, n, o, i) {
        var r,
          a,
          s,
          l,
          u,
          c,
          p,
          d = [o || V],
          f = G.call(t, "type") ? t.type : t,
          h = G.call(t, "namespace") ? t.namespace.split(".") : [];
        if (
          ((a = s = o = o || V),
          3 !== o.nodeType &&
            8 !== o.nodeType &&
            !Ie.test(f + J.event.triggered) &&
            (f.indexOf(".") >= 0 &&
              ((h = f.split(".")), (f = h.shift()), h.sort()),
            (u = f.indexOf(":") < 0 && "on" + f),
            (t = t[J.expando] ? t : new J.Event(f, "object" == typeof t && t)),
            (t.isTrigger = i ? 2 : 3),
            (t.namespace = h.join(".")),
            (t.namespace_re = t.namespace
              ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)")
              : null),
            (t.result = void 0),
            t.target || (t.target = o),
            (n = null == n ? [t] : J.makeArray(n, [t])),
            (p = J.event.special[f] || {}),
            i || !p.trigger || p.trigger.apply(o, n) !== !1))
        ) {
          if (!i && !p.noBubble && !J.isWindow(o)) {
            for (
              l = p.delegateType || f, Ie.test(l + f) || (a = a.parentNode);
              a;
              a = a.parentNode
            )
              d.push(a), (s = a);
            s === (o.ownerDocument || V) &&
              d.push(s.defaultView || s.parentWindow || e);
          }
          for (r = 0; (a = d[r++]) && !t.isPropagationStopped(); )
            (t.type = r > 1 ? l : p.bindType || f),
              (c = (ve.get(a, "events") || {})[t.type] && ve.get(a, "handle")),
              c && c.apply(a, n),
              (c = u && a[u]),
              c &&
                c.apply &&
                J.acceptData(a) &&
                ((t.result = c.apply(a, n)),
                t.result === !1 && t.preventDefault());
          return (
            (t.type = f),
            i ||
              t.isDefaultPrevented() ||
              (p._default && p._default.apply(d.pop(), n) !== !1) ||
              !J.acceptData(o) ||
              (u &&
                J.isFunction(o[f]) &&
                !J.isWindow(o) &&
                ((s = o[u]),
                s && (o[u] = null),
                (J.event.triggered = f),
                o[f](),
                (J.event.triggered = void 0),
                s && (o[u] = s))),
            t.result
          );
        }
      },
      dispatch: function(e) {
        e = J.event.fix(e);
        var t,
          n,
          o,
          i,
          r,
          a = [],
          s = B.call(arguments),
          l = (ve.get(this, "events") || {})[e.type] || [],
          u = J.event.special[e.type] || {};
        if (
          ((s[0] = e),
          (e.delegateTarget = this),
          !u.preDispatch || u.preDispatch.call(this, e) !== !1)
        ) {
          for (
            a = J.event.handlers.call(this, e, l), t = 0;
            (i = a[t++]) && !e.isPropagationStopped();

          )
            for (
              e.currentTarget = i.elem, n = 0;
              (r = i.handlers[n++]) && !e.isImmediatePropagationStopped();

            )
              (e.namespace_re && !e.namespace_re.test(r.namespace)) ||
                ((e.handleObj = r),
                (e.data = r.data),
                (o = (
                  (J.event.special[r.origType] || {}).handle || r.handler
                ).apply(i.elem, s)),
                void 0 !== o &&
                  (e.result = o) === !1 &&
                  (e.preventDefault(), e.stopPropagation()));
          return u.postDispatch && u.postDispatch.call(this, e), e.result;
        }
      },
      handlers: function(e, t) {
        var n,
          o,
          i,
          r,
          a = [],
          s = t.delegateCount,
          l = e.target;
        if (s && l.nodeType && (!e.button || "click" !== e.type))
          for (; l !== this; l = l.parentNode || this)
            if (l.disabled !== !0 || "click" !== e.type) {
              for (o = [], n = 0; n < s; n++)
                (r = t[n]),
                  (i = r.selector + " "),
                  void 0 === o[i] &&
                    (o[i] = r.needsContext
                      ? J(i, this).index(l) >= 0
                      : J.find(i, this, null, [l]).length),
                  o[i] && o.push(r);
              o.length && a.push({ elem: l, handlers: o });
            }
        return s < t.length && a.push({ elem: this, handlers: t.slice(s) }), a;
      },
      props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(
        " "
      ),
      fixHooks: {},
      keyHooks: {
        props: "char charCode key keyCode".split(" "),
        filter: function(e, t) {
          return (
            null == e.which &&
              (e.which = null != t.charCode ? t.charCode : t.keyCode),
            e
          );
        }
      },
      mouseHooks: {
        props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(
          " "
        ),
        filter: function(e, t) {
          var n,
            o,
            i,
            r = t.button;
          return (
            null == e.pageX &&
              null != t.clientX &&
              ((n = e.target.ownerDocument || V),
              (o = n.documentElement),
              (i = n.body),
              (e.pageX =
                t.clientX +
                ((o && o.scrollLeft) || (i && i.scrollLeft) || 0) -
                ((o && o.clientLeft) || (i && i.clientLeft) || 0)),
              (e.pageY =
                t.clientY +
                ((o && o.scrollTop) || (i && i.scrollTop) || 0) -
                ((o && o.clientTop) || (i && i.clientTop) || 0))),
            e.which ||
              void 0 === r ||
              (e.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0),
            e
          );
        }
      },
      fix: function(e) {
        if (e[J.expando]) return e;
        var t,
          n,
          o,
          i = e.type,
          r = e,
          a = this.fixHooks[i];
        for (
          a ||
            (this.fixHooks[i] = a = De.test(i)
              ? this.mouseHooks
              : Se.test(i)
              ? this.keyHooks
              : {}),
            o = a.props ? this.props.concat(a.props) : this.props,
            e = new J.Event(r),
            t = o.length;
          t--;

        )
          (n = o[t]), (e[n] = r[n]);
        return (
          e.target || (e.target = V),
          3 === e.target.nodeType && (e.target = e.target.parentNode),
          a.filter ? a.filter(e, r) : e
        );
      },
      special: {
        load: { noBubble: !0 },
        focus: {
          trigger: function() {
            if (this !== p() && this.focus) return this.focus(), !1;
          },
          delegateType: "focusin"
        },
        blur: {
          trigger: function() {
            if (this === p() && this.blur) return this.blur(), !1;
          },
          delegateType: "focusout"
        },
        click: {
          trigger: function() {
            if (
              "checkbox" === this.type &&
              this.click &&
              J.nodeName(this, "input")
            )
              return this.click(), !1;
          },
          _default: function(e) {
            return J.nodeName(e.target, "a");
          }
        },
        beforeunload: {
          postDispatch: function(e) {
            void 0 !== e.result &&
              e.originalEvent &&
              (e.originalEvent.returnValue = e.result);
          }
        }
      },
      simulate: function(e, t, n, o) {
        var i = J.extend(new J.Event(), n, {
          type: e,
          isSimulated: !0,
          originalEvent: {}
        });
        o ? J.event.trigger(i, null, t) : J.event.dispatch.call(t, i),
          i.isDefaultPrevented() && n.preventDefault();
      }
    }),
      (J.removeEvent = function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n, !1);
      }),
      (J.Event = function(e, t) {
        return this instanceof J.Event
          ? (e && e.type
              ? ((this.originalEvent = e),
                (this.type = e.type),
                (this.isDefaultPrevented =
                  e.defaultPrevented ||
                  (void 0 === e.defaultPrevented && e.returnValue === !1)
                    ? u
                    : c))
              : (this.type = e),
            t && J.extend(this, t),
            (this.timeStamp = (e && e.timeStamp) || J.now()),
            void (this[J.expando] = !0))
          : new J.Event(e, t);
      }),
      (J.Event.prototype = {
        isDefaultPrevented: c,
        isPropagationStopped: c,
        isImmediatePropagationStopped: c,
        preventDefault: function() {
          var e = this.originalEvent;
          (this.isDefaultPrevented = u),
            e && e.preventDefault && e.preventDefault();
        },
        stopPropagation: function() {
          var e = this.originalEvent;
          (this.isPropagationStopped = u),
            e && e.stopPropagation && e.stopPropagation();
        },
        stopImmediatePropagation: function() {
          var e = this.originalEvent;
          (this.isImmediatePropagationStopped = u),
            e && e.stopImmediatePropagation && e.stopImmediatePropagation(),
            this.stopPropagation();
        }
      }),
      J.each(
        {
          mouseenter: "mouseover",
          mouseleave: "mouseout",
          pointerenter: "pointerover",
          pointerleave: "pointerout"
        },
        function(e, t) {
          J.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
              var n,
                o = this,
                i = e.relatedTarget,
                r = e.handleObj;
              return (
                (i && (i === o || J.contains(o, i))) ||
                  ((e.type = r.origType),
                  (n = r.handler.apply(this, arguments)),
                  (e.type = t)),
                n
              );
            }
          };
        }
      ),
      Y.focusinBubbles ||
        J.each({ focus: "focusin", blur: "focusout" }, function(e, t) {
          var n = function(e) {
            J.event.simulate(t, e.target, J.event.fix(e), !0);
          };
          J.event.special[t] = {
            setup: function() {
              var o = this.ownerDocument || this,
                i = ve.access(o, t);
              i || o.addEventListener(e, n, !0), ve.access(o, t, (i || 0) + 1);
            },
            teardown: function() {
              var o = this.ownerDocument || this,
                i = ve.access(o, t) - 1;
              i
                ? ve.access(o, t, i)
                : (o.removeEventListener(e, n, !0), ve.remove(o, t));
            }
          };
        }),
      J.fn.extend({
        on: function(e, t, n, o, i) {
          var r, a;
          if ("object" == typeof e) {
            "string" != typeof t && ((n = n || t), (t = void 0));
            for (a in e) this.on(a, t, n, e[a], i);
            return this;
          }
          if (
            (null == n && null == o
              ? ((o = t), (n = t = void 0))
              : null == o &&
                ("string" == typeof t
                  ? ((o = n), (n = void 0))
                  : ((o = n), (n = t), (t = void 0))),
            o === !1)
          )
            o = c;
          else if (!o) return this;
          return (
            1 === i &&
              ((r = o),
              (o = function(e) {
                return J().off(e), r.apply(this, arguments);
              }),
              (o.guid = r.guid || (r.guid = J.guid++))),
            this.each(function() {
              J.event.add(this, e, o, n, t);
            })
          );
        },
        one: function(e, t, n, o) {
          return this.on(e, t, n, o, 1);
        },
        off: function(e, t, n) {
          var o, i;
          if (e && e.preventDefault && e.handleObj)
            return (
              (o = e.handleObj),
              J(e.delegateTarget).off(
                o.namespace ? o.origType + "." + o.namespace : o.origType,
                o.selector,
                o.handler
              ),
              this
            );
          if ("object" == typeof e) {
            for (i in e) this.off(i, t, e[i]);
            return this;
          }
          return (
            (t !== !1 && "function" != typeof t) || ((n = t), (t = void 0)),
            n === !1 && (n = c),
            this.each(function() {
              J.event.remove(this, e, n, t);
            })
          );
        },
        trigger: function(e, t) {
          return this.each(function() {
            J.event.trigger(e, t, this);
          });
        },
        triggerHandler: function(e, t) {
          var n = this[0];
          if (n) return J.event.trigger(e, t, n, !0);
        }
      });
    var Fe = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
      Ae = /<([\w:]+)/,
      _e = /<|&#?\w+;/,
      Le = /<(?:script|style|link)/i,
      Oe = /checked\s*(?:[^=]|=\s*.checked.)/i,
      Me = /^$|\/(?:java|ecma)script/i,
      Re = /^true\/(.*)/,
      Pe = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
      je = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
      };
    (je.optgroup = je.option),
      (je.tbody = je.tfoot = je.colgroup = je.caption = je.thead),
      (je.th = je.td),
      J.extend({
        clone: function(e, t, n) {
          var o,
            i,
            r,
            a,
            s = e.cloneNode(!0),
            l = J.contains(e.ownerDocument, e);
          if (
            !(
              Y.noCloneChecked ||
              (1 !== e.nodeType && 11 !== e.nodeType) ||
              J.isXMLDoc(e)
            )
          )
            for (a = v(s), r = v(e), o = 0, i = r.length; o < i; o++)
              y(r[o], a[o]);
          if (t)
            if (n)
              for (
                r = r || v(e), a = a || v(s), o = 0, i = r.length;
                o < i;
                o++
              )
                g(r[o], a[o]);
            else g(e, s);
          return (
            (a = v(s, "script")), a.length > 0 && m(a, !l && v(e, "script")), s
          );
        },
        buildFragment: function(e, t, n, o) {
          for (
            var i,
              r,
              a,
              s,
              l,
              u,
              c = t.createDocumentFragment(),
              p = [],
              d = 0,
              f = e.length;
            d < f;
            d++
          )
            if (((i = e[d]), i || 0 === i))
              if ("object" === J.type(i)) J.merge(p, i.nodeType ? [i] : i);
              else if (_e.test(i)) {
                for (
                  r = r || c.appendChild(t.createElement("div")),
                    a = (Ae.exec(i) || ["", ""])[1].toLowerCase(),
                    s = je[a] || je._default,
                    r.innerHTML = s[1] + i.replace(Fe, "<$1></$2>") + s[2],
                    u = s[0];
                  u--;

                )
                  r = r.lastChild;
                J.merge(p, r.childNodes),
                  (r = c.firstChild),
                  (r.textContent = "");
              } else p.push(t.createTextNode(i));
          for (c.textContent = "", d = 0; (i = p[d++]); )
            if (
              (!o || J.inArray(i, o) === -1) &&
              ((l = J.contains(i.ownerDocument, i)),
              (r = v(c.appendChild(i), "script")),
              l && m(r),
              n)
            )
              for (u = 0; (i = r[u++]); ) Me.test(i.type || "") && n.push(i);
          return c;
        },
        cleanData: function(e) {
          for (
            var t, n, o, i, r = J.event.special, a = 0;
            void 0 !== (n = e[a]);
            a++
          ) {
            if (
              J.acceptData(n) &&
              ((i = n[ve.expando]), i && (t = ve.cache[i]))
            ) {
              if (t.events)
                for (o in t.events)
                  r[o] ? J.event.remove(n, o) : J.removeEvent(n, o, t.handle);
              ve.cache[i] && delete ve.cache[i];
            }
            delete ye.cache[n[ye.expando]];
          }
        }
      }),
      J.fn.extend({
        text: function(e) {
          return ge(
            this,
            function(e) {
              return void 0 === e
                ? J.text(this)
                : this.empty().each(function() {
                    (1 !== this.nodeType &&
                      11 !== this.nodeType &&
                      9 !== this.nodeType) ||
                      (this.textContent = e);
                  });
            },
            null,
            e,
            arguments.length
          );
        },
        append: function() {
          return this.domManip(arguments, function(e) {
            if (
              1 === this.nodeType ||
              11 === this.nodeType ||
              9 === this.nodeType
            ) {
              var t = d(this, e);
              t.appendChild(e);
            }
          });
        },
        prepend: function() {
          return this.domManip(arguments, function(e) {
            if (
              1 === this.nodeType ||
              11 === this.nodeType ||
              9 === this.nodeType
            ) {
              var t = d(this, e);
              t.insertBefore(e, t.firstChild);
            }
          });
        },
        before: function() {
          return this.domManip(arguments, function(e) {
            this.parentNode && this.parentNode.insertBefore(e, this);
          });
        },
        after: function() {
          return this.domManip(arguments, function(e) {
            this.parentNode &&
              this.parentNode.insertBefore(e, this.nextSibling);
          });
        },
        remove: function(e, t) {
          for (
            var n, o = e ? J.filter(e, this) : this, i = 0;
            null != (n = o[i]);
            i++
          )
            t || 1 !== n.nodeType || J.cleanData(v(n)),
              n.parentNode &&
                (t && J.contains(n.ownerDocument, n) && m(v(n, "script")),
                n.parentNode.removeChild(n));
          return this;
        },
        empty: function() {
          for (var e, t = 0; null != (e = this[t]); t++)
            1 === e.nodeType && (J.cleanData(v(e, !1)), (e.textContent = ""));
          return this;
        },
        clone: function(e, t) {
          return (
            (e = null != e && e),
            (t = null == t ? e : t),
            this.map(function() {
              return J.clone(this, e, t);
            })
          );
        },
        html: function(e) {
          return ge(
            this,
            function(e) {
              var t = this[0] || {},
                n = 0,
                o = this.length;
              if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
              if (
                "string" == typeof e &&
                !Le.test(e) &&
                !je[(Ae.exec(e) || ["", ""])[1].toLowerCase()]
              ) {
                e = e.replace(Fe, "<$1></$2>");
                try {
                  for (; n < o; n++)
                    (t = this[n] || {}),
                      1 === t.nodeType &&
                        (J.cleanData(v(t, !1)), (t.innerHTML = e));
                  t = 0;
                } catch (i) {}
              }
              t && this.empty().append(e);
            },
            null,
            e,
            arguments.length
          );
        },
        replaceWith: function() {
          var e = arguments[0];
          return (
            this.domManip(arguments, function(t) {
              (e = this.parentNode),
                J.cleanData(v(this)),
                e && e.replaceChild(t, this);
            }),
            e && (e.length || e.nodeType) ? this : this.remove()
          );
        },
        detach: function(e) {
          return this.remove(e, !0);
        },
        domManip: function(e, t) {
          e = Z.apply([], e);
          var n,
            o,
            i,
            r,
            a,
            s,
            l = 0,
            u = this.length,
            c = this,
            p = u - 1,
            d = e[0],
            m = J.isFunction(d);
          if (
            m ||
            (u > 1 && "string" == typeof d && !Y.checkClone && Oe.test(d))
          )
            return this.each(function(n) {
              var o = c.eq(n);
              m && (e[0] = d.call(this, n, o.html())), o.domManip(e, t);
            });
          if (
            u &&
            ((n = J.buildFragment(e, this[0].ownerDocument, !1, this)),
            (o = n.firstChild),
            1 === n.childNodes.length && (n = o),
            o)
          ) {
            for (i = J.map(v(n, "script"), f), r = i.length; l < u; l++)
              (a = n),
                l !== p &&
                  ((a = J.clone(a, !0, !0)), r && J.merge(i, v(a, "script"))),
                t.call(this[l], a, l);
            if (r)
              for (
                s = i[i.length - 1].ownerDocument, J.map(i, h), l = 0;
                l < r;
                l++
              )
                (a = i[l]),
                  Me.test(a.type || "") &&
                    !ve.access(a, "globalEval") &&
                    J.contains(s, a) &&
                    (a.src
                      ? J._evalUrl && J._evalUrl(a.src)
                      : J.globalEval(a.textContent.replace(Pe, "")));
          }
          return this;
        }
      }),
      J.each(
        {
          appendTo: "append",
          prependTo: "prepend",
          insertBefore: "before",
          insertAfter: "after",
          replaceAll: "replaceWith"
        },
        function(e, t) {
          J.fn[e] = function(e) {
            for (var n, o = [], i = J(e), r = i.length - 1, a = 0; a <= r; a++)
              (n = a === r ? this : this.clone(!0)),
                J(i[a])[t](n),
                U.apply(o, n.get());
            return this.pushStack(o);
          };
        }
      );
    var He,
      qe = {},
      ze = /^margin/,
      We = new RegExp("^(" + be + ")(?!px)[a-z%]+$", "i"),
      Be = function(t) {
        return t.ownerDocument.defaultView.opener
          ? t.ownerDocument.defaultView.getComputedStyle(t, null)
          : e.getComputedStyle(t, null);
      };
    !(function() {
      function t() {
        (a.style.cssText =
          "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute"),
          (a.innerHTML = ""),
          i.appendChild(r);
        var t = e.getComputedStyle(a, null);
        (n = "1%" !== t.top), (o = "4px" === t.width), i.removeChild(r);
      }
      var n,
        o,
        i = V.documentElement,
        r = V.createElement("div"),
        a = V.createElement("div");
      a.style &&
        ((a.style.backgroundClip = "content-box"),
        (a.cloneNode(!0).style.backgroundClip = ""),
        (Y.clearCloneStyle = "content-box" === a.style.backgroundClip),
        (r.style.cssText =
          "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute"),
        r.appendChild(a),
        e.getComputedStyle &&
          J.extend(Y, {
            pixelPosition: function() {
              return t(), n;
            },
            boxSizingReliable: function() {
              return null == o && t(), o;
            },
            reliableMarginRight: function() {
              var t,
                n = a.appendChild(V.createElement("div"));
              return (
                (n.style.cssText = a.style.cssText =
                  "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0"),
                (n.style.marginRight = n.style.width = "0"),
                (a.style.width = "1px"),
                i.appendChild(r),
                (t = !parseFloat(e.getComputedStyle(n, null).marginRight)),
                i.removeChild(r),
                a.removeChild(n),
                t
              );
            }
          }));
    })(),
      (J.swap = function(e, t, n, o) {
        var i,
          r,
          a = {};
        for (r in t) (a[r] = e.style[r]), (e.style[r] = t[r]);
        i = n.apply(e, o || []);
        for (r in t) e.style[r] = a[r];
        return i;
      });
    var Ze = /^(none|table(?!-c[ea]).+)/,
      Ue = new RegExp("^(" + be + ")(.*)$", "i"),
      $e = new RegExp("^([+-])=(" + be + ")", "i"),
      Ke = { position: "absolute", visibility: "hidden", display: "block" },
      Xe = { letterSpacing: "0", fontWeight: "400" },
      Ge = ["Webkit", "O", "Moz", "ms"];
    J.extend({
      cssHooks: {
        opacity: {
          get: function(e, t) {
            if (t) {
              var n = b(e, "opacity");
              return "" === n ? "1" : n;
            }
          }
        }
      },
      cssNumber: {
        columnCount: !0,
        fillOpacity: !0,
        flexGrow: !0,
        flexShrink: !0,
        fontWeight: !0,
        lineHeight: !0,
        opacity: !0,
        order: !0,
        orphans: !0,
        widows: !0,
        zIndex: !0,
        zoom: !0
      },
      cssProps: { float: "cssFloat" },
      style: function(e, t, n, o) {
        if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
          var i,
            r,
            a,
            s = J.camelCase(t),
            l = e.style;
          return (
            (t = J.cssProps[s] || (J.cssProps[s] = C(l, s))),
            (a = J.cssHooks[t] || J.cssHooks[s]),
            void 0 === n
              ? a && "get" in a && void 0 !== (i = a.get(e, !1, o))
                ? i
                : l[t]
              : ((r = typeof n),
                "string" === r &&
                  (i = $e.exec(n)) &&
                  ((n = (i[1] + 1) * i[2] + parseFloat(J.css(e, t))),
                  (r = "number")),
                null != n &&
                  n === n &&
                  ("number" !== r || J.cssNumber[s] || (n += "px"),
                  Y.clearCloneStyle ||
                    "" !== n ||
                    0 !== t.indexOf("background") ||
                    (l[t] = "inherit"),
                  (a && "set" in a && void 0 === (n = a.set(e, n, o))) ||
                    (l[t] = n)),
                void 0)
          );
        }
      },
      css: function(e, t, n, o) {
        var i,
          r,
          a,
          s = J.camelCase(t);
        return (
          (t = J.cssProps[s] || (J.cssProps[s] = C(e.style, s))),
          (a = J.cssHooks[t] || J.cssHooks[s]),
          a && "get" in a && (i = a.get(e, !0, n)),
          void 0 === i && (i = b(e, t, o)),
          "normal" === i && t in Xe && (i = Xe[t]),
          "" === n || n
            ? ((r = parseFloat(i)), n === !0 || J.isNumeric(r) ? r || 0 : i)
            : i
        );
      }
    }),
      J.each(["height", "width"], function(e, t) {
        J.cssHooks[t] = {
          get: function(e, n, o) {
            if (n)
              return Ze.test(J.css(e, "display")) && 0 === e.offsetWidth
                ? J.swap(e, Ke, function() {
                    return S(e, t, o);
                  })
                : S(e, t, o);
          },
          set: function(e, n, o) {
            var i = o && Be(e);
            return E(
              e,
              n,
              o
                ? k(e, t, o, "border-box" === J.css(e, "boxSizing", !1, i), i)
                : 0
            );
          }
        };
      }),
      (J.cssHooks.marginRight = T(Y.reliableMarginRight, function(e, t) {
        if (t)
          return J.swap(e, { display: "inline-block" }, b, [e, "marginRight"]);
      })),
      J.each({ margin: "", padding: "", border: "Width" }, function(e, t) {
        (J.cssHooks[e + t] = {
          expand: function(n) {
            for (
              var o = 0, i = {}, r = "string" == typeof n ? n.split(" ") : [n];
              o < 4;
              o++
            )
              i[e + Te[o] + t] = r[o] || r[o - 2] || r[0];
            return i;
          }
        }),
          ze.test(e) || (J.cssHooks[e + t].set = E);
      }),
      J.fn.extend({
        css: function(e, t) {
          return ge(
            this,
            function(e, t, n) {
              var o,
                i,
                r = {},
                a = 0;
              if (J.isArray(t)) {
                for (o = Be(e), i = t.length; a < i; a++)
                  r[t[a]] = J.css(e, t[a], !1, o);
                return r;
              }
              return void 0 !== n ? J.style(e, t, n) : J.css(e, t);
            },
            e,
            t,
            arguments.length > 1
          );
        },
        show: function() {
          return D(this, !0);
        },
        hide: function() {
          return D(this);
        },
        toggle: function(e) {
          return "boolean" == typeof e
            ? e
              ? this.show()
              : this.hide()
            : this.each(function() {
                Ce(this) ? J(this).show() : J(this).hide();
              });
        }
      }),
      (J.Tween = I),
      (I.prototype = {
        constructor: I,
        init: function(e, t, n, o, i, r) {
          (this.elem = e),
            (this.prop = n),
            (this.easing = i || "swing"),
            (this.options = t),
            (this.start = this.now = this.cur()),
            (this.end = o),
            (this.unit = r || (J.cssNumber[n] ? "" : "px"));
        },
        cur: function() {
          var e = I.propHooks[this.prop];
          return e && e.get ? e.get(this) : I.propHooks._default.get(this);
        },
        run: function(e) {
          var t,
            n = I.propHooks[this.prop];
          return (
            this.options.duration
              ? (this.pos = t = J.easing[this.easing](
                  e,
                  this.options.duration * e,
                  0,
                  1,
                  this.options.duration
                ))
              : (this.pos = t = e),
            (this.now = (this.end - this.start) * t + this.start),
            this.options.step &&
              this.options.step.call(this.elem, this.now, this),
            n && n.set ? n.set(this) : I.propHooks._default.set(this),
            this
          );
        }
      }),
      (I.prototype.init.prototype = I.prototype),
      (I.propHooks = {
        _default: {
          get: function(e) {
            var t;
            return null == e.elem[e.prop] ||
              (e.elem.style && null != e.elem.style[e.prop])
              ? ((t = J.css(e.elem, e.prop, "")), t && "auto" !== t ? t : 0)
              : e.elem[e.prop];
          },
          set: function(e) {
            J.fx.step[e.prop]
              ? J.fx.step[e.prop](e)
              : e.elem.style &&
                (null != e.elem.style[J.cssProps[e.prop]] || J.cssHooks[e.prop])
              ? J.style(e.elem, e.prop, e.now + e.unit)
              : (e.elem[e.prop] = e.now);
          }
        }
      }),
      (I.propHooks.scrollTop = I.propHooks.scrollLeft = {
        set: function(e) {
          e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
        }
      }),
      (J.easing = {
        linear: function(e) {
          return e;
        },
        swing: function(e) {
          return 0.5 - Math.cos(e * Math.PI) / 2;
        }
      }),
      (J.fx = I.prototype.init),
      (J.fx.step = {});
    var Ye,
      Ve,
      Qe = /^(?:toggle|show|hide)$/,
      Je = new RegExp("^(?:([+-])=|)(" + be + ")([a-z%]*)$", "i"),
      et = /queueHooks$/,
      tt = [_],
      nt = {
        "*": [
          function(e, t) {
            var n = this.createTween(e, t),
              o = n.cur(),
              i = Je.exec(t),
              r = (i && i[3]) || (J.cssNumber[e] ? "" : "px"),
              a =
                (J.cssNumber[e] || ("px" !== r && +o)) &&
                Je.exec(J.css(n.elem, e)),
              s = 1,
              l = 20;
            if (a && a[3] !== r) {
              (r = r || a[3]), (i = i || []), (a = +o || 1);
              do (s = s || ".5"), (a /= s), J.style(n.elem, e, a + r);
              while (s !== (s = n.cur() / o) && 1 !== s && --l);
            }
            return (
              i &&
                ((a = n.start = +a || +o || 0),
                (n.unit = r),
                (n.end = i[1] ? a + (i[1] + 1) * i[2] : +i[2])),
              n
            );
          }
        ]
      };
    (J.Animation = J.extend(O, {
      tweener: function(e, t) {
        J.isFunction(e) ? ((t = e), (e = ["*"])) : (e = e.split(" "));
        for (var n, o = 0, i = e.length; o < i; o++)
          (n = e[o]), (nt[n] = nt[n] || []), nt[n].unshift(t);
      },
      prefilter: function(e, t) {
        t ? tt.unshift(e) : tt.push(e);
      }
    })),
      (J.speed = function(e, t, n) {
        var o =
          e && "object" == typeof e
            ? J.extend({}, e)
            : {
                complete: n || (!n && t) || (J.isFunction(e) && e),
                duration: e,
                easing: (n && t) || (t && !J.isFunction(t) && t)
              };
        return (
          (o.duration = J.fx.off
            ? 0
            : "number" == typeof o.duration
            ? o.duration
            : o.duration in J.fx.speeds
            ? J.fx.speeds[o.duration]
            : J.fx.speeds._default),
          (null != o.queue && o.queue !== !0) || (o.queue = "fx"),
          (o.old = o.complete),
          (o.complete = function() {
            J.isFunction(o.old) && o.old.call(this),
              o.queue && J.dequeue(this, o.queue);
          }),
          o
        );
      }),
      J.fn.extend({
        fadeTo: function(e, t, n, o) {
          return this.filter(Ce)
            .css("opacity", 0)
            .show()
            .end()
            .animate({ opacity: t }, e, n, o);
        },
        animate: function(e, t, n, o) {
          var i = J.isEmptyObject(e),
            r = J.speed(t, n, o),
            a = function() {
              var t = O(this, J.extend({}, e), r);
              (i || ve.get(this, "finish")) && t.stop(!0);
            };
          return (
            (a.finish = a),
            i || r.queue === !1 ? this.each(a) : this.queue(r.queue, a)
          );
        },
        stop: function(e, t, n) {
          var o = function(e) {
            var t = e.stop;
            delete e.stop, t(n);
          };
          return (
            "string" != typeof e && ((n = t), (t = e), (e = void 0)),
            t && e !== !1 && this.queue(e || "fx", []),
            this.each(function() {
              var t = !0,
                i = null != e && e + "queueHooks",
                r = J.timers,
                a = ve.get(this);
              if (i) a[i] && a[i].stop && o(a[i]);
              else for (i in a) a[i] && a[i].stop && et.test(i) && o(a[i]);
              for (i = r.length; i--; )
                r[i].elem !== this ||
                  (null != e && r[i].queue !== e) ||
                  (r[i].anim.stop(n), (t = !1), r.splice(i, 1));
              (!t && n) || J.dequeue(this, e);
            })
          );
        },
        finish: function(e) {
          return (
            e !== !1 && (e = e || "fx"),
            this.each(function() {
              var t,
                n = ve.get(this),
                o = n[e + "queue"],
                i = n[e + "queueHooks"],
                r = J.timers,
                a = o ? o.length : 0;
              for (
                n.finish = !0,
                  J.queue(this, e, []),
                  i && i.stop && i.stop.call(this, !0),
                  t = r.length;
                t--;

              )
                r[t].elem === this &&
                  r[t].queue === e &&
                  (r[t].anim.stop(!0), r.splice(t, 1));
              for (t = 0; t < a; t++)
                o[t] && o[t].finish && o[t].finish.call(this);
              delete n.finish;
            })
          );
        }
      }),
      J.each(["toggle", "show", "hide"], function(e, t) {
        var n = J.fn[t];
        J.fn[t] = function(e, o, i) {
          return null == e || "boolean" == typeof e
            ? n.apply(this, arguments)
            : this.animate(F(t, !0), e, o, i);
        };
      }),
      J.each(
        {
          slideDown: F("show"),
          slideUp: F("hide"),
          slideToggle: F("toggle"),
          fadeIn: { opacity: "show" },
          fadeOut: { opacity: "hide" },
          fadeToggle: { opacity: "toggle" }
        },
        function(e, t) {
          J.fn[e] = function(e, n, o) {
            return this.animate(t, e, n, o);
          };
        }
      ),
      (J.timers = []),
      (J.fx.tick = function() {
        var e,
          t = 0,
          n = J.timers;
        for (Ye = J.now(); t < n.length; t++)
          (e = n[t]), e() || n[t] !== e || n.splice(t--, 1);
        n.length || J.fx.stop(), (Ye = void 0);
      }),
      (J.fx.timer = function(e) {
        J.timers.push(e), e() ? J.fx.start() : J.timers.pop();
      }),
      (J.fx.interval = 13),
      (J.fx.start = function() {
        Ve || (Ve = setInterval(J.fx.tick, J.fx.interval));
      }),
      (J.fx.stop = function() {
        clearInterval(Ve), (Ve = null);
      }),
      (J.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
      (J.fn.delay = function(e, t) {
        return (
          (e = J.fx ? J.fx.speeds[e] || e : e),
          (t = t || "fx"),
          this.queue(t, function(t, n) {
            var o = setTimeout(t, e);
            n.stop = function() {
              clearTimeout(o);
            };
          })
        );
      }),
      (function() {
        var e = V.createElement("input"),
          t = V.createElement("select"),
          n = t.appendChild(V.createElement("option"));
        (e.type = "checkbox"),
          (Y.checkOn = "" !== e.value),
          (Y.optSelected = n.selected),
          (t.disabled = !0),
          (Y.optDisabled = !n.disabled),
          (e = V.createElement("input")),
          (e.value = "t"),
          (e.type = "radio"),
          (Y.radioValue = "t" === e.value);
      })();
    var ot,
      it,
      rt = J.expr.attrHandle;
    J.fn.extend({
      attr: function(e, t) {
        return ge(this, J.attr, e, t, arguments.length > 1);
      },
      removeAttr: function(e) {
        return this.each(function() {
          J.removeAttr(this, e);
        });
      }
    }),
      J.extend({
        attr: function(e, t, n) {
          var o,
            i,
            r = e.nodeType;
          if (e && 3 !== r && 8 !== r && 2 !== r)
            return typeof e.getAttribute === ke
              ? J.prop(e, t, n)
              : ((1 === r && J.isXMLDoc(e)) ||
                  ((t = t.toLowerCase()),
                  (o =
                    J.attrHooks[t] || (J.expr.match.bool.test(t) ? it : ot))),
                void 0 === n
                  ? o && "get" in o && null !== (i = o.get(e, t))
                    ? i
                    : ((i = J.find.attr(e, t)), null == i ? void 0 : i)
                  : null !== n
                  ? o && "set" in o && void 0 !== (i = o.set(e, n, t))
                    ? i
                    : (e.setAttribute(t, n + ""), n)
                  : void J.removeAttr(e, t));
        },
        removeAttr: function(e, t) {
          var n,
            o,
            i = 0,
            r = t && t.match(fe);
          if (r && 1 === e.nodeType)
            for (; (n = r[i++]); )
              (o = J.propFix[n] || n),
                J.expr.match.bool.test(n) && (e[o] = !1),
                e.removeAttribute(n);
        },
        attrHooks: {
          type: {
            set: function(e, t) {
              if (!Y.radioValue && "radio" === t && J.nodeName(e, "input")) {
                var n = e.value;
                return e.setAttribute("type", t), n && (e.value = n), t;
              }
            }
          }
        }
      }),
      (it = {
        set: function(e, t, n) {
          return t === !1 ? J.removeAttr(e, n) : e.setAttribute(n, n), n;
        }
      }),
      J.each(J.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var n = rt[t] || J.find.attr;
        rt[t] = function(e, t, o) {
          var i, r;
          return (
            o ||
              ((r = rt[t]),
              (rt[t] = i),
              (i = null != n(e, t, o) ? t.toLowerCase() : null),
              (rt[t] = r)),
            i
          );
        };
      });
    var at = /^(?:input|select|textarea|button)$/i;
    J.fn.extend({
      prop: function(e, t) {
        return ge(this, J.prop, e, t, arguments.length > 1);
      },
      removeProp: function(e) {
        return this.each(function() {
          delete this[J.propFix[e] || e];
        });
      }
    }),
      J.extend({
        propFix: { for: "htmlFor", class: "className" },
        prop: function(e, t, n) {
          var o,
            i,
            r,
            a = e.nodeType;
          if (e && 3 !== a && 8 !== a && 2 !== a)
            return (
              (r = 1 !== a || !J.isXMLDoc(e)),
              r && ((t = J.propFix[t] || t), (i = J.propHooks[t])),
              void 0 !== n
                ? i && "set" in i && void 0 !== (o = i.set(e, n, t))
                  ? o
                  : (e[t] = n)
                : i && "get" in i && null !== (o = i.get(e, t))
                ? o
                : e[t]
            );
        },
        propHooks: {
          tabIndex: {
            get: function(e) {
              return e.hasAttribute("tabindex") || at.test(e.nodeName) || e.href
                ? e.tabIndex
                : -1;
            }
          }
        }
      }),
      Y.optSelected ||
        (J.propHooks.selected = {
          get: function(e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null;
          }
        }),
      J.each(
        [
          "tabIndex",
          "readOnly",
          "maxLength",
          "cellSpacing",
          "cellPadding",
          "rowSpan",
          "colSpan",
          "useMap",
          "frameBorder",
          "contentEditable"
        ],
        function() {
          J.propFix[this.toLowerCase()] = this;
        }
      );
    var st = /[\t\r\n\f]/g;
    J.fn.extend({
      addClass: function(e) {
        var t,
          n,
          o,
          i,
          r,
          a,
          s = "string" == typeof e && e,
          l = 0,
          u = this.length;
        if (J.isFunction(e))
          return this.each(function(t) {
            J(this).addClass(e.call(this, t, this.className));
          });
        if (s)
          for (t = (e || "").match(fe) || []; l < u; l++)
            if (
              ((n = this[l]),
              (o =
                1 === n.nodeType &&
                (n.className
                  ? (" " + n.className + " ").replace(st, " ")
                  : " ")))
            ) {
              for (r = 0; (i = t[r++]); )
                o.indexOf(" " + i + " ") < 0 && (o += i + " ");
              (a = J.trim(o)), n.className !== a && (n.className = a);
            }
        return this;
      },
      removeClass: function(e) {
        var t,
          n,
          o,
          i,
          r,
          a,
          s = 0 === arguments.length || ("string" == typeof e && e),
          l = 0,
          u = this.length;
        if (J.isFunction(e))
          return this.each(function(t) {
            J(this).removeClass(e.call(this, t, this.className));
          });
        if (s)
          for (t = (e || "").match(fe) || []; l < u; l++)
            if (
              ((n = this[l]),
              (o =
                1 === n.nodeType &&
                (n.className
                  ? (" " + n.className + " ").replace(st, " ")
                  : "")))
            ) {
              for (r = 0; (i = t[r++]); )
                for (; o.indexOf(" " + i + " ") >= 0; )
                  o = o.replace(" " + i + " ", " ");
              (a = e ? J.trim(o) : ""), n.className !== a && (n.className = a);
            }
        return this;
      },
      toggleClass: function(e, t) {
        var n = typeof e;
        return "boolean" == typeof t && "string" === n
          ? t
            ? this.addClass(e)
            : this.removeClass(e)
          : J.isFunction(e)
          ? this.each(function(n) {
              J(this).toggleClass(e.call(this, n, this.className, t), t);
            })
          : this.each(function() {
              if ("string" === n)
                for (
                  var t, o = 0, i = J(this), r = e.match(fe) || [];
                  (t = r[o++]);

                )
                  i.hasClass(t) ? i.removeClass(t) : i.addClass(t);
              else
                (n !== ke && "boolean" !== n) ||
                  (this.className &&
                    ve.set(this, "__className__", this.className),
                  (this.className =
                    this.className || e === !1
                      ? ""
                      : ve.get(this, "__className__") || ""));
            });
      },
      hasClass: function(e) {
        for (var t = " " + e + " ", n = 0, o = this.length; n < o; n++)
          if (
            1 === this[n].nodeType &&
            (" " + this[n].className + " ").replace(st, " ").indexOf(t) >= 0
          )
            return !0;
        return !1;
      }
    });
    var lt = /\r/g;
    J.fn.extend({
      val: function(e) {
        var t,
          n,
          o,
          i = this[0];
        {
          if (arguments.length)
            return (
              (o = J.isFunction(e)),
              this.each(function(n) {
                var i;
                1 === this.nodeType &&
                  ((i = o ? e.call(this, n, J(this).val()) : e),
                  null == i
                    ? (i = "")
                    : "number" == typeof i
                    ? (i += "")
                    : J.isArray(i) &&
                      (i = J.map(i, function(e) {
                        return null == e ? "" : e + "";
                      })),
                  (t =
                    J.valHooks[this.type] ||
                    J.valHooks[this.nodeName.toLowerCase()]),
                  (t && "set" in t && void 0 !== t.set(this, i, "value")) ||
                    (this.value = i));
              })
            );
          if (i)
            return (
              (t = J.valHooks[i.type] || J.valHooks[i.nodeName.toLowerCase()]),
              t && "get" in t && void 0 !== (n = t.get(i, "value"))
                ? n
                : ((n = i.value),
                  "string" == typeof n ? n.replace(lt, "") : null == n ? "" : n)
            );
        }
      }
    }),
      J.extend({
        valHooks: {
          option: {
            get: function(e) {
              var t = J.find.attr(e, "value");
              return null != t ? t : J.trim(J.text(e));
            }
          },
          select: {
            get: function(e) {
              for (
                var t,
                  n,
                  o = e.options,
                  i = e.selectedIndex,
                  r = "select-one" === e.type || i < 0,
                  a = r ? null : [],
                  s = r ? i + 1 : o.length,
                  l = i < 0 ? s : r ? i : 0;
                l < s;
                l++
              )
                if (
                  ((n = o[l]),
                  (n.selected || l === i) &&
                    (Y.optDisabled
                      ? !n.disabled
                      : null === n.getAttribute("disabled")) &&
                    (!n.parentNode.disabled ||
                      !J.nodeName(n.parentNode, "optgroup")))
                ) {
                  if (((t = J(n).val()), r)) return t;
                  a.push(t);
                }
              return a;
            },
            set: function(e, t) {
              for (
                var n, o, i = e.options, r = J.makeArray(t), a = i.length;
                a--;

              )
                (o = i[a]),
                  (o.selected = J.inArray(o.value, r) >= 0) && (n = !0);
              return n || (e.selectedIndex = -1), r;
            }
          }
        }
      }),
      J.each(["radio", "checkbox"], function() {
        (J.valHooks[this] = {
          set: function(e, t) {
            if (J.isArray(t))
              return (e.checked = J.inArray(J(e).val(), t) >= 0);
          }
        }),
          Y.checkOn ||
            (J.valHooks[this].get = function(e) {
              return null === e.getAttribute("value") ? "on" : e.value;
            });
      }),
      J.each(
        "blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(
          " "
        ),
        function(e, t) {
          J.fn[t] = function(e, n) {
            return arguments.length > 0
              ? this.on(t, null, e, n)
              : this.trigger(t);
          };
        }
      ),
      J.fn.extend({
        hover: function(e, t) {
          return this.mouseenter(e).mouseleave(t || e);
        },
        bind: function(e, t, n) {
          return this.on(e, null, t, n);
        },
        unbind: function(e, t) {
          return this.off(e, null, t);
        },
        delegate: function(e, t, n, o) {
          return this.on(t, e, n, o);
        },
        undelegate: function(e, t, n) {
          return 1 === arguments.length
            ? this.off(e, "**")
            : this.off(t, e || "**", n);
        }
      });
    var ut = J.now(),
      ct = /\?/;
    (J.parseJSON = function(e) {
      return JSON.parse(e + "");
    }),
      (J.parseXML = function(e) {
        var t, n;
        if (!e || "string" != typeof e) return null;
        try {
          (n = new DOMParser()), (t = n.parseFromString(e, "text/xml"));
        } catch (o) {
          t = void 0;
        }
        return (
          (t && !t.getElementsByTagName("parsererror").length) ||
            J.error("Invalid XML: " + e),
          t
        );
      });
    var pt = /#.*$/,
      dt = /([?&])_=[^&]*/,
      ft = /^(.*?):[ \t]*([^\r\n]*)$/gm,
      ht = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
      mt = /^(?:GET|HEAD)$/,
      gt = /^\/\//,
      vt = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
      yt = {},
      xt = {},
      wt = "*/".concat("*"),
      bt = e.location.href,
      Tt = vt.exec(bt.toLowerCase()) || [];
    J.extend({
      active: 0,
      lastModified: {},
      etag: {},
      ajaxSettings: {
        url: bt,
        type: "GET",
        isLocal: ht.test(Tt[1]),
        global: !0,
        processData: !0,
        async: !0,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        accepts: {
          "*": wt,
          text: "text/plain",
          html: "text/html",
          xml: "application/xml, text/xml",
          json: "application/json, text/javascript"
        },
        contents: { xml: /xml/, html: /html/, json: /json/ },
        responseFields: {
          xml: "responseXML",
          text: "responseText",
          json: "responseJSON"
        },
        converters: {
          "* text": String,
          "text html": !0,
          "text json": J.parseJSON,
          "text xml": J.parseXML
        },
        flatOptions: { url: !0, context: !0 }
      },
      ajaxSetup: function(e, t) {
        return t ? P(P(e, J.ajaxSettings), t) : P(J.ajaxSettings, e);
      },
      ajaxPrefilter: M(yt),
      ajaxTransport: M(xt),
      ajax: function(e, t) {
        function n(e, t, n, a) {
          var l,
            c,
            v,
            y,
            w,
            T = t;
          2 !== x &&
            ((x = 2),
            s && clearTimeout(s),
            (o = void 0),
            (r = a || ""),
            (b.readyState = e > 0 ? 4 : 0),
            (l = (e >= 200 && e < 300) || 304 === e),
            n && (y = j(p, b, n)),
            (y = H(p, y, b, l)),
            l
              ? (p.ifModified &&
                  ((w = b.getResponseHeader("Last-Modified")),
                  w && (J.lastModified[i] = w),
                  (w = b.getResponseHeader("etag")),
                  w && (J.etag[i] = w)),
                204 === e || "HEAD" === p.type
                  ? (T = "nocontent")
                  : 304 === e
                  ? (T = "notmodified")
                  : ((T = y.state), (c = y.data), (v = y.error), (l = !v)))
              : ((v = T), (!e && T) || ((T = "error"), e < 0 && (e = 0))),
            (b.status = e),
            (b.statusText = (t || T) + ""),
            l ? h.resolveWith(d, [c, T, b]) : h.rejectWith(d, [b, T, v]),
            b.statusCode(g),
            (g = void 0),
            u && f.trigger(l ? "ajaxSuccess" : "ajaxError", [b, p, l ? c : v]),
            m.fireWith(d, [b, T]),
            u &&
              (f.trigger("ajaxComplete", [b, p]),
              --J.active || J.event.trigger("ajaxStop")));
        }
        "object" == typeof e && ((t = e), (e = void 0)), (t = t || {});
        var o,
          i,
          r,
          a,
          s,
          l,
          u,
          c,
          p = J.ajaxSetup({}, t),
          d = p.context || p,
          f = p.context && (d.nodeType || d.jquery) ? J(d) : J.event,
          h = J.Deferred(),
          m = J.Callbacks("once memory"),
          g = p.statusCode || {},
          v = {},
          y = {},
          x = 0,
          w = "canceled",
          b = {
            readyState: 0,
            getResponseHeader: function(e) {
              var t;
              if (2 === x) {
                if (!a)
                  for (a = {}; (t = ft.exec(r)); ) a[t[1].toLowerCase()] = t[2];
                t = a[e.toLowerCase()];
              }
              return null == t ? null : t;
            },
            getAllResponseHeaders: function() {
              return 2 === x ? r : null;
            },
            setRequestHeader: function(e, t) {
              var n = e.toLowerCase();
              return x || ((e = y[n] = y[n] || e), (v[e] = t)), this;
            },
            overrideMimeType: function(e) {
              return x || (p.mimeType = e), this;
            },
            statusCode: function(e) {
              var t;
              if (e)
                if (x < 2) for (t in e) g[t] = [g[t], e[t]];
                else b.always(e[b.status]);
              return this;
            },
            abort: function(e) {
              var t = e || w;
              return o && o.abort(t), n(0, t), this;
            }
          };
        if (
          ((h.promise(b).complete = m.add),
          (b.success = b.done),
          (b.error = b.fail),
          (p.url = ((e || p.url || bt) + "")
            .replace(pt, "")
            .replace(gt, Tt[1] + "//")),
          (p.type = t.method || t.type || p.method || p.type),
          (p.dataTypes = J.trim(p.dataType || "*")
            .toLowerCase()
            .match(fe) || [""]),
          null == p.crossDomain &&
            ((l = vt.exec(p.url.toLowerCase())),
            (p.crossDomain = !(
              !l ||
              (l[1] === Tt[1] &&
                l[2] === Tt[2] &&
                (l[3] || ("http:" === l[1] ? "80" : "443")) ===
                  (Tt[3] || ("http:" === Tt[1] ? "80" : "443")))
            ))),
          p.data &&
            p.processData &&
            "string" != typeof p.data &&
            (p.data = J.param(p.data, p.traditional)),
          R(yt, p, t, b),
          2 === x)
        )
          return b;
        (u = J.event && p.global),
          u && 0 === J.active++ && J.event.trigger("ajaxStart"),
          (p.type = p.type.toUpperCase()),
          (p.hasContent = !mt.test(p.type)),
          (i = p.url),
          p.hasContent ||
            (p.data &&
              ((i = p.url += (ct.test(i) ? "&" : "?") + p.data), delete p.data),
            p.cache === !1 &&
              (p.url = dt.test(i)
                ? i.replace(dt, "$1_=" + ut++)
                : i + (ct.test(i) ? "&" : "?") + "_=" + ut++)),
          p.ifModified &&
            (J.lastModified[i] &&
              b.setRequestHeader("If-Modified-Since", J.lastModified[i]),
            J.etag[i] && b.setRequestHeader("If-None-Match", J.etag[i])),
          ((p.data && p.hasContent && p.contentType !== !1) || t.contentType) &&
            b.setRequestHeader("Content-Type", p.contentType),
          b.setRequestHeader(
            "Accept",
            p.dataTypes[0] && p.accepts[p.dataTypes[0]]
              ? p.accepts[p.dataTypes[0]] +
                  ("*" !== p.dataTypes[0] ? ", " + wt + "; q=0.01" : "")
              : p.accepts["*"]
          );
        for (c in p.headers) b.setRequestHeader(c, p.headers[c]);
        if (p.beforeSend && (p.beforeSend.call(d, b, p) === !1 || 2 === x))
          return b.abort();
        w = "abort";
        for (c in { success: 1, error: 1, complete: 1 }) b[c](p[c]);
        if ((o = R(xt, p, t, b))) {
          (b.readyState = 1),
            u && f.trigger("ajaxSend", [b, p]),
            p.async &&
              p.timeout > 0 &&
              (s = setTimeout(function() {
                b.abort("timeout");
              }, p.timeout));
          try {
            (x = 1), o.send(v, n);
          } catch (T) {
            if (!(x < 2)) throw T;
            n(-1, T);
          }
        } else n(-1, "No Transport");
        return b;
      },
      getJSON: function(e, t, n) {
        return J.get(e, t, n, "json");
      },
      getScript: function(e, t) {
        return J.get(e, void 0, t, "script");
      }
    }),
      J.each(["get", "post"], function(e, t) {
        J[t] = function(e, n, o, i) {
          return (
            J.isFunction(n) && ((i = i || o), (o = n), (n = void 0)),
            J.ajax({ url: e, type: t, dataType: i, data: n, success: o })
          );
        };
      }),
      (J._evalUrl = function(e) {
        return J.ajax({
          url: e,
          type: "GET",
          dataType: "script",
          async: !1,
          global: !1,
          throws: !0
        });
      }),
      J.fn.extend({
        wrapAll: function(e) {
          var t;
          return J.isFunction(e)
            ? this.each(function(t) {
                J(this).wrapAll(e.call(this, t));
              })
            : (this[0] &&
                ((t = J(e, this[0].ownerDocument)
                  .eq(0)
                  .clone(!0)),
                this[0].parentNode && t.insertBefore(this[0]),
                t
                  .map(function() {
                    for (var e = this; e.firstElementChild; )
                      e = e.firstElementChild;
                    return e;
                  })
                  .append(this)),
              this);
        },
        wrapInner: function(e) {
          return J.isFunction(e)
            ? this.each(function(t) {
                J(this).wrapInner(e.call(this, t));
              })
            : this.each(function() {
                var t = J(this),
                  n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e);
              });
        },
        wrap: function(e) {
          var t = J.isFunction(e);
          return this.each(function(n) {
            J(this).wrapAll(t ? e.call(this, n) : e);
          });
        },
        unwrap: function() {
          return this.parent()
            .each(function() {
              J.nodeName(this, "body") || J(this).replaceWith(this.childNodes);
            })
            .end();
        }
      }),
      (J.expr.filters.hidden = function(e) {
        return e.offsetWidth <= 0 && e.offsetHeight <= 0;
      }),
      (J.expr.filters.visible = function(e) {
        return !J.expr.filters.hidden(e);
      });
    var Ct = /%20/g,
      Et = /\[\]$/,
      kt = /\r?\n/g,
      St = /^(?:submit|button|image|reset|file)$/i,
      Dt = /^(?:input|select|textarea|keygen)/i;
    (J.param = function(e, t) {
      var n,
        o = [],
        i = function(e, t) {
          (t = J.isFunction(t) ? t() : null == t ? "" : t),
            (o[o.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t));
        };
      if (
        (void 0 === t && (t = J.ajaxSettings && J.ajaxSettings.traditional),
        J.isArray(e) || (e.jquery && !J.isPlainObject(e)))
      )
        J.each(e, function() {
          i(this.name, this.value);
        });
      else for (n in e) q(n, e[n], t, i);
      return o.join("&").replace(Ct, "+");
    }),
      J.fn.extend({
        serialize: function() {
          return J.param(this.serializeArray());
        },
        serializeArray: function() {
          return this.map(function() {
            var e = J.prop(this, "elements");
            return e ? J.makeArray(e) : this;
          })
            .filter(function() {
              var e = this.type;
              return (
                this.name &&
                !J(this).is(":disabled") &&
                Dt.test(this.nodeName) &&
                !St.test(e) &&
                (this.checked || !Ee.test(e))
              );
            })
            .map(function(e, t) {
              var n = J(this).val();
              return null == n
                ? null
                : J.isArray(n)
                ? J.map(n, function(e) {
                    return { name: t.name, value: e.replace(kt, "\r\n") };
                  })
                : { name: t.name, value: n.replace(kt, "\r\n") };
            })
            .get();
        }
      }),
      (J.ajaxSettings.xhr = function() {
        try {
          return new XMLHttpRequest();
        } catch (e) {}
      });
    var It = 0,
      Nt = {},
      Ft = { 0: 200, 1223: 204 },
      At = J.ajaxSettings.xhr();
    e.attachEvent &&
      e.attachEvent("onunload", function() {
        for (var e in Nt) Nt[e]();
      }),
      (Y.cors = !!At && "withCredentials" in At),
      (Y.ajax = At = !!At),
      J.ajaxTransport(function(e) {
        var t;
        if (Y.cors || (At && !e.crossDomain))
          return {
            send: function(n, o) {
              var i,
                r = e.xhr(),
                a = ++It;
              if (
                (r.open(e.type, e.url, e.async, e.username, e.password),
                e.xhrFields)
              )
                for (i in e.xhrFields) r[i] = e.xhrFields[i];
              e.mimeType &&
                r.overrideMimeType &&
                r.overrideMimeType(e.mimeType),
                e.crossDomain ||
                  n["X-Requested-With"] ||
                  (n["X-Requested-With"] = "XMLHttpRequest");
              for (i in n) r.setRequestHeader(i, n[i]);
              (t = function(e) {
                return function() {
                  t &&
                    (delete Nt[a],
                    (t = r.onload = r.onerror = null),
                    "abort" === e
                      ? r.abort()
                      : "error" === e
                      ? o(r.status, r.statusText)
                      : o(
                          Ft[r.status] || r.status,
                          r.statusText,
                          "string" == typeof r.responseText
                            ? { text: r.responseText }
                            : void 0,
                          r.getAllResponseHeaders()
                        ));
                };
              }),
                (r.onload = t()),
                (r.onerror = t("error")),
                (t = Nt[a] = t("abort"));
              try {
                r.send((e.hasContent && e.data) || null);
              } catch (s) {
                if (t) throw s;
              }
            },
            abort: function() {
              t && t();
            }
          };
      }),
      J.ajaxSetup({
        accepts: {
          script:
            "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: { script: /(?:java|ecma)script/ },
        converters: {
          "text script": function(e) {
            return J.globalEval(e), e;
          }
        }
      }),
      J.ajaxPrefilter("script", function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET");
      }),
      J.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
          var t, n;
          return {
            send: function(o, i) {
              (t = J("<script>")
                .prop({ async: !0, charset: e.scriptCharset, src: e.url })
                .on(
                  "load error",
                  (n = function(e) {
                    t.remove(),
                      (n = null),
                      e && i("error" === e.type ? 404 : 200, e.type);
                  })
                )),
                V.head.appendChild(t[0]);
            },
            abort: function() {
              n && n();
            }
          };
        }
      });
    var _t = [],
      Lt = /(=)\?(?=&|$)|\?\?/;
    J.ajaxSetup({
      jsonp: "callback",
      jsonpCallback: function() {
        var e = _t.pop() || J.expando + "_" + ut++;
        return (this[e] = !0), e;
      }
    }),
      J.ajaxPrefilter("json jsonp", function(t, n, o) {
        var i,
          r,
          a,
          s =
            t.jsonp !== !1 &&
            (Lt.test(t.url)
              ? "url"
              : "string" == typeof t.data &&
                !(t.contentType || "").indexOf(
                  "application/x-www-form-urlencoded"
                ) &&
                Lt.test(t.data) &&
                "data");
        if (s || "jsonp" === t.dataTypes[0])
          return (
            (i = t.jsonpCallback = J.isFunction(t.jsonpCallback)
              ? t.jsonpCallback()
              : t.jsonpCallback),
            s
              ? (t[s] = t[s].replace(Lt, "$1" + i))
              : t.jsonp !== !1 &&
                (t.url += (ct.test(t.url) ? "&" : "?") + t.jsonp + "=" + i),
            (t.converters["script json"] = function() {
              return a || J.error(i + " was not called"), a[0];
            }),
            (t.dataTypes[0] = "json"),
            (r = e[i]),
            (e[i] = function() {
              a = arguments;
            }),
            o.always(function() {
              (e[i] = r),
                t[i] && ((t.jsonpCallback = n.jsonpCallback), _t.push(i)),
                a && J.isFunction(r) && r(a[0]),
                (a = r = void 0);
            }),
            "script"
          );
      }),
      (J.parseHTML = function(e, t, n) {
        if (!e || "string" != typeof e) return null;
        "boolean" == typeof t && ((n = t), (t = !1)), (t = t || V);
        var o = ae.exec(e),
          i = !n && [];
        return o
          ? [t.createElement(o[1])]
          : ((o = J.buildFragment([e], t, i)),
            i && i.length && J(i).remove(),
            J.merge([], o.childNodes));
      });
    var Ot = J.fn.load;
    (J.fn.load = function(e, t, n) {
      if ("string" != typeof e && Ot) return Ot.apply(this, arguments);
      var o,
        i,
        r,
        a = this,
        s = e.indexOf(" ");
      return (
        s >= 0 && ((o = J.trim(e.slice(s))), (e = e.slice(0, s))),
        J.isFunction(t)
          ? ((n = t), (t = void 0))
          : t && "object" == typeof t && (i = "POST"),
        a.length > 0 &&
          J.ajax({ url: e, type: i, dataType: "html", data: t })
            .done(function(e) {
              (r = arguments),
                a.html(
                  o
                    ? J("<div>")
                        .append(J.parseHTML(e))
                        .find(o)
                    : e
                );
            })
            .complete(
              n &&
                function(e, t) {
                  a.each(n, r || [e.responseText, t, e]);
                }
            ),
        this
      );
    }),
      J.each(
        [
          "ajaxStart",
          "ajaxStop",
          "ajaxComplete",
          "ajaxError",
          "ajaxSuccess",
          "ajaxSend"
        ],
        function(e, t) {
          J.fn[t] = function(e) {
            return this.on(t, e);
          };
        }
      ),
      (J.expr.filters.animated = function(e) {
        return J.grep(J.timers, function(t) {
          return e === t.elem;
        }).length;
      });
    var Mt = e.document.documentElement;
    (J.offset = {
      setOffset: function(e, t, n) {
        var o,
          i,
          r,
          a,
          s,
          l,
          u,
          c = J.css(e, "position"),
          p = J(e),
          d = {};
        "static" === c && (e.style.position = "relative"),
          (s = p.offset()),
          (r = J.css(e, "top")),
          (l = J.css(e, "left")),
          (u =
            ("absolute" === c || "fixed" === c) &&
            (r + l).indexOf("auto") > -1),
          u
            ? ((o = p.position()), (a = o.top), (i = o.left))
            : ((a = parseFloat(r) || 0), (i = parseFloat(l) || 0)),
          J.isFunction(t) && (t = t.call(e, n, s)),
          null != t.top && (d.top = t.top - s.top + a),
          null != t.left && (d.left = t.left - s.left + i),
          "using" in t ? t.using.call(e, d) : p.css(d);
      }
    }),
      J.fn.extend({
        offset: function(e) {
          if (arguments.length)
            return void 0 === e
              ? this
              : this.each(function(t) {
                  J.offset.setOffset(this, e, t);
                });
          var t,
            n,
            o = this[0],
            i = { top: 0, left: 0 },
            r = o && o.ownerDocument;
          if (r)
            return (
              (t = r.documentElement),
              J.contains(t, o)
                ? (typeof o.getBoundingClientRect !== ke &&
                    (i = o.getBoundingClientRect()),
                  (n = z(r)),
                  {
                    top: i.top + n.pageYOffset - t.clientTop,
                    left: i.left + n.pageXOffset - t.clientLeft
                  })
                : i
            );
        },
        position: function() {
          if (this[0]) {
            var e,
              t,
              n = this[0],
              o = { top: 0, left: 0 };
            return (
              "fixed" === J.css(n, "position")
                ? (t = n.getBoundingClientRect())
                : ((e = this.offsetParent()),
                  (t = this.offset()),
                  J.nodeName(e[0], "html") || (o = e.offset()),
                  (o.top += J.css(e[0], "borderTopWidth", !0)),
                  (o.left += J.css(e[0], "borderLeftWidth", !0))),
              {
                top: t.top - o.top - J.css(n, "marginTop", !0),
                left: t.left - o.left - J.css(n, "marginLeft", !0)
              }
            );
          }
        },
        offsetParent: function() {
          return this.map(function() {
            for (
              var e = this.offsetParent || Mt;
              e && !J.nodeName(e, "html") && "static" === J.css(e, "position");

            )
              e = e.offsetParent;
            return e || Mt;
          });
        }
      }),
      J.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function(
        t,
        n
      ) {
        var o = "pageYOffset" === n;
        J.fn[t] = function(i) {
          return ge(
            this,
            function(t, i, r) {
              var a = z(t);
              return void 0 === r
                ? a
                  ? a[n]
                  : t[i]
                : void (a
                    ? a.scrollTo(o ? e.pageXOffset : r, o ? r : e.pageYOffset)
                    : (t[i] = r));
            },
            t,
            i,
            arguments.length,
            null
          );
        };
      }),
      J.each(["top", "left"], function(e, t) {
        J.cssHooks[t] = T(Y.pixelPosition, function(e, n) {
          if (n)
            return (n = b(e, t)), We.test(n) ? J(e).position()[t] + "px" : n;
        });
      }),
      J.each({ Height: "height", Width: "width" }, function(e, t) {
        J.each({ padding: "inner" + e, content: t, "": "outer" + e }, function(
          n,
          o
        ) {
          J.fn[o] = function(o, i) {
            var r = arguments.length && (n || "boolean" != typeof o),
              a = n || (o === !0 || i === !0 ? "margin" : "border");
            return ge(
              this,
              function(t, n, o) {
                var i;
                return J.isWindow(t)
                  ? t.document.documentElement["client" + e]
                  : 9 === t.nodeType
                  ? ((i = t.documentElement),
                    Math.max(
                      t.body["scroll" + e],
                      i["scroll" + e],
                      t.body["offset" + e],
                      i["offset" + e],
                      i["client" + e]
                    ))
                  : void 0 === o
                  ? J.css(t, n, a)
                  : J.style(t, n, o, a);
              },
              t,
              r ? o : void 0,
              r,
              null
            );
          };
        });
      }),
      (J.fn.size = function() {
        return this.length;
      }),
      (J.fn.andSelf = J.fn.addBack),
      "function" == typeof define &&
        define.amd &&
        define("jquery", [], function() {
          return J;
        });
    var Rt = e.jQuery,
      Pt = e.$;
    return (
      (J.noConflict = function(t) {
        return (
          e.$ === J && (e.$ = Pt), t && e.jQuery === J && (e.jQuery = Rt), J
        );
      }),
      typeof t === ke && (e.jQuery = e.$ = J),
      J
    );
  }),
  (function(e, t) {
    "function" == typeof define && define.amd
      ? define(t)
      : "object" == typeof exports
      ? (module.exports = t())
      : (e.PhotoSwipeUI_Default = t());
  })(this, function() {
    "use strict";
    var e = function(e, t) {
      var n,
        o,
        i,
        r,
        a,
        s,
        l,
        u,
        c,
        p,
        d,
        f,
        h,
        m,
        g,
        v,
        y,
        x,
        w,
        b = this,
        T = !1,
        C = !0,
        E = !0,
        k = {
          barsSize: { top: 44, bottom: "auto" },
          closeElClasses: ["item", "caption", "zoom-wrap", "ui", "top-bar"],
          timeToIdle: 4e3,
          timeToIdleOutside: 1e3,
          loadingIndicatorDelay: 1e3,
          addCaptionHTMLFn: function(e, t) {
            return e.title
              ? ((t.children[0].innerHTML = e.title), !0)
              : ((t.children[0].innerHTML = ""), !1);
          },
          closeEl: !0,
          captionEl: !0,
          fullscreenEl: !0,
          zoomEl: !0,
          shareEl: !0,
          counterEl: !0,
          arrowEl: !0,
          preloaderEl: !0,
          tapToClose: !1,
          tapToToggleControls: !0,
          clickToCloseNonZoomable: !0,
          shareButtons: [
            {
              id: "facebook",
              label: "Share on Facebook",
              url: "https://www.facebook.com/sharer/sharer.php?u={{url}}"
            },
            {
              id: "twitter",
              label: "Tweet",
              url: "https://twitter.com/intent/tweet?text={{text}}&url={{url}}"
            },
            {
              id: "pinterest",
              label: "Pin it",
              url:
                "http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}"
            },
            {
              id: "download",
              label: "Download image",
              url: "{{raw_image_url}}",
              download: !0
            }
          ],
          getImageURLForShare: function() {
            return e.currItem.src || "";
          },
          getPageURLForShare: function() {
            return window.location.href;
          },
          getTextForShare: function() {
            return e.currItem.title || "";
          },
          indexIndicatorSep: " / "
        },
        S = function(e) {
          if (v) return !0;
          (e = e || window.event), g.timeToIdle && g.mouseUsed && !c && R();
          for (
            var n, o, i = e.target || e.srcElement, r = i.className, a = 0;
            a < Z.length;
            a++
          )
            (n = Z[a]),
              n.onTap &&
                r.indexOf("pswp__" + n.name) > -1 &&
                (n.onTap(), (o = !0));
          if (o) {
            e.stopPropagation && e.stopPropagation(), (v = !0);
            var s = t.features.isOldAndroid ? 600 : 30;
            y = setTimeout(function() {
              v = !1;
            }, s);
          }
        },
        D = function() {
          return !e.likelyTouchDevice || g.mouseUsed || screen.width > 1200;
        },
        I = function(e, n, o) {
          t[(o ? "add" : "remove") + "Class"](e, "pswp__" + n);
        },
        N = function() {
          var e = 1 === g.getNumItemsFn();
          e !== m && (I(o, "ui--one-slide", e), (m = e));
        },
        F = function() {
          I(l, "share-modal--hidden", E);
        },
        A = function() {
          return (
            (E = !E),
            E
              ? (t.removeClass(l, "pswp__share-modal--fade-in"),
                setTimeout(function() {
                  E && F();
                }, 300))
              : (F(),
                setTimeout(function() {
                  E || t.addClass(l, "pswp__share-modal--fade-in");
                }, 30)),
            E || L(),
            !1
          );
        },
        _ = function(t) {
          t = t || window.event;
          var n = t.target || t.srcElement;
          return (
            e.shout("shareLinkClick", t, n),
            !!n.href &&
              (!!n.hasAttribute("download") ||
                (window.open(
                  n.href,
                  "pswp_share",
                  "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=" +
                    (window.screen ? Math.round(screen.width / 2 - 275) : 100)
                ),
                E || A(),
                !1))
          );
        },
        L = function() {
          for (var e, t, n, o, i, r = "", a = 0; a < g.shareButtons.length; a++)
            (e = g.shareButtons[a]),
              (n = g.getImageURLForShare(e)),
              (o = g.getPageURLForShare(e)),
              (i = g.getTextForShare(e)),
              (t = e.url
                .replace("{{url}}", encodeURIComponent(o))
                .replace("{{image_url}}", encodeURIComponent(n))
                .replace("{{raw_image_url}}", n)
                .replace("{{text}}", encodeURIComponent(i))),
              (r +=
                '<a href="' +
                t +
                '" target="_blank" class="pswp__share--' +
                e.id +
                '"' +
                (e.download ? "download" : "") +
                ">" +
                e.label +
                "</a>"),
              g.parseShareButtonOut && (r = g.parseShareButtonOut(e, r));
          (l.children[0].innerHTML = r), (l.children[0].onclick = _);
        },
        O = function(e) {
          for (var n = 0; n < g.closeElClasses.length; n++)
            if (t.hasClass(e, "pswp__" + g.closeElClasses[n])) return !0;
        },
        M = 0,
        R = function() {
          clearTimeout(w), (M = 0), c && b.setIdle(!1);
        },
        P = function(e) {
          e = e ? e : window.event;
          var t = e.relatedTarget || e.toElement;
          (t && "HTML" !== t.nodeName) ||
            (clearTimeout(w),
            (w = setTimeout(function() {
              b.setIdle(!0);
            }, g.timeToIdleOutside)));
        },
        j = function() {
          g.fullscreenEl &&
            (n || (n = b.getFullscreenAPI()),
            n
              ? (t.bind(document, n.eventK, b.updateFullscreen),
                b.updateFullscreen(),
                t.addClass(e.template, "pswp--supports-fs"))
              : t.removeClass(e.template, "pswp--supports-fs"));
        },
        H = function() {
          g.preloaderEl &&
            (q(!0),
            p("beforeChange", function() {
              clearTimeout(h),
                (h = setTimeout(function() {
                  e.currItem && e.currItem.loading
                    ? (!e.allowProgressiveImg() ||
                        (e.currItem.img && !e.currItem.img.naturalWidth)) &&
                      q(!1)
                    : q(!0);
                }, g.loadingIndicatorDelay));
            }),
            p("imageLoadComplete", function(t, n) {
              e.currItem === n && q(!0);
            }));
        },
        q = function(e) {
          f !== e && (I(d, "preloader--active", !e), (f = e));
        },
        z = function(e) {
          var n = e.vGap;
          if (D()) {
            var a = g.barsSize;
            if (g.captionEl && "auto" === a.bottom)
              if (
                (r ||
                  ((r = t.createEl("pswp__caption pswp__caption--fake")),
                  r.appendChild(t.createEl("pswp__caption__center")),
                  o.insertBefore(r, i),
                  t.addClass(o, "pswp__ui--fit")),
                g.addCaptionHTMLFn(e, r, !0))
              ) {
                var s = r.clientHeight;
                n.bottom = parseInt(s, 10) || 44;
              } else n.bottom = a.top;
            else n.bottom = a.bottom;
            n.top = a.top;
          } else n.top = n.bottom = 0;
        },
        W = function() {
          g.timeToIdle &&
            p("mouseUsed", function() {
              t.bind(document, "mousemove", R),
                t.bind(document, "mouseout", P),
                (x = setInterval(function() {
                  M++, 2 === M && b.setIdle(!0);
                }, g.timeToIdle / 2));
            });
        },
        B = function() {
          p("onVerticalDrag", function(e) {
            C && e < 0.95
              ? b.hideControls()
              : !C && e >= 0.95 && b.showControls();
          });
          var e;
          p("onPinchClose", function(t) {
            C && t < 0.9
              ? (b.hideControls(), (e = !0))
              : e && !C && t > 0.9 && b.showControls();
          }),
            p("zoomGestureEnded", function() {
              (e = !1), e && !C && b.showControls();
            });
        },
        Z = [
          {
            name: "caption",
            option: "captionEl",
            onInit: function(e) {
              i = e;
            }
          },
          {
            name: "share-modal",
            option: "shareEl",
            onInit: function(e) {
              l = e;
            },
            onTap: function() {
              A();
            }
          },
          {
            name: "button--share",
            option: "shareEl",
            onInit: function(e) {
              s = e;
            },
            onTap: function() {
              A();
            }
          },
          {
            name: "button--zoom",
            option: "zoomEl",
            onTap: e.toggleDesktopZoom
          },
          {
            name: "counter",
            option: "counterEl",
            onInit: function(e) {
              a = e;
            }
          },
          { name: "button--close", option: "closeEl", onTap: e.close },
          { name: "button--arrow--left", option: "arrowEl", onTap: e.prev },
          { name: "button--arrow--right", option: "arrowEl", onTap: e.next },
          {
            name: "button--fs",
            option: "fullscreenEl",
            onTap: function() {
              n.isFullscreen() ? n.exit() : n.enter();
            }
          },
          {
            name: "preloader",
            option: "preloaderEl",
            onInit: function(e) {
              d = e;
            }
          }
        ],
        U = function() {
          var e,
            n,
            i,
            r = function(o) {
              if (o)
                for (var r = o.length, a = 0; a < r; a++) {
                  (e = o[a]), (n = e.className);
                  for (var s = 0; s < Z.length; s++)
                    (i = Z[s]),
                      n.indexOf("pswp__" + i.name) > -1 &&
                        (g[i.option]
                          ? (t.removeClass(e, "pswp__element--disabled"),
                            i.onInit && i.onInit(e))
                          : t.addClass(e, "pswp__element--disabled"));
                }
            };
          r(o.children);
          var a = t.getChildByClass(o, "pswp__top-bar");
          a && r(a.children);
        };
      (b.init = function() {
        t.extend(e.options, k, !0),
          (g = e.options),
          (o = t.getChildByClass(e.scrollWrap, "pswp__ui")),
          (p = e.listen),
          B(),
          p("beforeChange", b.update),
          p("doubleTap", function(t) {
            var n = e.currItem.initialZoomLevel;
            e.getZoomLevel() !== n
              ? e.zoomTo(n, t, 333)
              : e.zoomTo(g.getDoubleTapZoom(!1, e.currItem), t, 333);
          }),
          p("preventDragEvent", function(e, t, n) {
            var o = e.target || e.srcElement;
            o &&
              o.className &&
              e.type.indexOf("mouse") > -1 &&
              (o.className.indexOf("__caption") > 0 ||
                /(SMALL|STRONG|EM)/i.test(o.tagName)) &&
              (n.prevent = !1);
          }),
          p("bindEvents", function() {
            t.bind(o, "pswpTap click", S),
              t.bind(e.scrollWrap, "pswpTap", b.onGlobalTap),
              e.likelyTouchDevice ||
                t.bind(e.scrollWrap, "mouseover", b.onMouseOver);
          }),
          p("unbindEvents", function() {
            E || A(),
              x && clearInterval(x),
              t.unbind(document, "mouseout", P),
              t.unbind(document, "mousemove", R),
              t.unbind(o, "pswpTap click", S),
              t.unbind(e.scrollWrap, "pswpTap", b.onGlobalTap),
              t.unbind(e.scrollWrap, "mouseover", b.onMouseOver),
              n &&
                (t.unbind(document, n.eventK, b.updateFullscreen),
                n.isFullscreen() && ((g.hideAnimationDuration = 0), n.exit()),
                (n = null));
          }),
          p("destroy", function() {
            g.captionEl &&
              (r && o.removeChild(r), t.removeClass(i, "pswp__caption--empty")),
              l && (l.children[0].onclick = null),
              t.removeClass(o, "pswp__ui--over-close"),
              t.addClass(o, "pswp__ui--hidden"),
              b.setIdle(!1);
          }),
          g.showAnimationDuration || t.removeClass(o, "pswp__ui--hidden"),
          p("initialZoomIn", function() {
            g.showAnimationDuration && t.removeClass(o, "pswp__ui--hidden");
          }),
          p("initialZoomOut", function() {
            t.addClass(o, "pswp__ui--hidden");
          }),
          p("parseVerticalMargin", z),
          U(),
          g.shareEl && s && l && (E = !0),
          N(),
          W(),
          j(),
          H();
      }),
        (b.setIdle = function(e) {
          (c = e), I(o, "ui--idle", e);
        }),
        (b.update = function() {
          C && e.currItem
            ? (b.updateIndexIndicator(),
              g.captionEl &&
                (g.addCaptionHTMLFn(e.currItem, i),
                I(i, "caption--empty", !e.currItem.title)),
              (T = !0))
            : (T = !1),
            E || A(),
            N();
        }),
        (b.updateFullscreen = function() {
          I(e.template, "fs", n.isFullscreen());
        }),
        (b.updateIndexIndicator = function() {
          g.counterEl &&
            (a.innerHTML =
              e.getCurrentIndex() +
              1 +
              g.indexIndicatorSep +
              g.getNumItemsFn());
        }),
        (b.onGlobalTap = function(n) {
          n = n || window.event;
          var o = n.target || n.srcElement;
          if (!v)
            if (n.detail && "mouse" === n.detail.pointerType) {
              if (O(o)) return void e.close();
              t.hasClass(o, "pswp__img") &&
                (1 === e.getZoomLevel() &&
                e.getZoomLevel() <= e.currItem.fitRatio
                  ? g.clickToCloseNonZoomable && e.close()
                  : e.toggleDesktopZoom(n.detail.releasePoint));
            } else if (
              (g.tapToToggleControls &&
                (C ? b.hideControls() : b.showControls()),
              g.tapToClose && (t.hasClass(o, "pswp__img") || O(o)))
            )
              return void e.close();
        }),
        (b.onMouseOver = function(e) {
          e = e || window.event;
          var t = e.target || e.srcElement;
          I(o, "ui--over-close", O(t));
        }),
        (b.hideControls = function() {
          t.addClass(o, "pswp__ui--hidden"), (C = !1);
        }),
        (b.showControls = function() {
          (C = !0), T || b.update(), t.removeClass(o, "pswp__ui--hidden");
        }),
        (b.supportsFullscreen = function() {
          var e = document;
          return !!(
            e.exitFullscreen ||
            e.mozCancelFullScreen ||
            e.webkitExitFullscreen ||
            e.msExitFullscreen
          );
        }),
        (b.getFullscreenAPI = function() {
          var t,
            n = document.documentElement,
            o = "fullscreenchange";
          return (
            n.requestFullscreen
              ? (t = {
                  enterK: "requestFullscreen",
                  exitK: "exitFullscreen",
                  elementK: "fullscreenElement",
                  eventK: o
                })
              : n.mozRequestFullScreen
              ? (t = {
                  enterK: "mozRequestFullScreen",
                  exitK: "mozCancelFullScreen",
                  elementK: "mozFullScreenElement",
                  eventK: "moz" + o
                })
              : n.webkitRequestFullscreen
              ? (t = {
                  enterK: "webkitRequestFullscreen",
                  exitK: "webkitExitFullscreen",
                  elementK: "webkitFullscreenElement",
                  eventK: "webkit" + o
                })
              : n.msRequestFullscreen &&
                (t = {
                  enterK: "msRequestFullscreen",
                  exitK: "msExitFullscreen",
                  elementK: "msFullscreenElement",
                  eventK: "MSFullscreenChange"
                }),
            t &&
              ((t.enter = function() {
                return (
                  (u = g.closeOnScroll),
                  (g.closeOnScroll = !1),
                  "webkitRequestFullscreen" !== this.enterK
                    ? e.template[this.enterK]()
                    : void e.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT)
                );
              }),
              (t.exit = function() {
                return (g.closeOnScroll = u), document[this.exitK]();
              }),
              (t.isFullscreen = function() {
                return document[this.elementK];
              })),
            t
          );
        });
    };
    return e;
  }),
  $(function() {
    var e, t;
    return (
      (e = {
        index: 0,
        loop: !1,
        history: !1,
        shareButtons: [
          {
            id: "facebook",
            label: "Sdílet na Facebook",
            url: "https://www.facebook.com/sharer/sharer.php?u={{image_url}}"
          },
          {
            id: "twitter",
            label: "Sdílet na Twitter",
            url:
              "https://twitter.com/intent/tweet?text={{text}}&url={{image_url}}"
          },
          {
            id: "download",
            label: "Stáhnout",
            url: "{{raw_image_url}}",
            download: !0
          }
        ]
      }),
      (t = document.querySelectorAll(".pswp")[0]),
      $("[data-photoswipe-gallery]").each(function(n, o) {
        var i, r;
        return (
          (i = $(o)),
          (r = []),
          $("[data-photoswipe-image]", i).each(function(n, o) {
            var i, a;
            return (
              (i = $(o)),
              (a = i.data("size").split("x")),
              r.push({
                src: i.parent().attr("href"),
                msrc: i.attr("src"),
                w: a[0],
                h: a[1]
              }),
              i.parent().click(function(n) {
                var o, i;
                return (
                  n.preventDefault(),
                  (i = $(this).index()),
                  (e.index = i),
                  (o = new PhotoSwipe(t, PhotoSwipeUI_Default, r, e)),
                  o.init()
                );
              })
            );
          })
        );
      })
    );
  });
