import { NowRequest, NowResponse } from '@now/node/dist'

const mandrill = require('node-mandrill')()

export default function sendMail(req: NowRequest, res: NowResponse) {
  if (!req.body || !req.body.email || !req.body.message) {
    res.status(403)
    res.send({
      message: 'Bad request',
    })
    return
  }

  mandrill(
    '/messages/send',
    {
      key: process.env.MANDRILL_API_KEY,
      message: {
        to: [
          {
            email: process.env.CONTACT_FORM_TO_MAIL,
            name: process.env.CONTACT_FORM_TO_NAME,
          },
        ],
        from_email: 'web@kordulka-kroje.cz',
        headers: {
          'Reply-To': {
            email: req.body.email,
            name: req.body.name,
          },
        },
        subject: 'Dotaz z webu',
        text: req.body.message,
      },
    },
    function(error, response) {
      if (error) {
        console.error(JSON.stringify(error))
      }

      res.setHeader(
        'Location',
        (req.headers.referer || 'http://localhost:3000') + '#contact-form-sent',
      )
      res.status(301)
      res.send({})
    },
  )
}
